.. _configuration:

****************************************
Configuration
****************************************

The following environment variables are used to configure the application.

Generally, they are set via a Kubernetes ConfigMap with values coming from the Helm values.yaml. See :doc:`deployment_to_kubernetes`


.. list-table:: Environment variables used by ska-oso-odt-ui
   :widths: 10 10 10 10 10
   :header-rows: 1

   * - Environment variable
     - Description
     - Required/optional in the application
     - Corresponding Helm value
     - Required/optional in the Helm chart
   * - BACKEND_URL
     - A URL that points to a running ODT back-end (ska-oso-services), without the full API path (e.g. ``http://192.0.0.0/ska-oso-odt-ui``).
     - Required
     - ``ska-oso-odt-ui.backendURL``
     - Optional - will fall back on: ``{{ .Values.global.ingress.host }}/{{ .Release.Namespace }}``
   * - BASE_URL
     - The path the application should be available on the host, i.e. what to visit in the browser
     - Required
     - ``/{{ .Release.Namespace }}/{{.Values.ingress.path}}/``
     - Required - default ``.Values.ingress.path`` is ``/odt/``


These variables are set when the application is deployed rather than when it is built to allow the deployment
in different Kubernetes environments without the need to rebuild the application. The variables are set by
the ``scripts/write_env_js.sh`` script which writes them to ``env.js`` file that is read by the application at
runtime. The script is executed when the ODT UI docker image  is started. This script is also used for local deployment
as part of ``yarn start`` command (run in the ``make set-dev-env-vars`` target).

Additionally when deploying the pod to Kubernetes, the script ``scripts/set_absolute_paths.sh`` is run when
the container starts to replace any relative path references in the ``index.html`` file with the full ``BASE_URL``.