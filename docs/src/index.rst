
SKA Observation Design Tool UI
==============================

The Observation Design Tool (ODT) allows users to design observations for the SKA (Mid or Low) by
creating and editing Scheduling Block Definitions (SBDs)

The UI component is a React Single-page Application (SPA) which is served from a Kubernetes deployment.
This communicates with a back-end (ska-oso-services) via a REST API.

For an overview of the applications and functionality within OSO, see `Solution Intent <https://confluence.skatelescope.org/pages/viewpage.action?pageId=159387040>`_.

For information on deploying and configuring the application in a given Kubernetes or local environment, see the 'Deploying and configuring' section.

For user information for a deployed instance of this application, see the 'User Guide'.

For developer information, application internals, and information about interactions with other OSO applications, see the 'Developer Information' section

.. toctree::
   :maxdepth: 1
   :caption: Releases
   :hidden:

   CHANGELOG.rst

.. toctree::
    :maxdepth: 2
    :caption: General
    :hidden:

    general/background.rst

.. toctree::
    :maxdepth: 2
    :caption: Deploying and configuring
    :hidden:

    deployment/deployment_to_kubernetes.rst
    deployment/configuration.rst
    deployment/persistent_environments.rst

.. toctree::
    :maxdepth: 2
    :caption: User Guide
    :hidden:

    external/user_guide.rst
    external/sbd_editor.rst
    
.. toctree::
    :maxdepth: 2
    :caption: Developer Information
    :hidden:

    internal/development.rst
