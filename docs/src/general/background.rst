.. _background:

**********
Background
**********

The Observation Design Tool is a React-based front-end that allows a user to create a
Scheduling Block Definition that an be run on either SKA Mid or Low.
