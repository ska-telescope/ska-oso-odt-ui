import { describe, expect, test } from 'vitest';
import { SdpConfigurationScheme } from './sdpConfiguration';

describe('SdpConfigurationScheme', () => {
  test('with null should return success false and error message', () => {
    const data = null;
    const result = SdpConfigurationScheme.safeParse(data);
    expect(result.success).toBe(false);
    if (result.error && result.error.length > 0) {
      const error = result.error[0];
      expect(error.code).toBe('invalid_type');
      expect(error.message).toBe('Expected array, received null');
    }
  });
  test('with an array should return success true', () => {
    const data = {
      eb_id: null,
      max_length: null,
      scan_types: [
        {
          scan_type_id: null,
          target: null,
          channels: [
            {
              count: null,
              start: null,
              stride: null,
              freq_min: null,
              freq_max: null,
              link_map: []
            }
          ]
        }
      ],
      processing_blocks: [
        {
          pb_id: null,
          workflow: [],
          parameters: {},
          dependencies: []
        }
      ]
    };
    const result = SdpConfigurationScheme.safeParse(data);
    expect(result.success).toBe(false);
    if (result.error && result.error.length > 0) {
      const error = result.error[0];
      expect(error.code).toBe('invalid_type');
      expect(error.message).toBe('Expected array, received null');
    }
  });
});
