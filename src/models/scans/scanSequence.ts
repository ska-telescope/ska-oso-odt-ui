import { z } from 'zod';

export const ScanSequenceItemScheme = z.object({
  scanSequenceId: z.number().default(Math.random),
  scanDefinitionId: z.string().trim(),
  target: z.string().trim()
});

export const ScanSequenceScheme = z.array(ScanSequenceItemScheme);

export type ScanSequenceItemType = z.infer<typeof ScanSequenceItemScheme>;
export type ScanSequenceType = z.infer<typeof ScanSequenceScheme>;
