import { z } from 'zod';
import { zodRequiredNumber } from '../validation/common';

export const ScanDefinitionScheme = z.object({
  scanDefinitionId: z.string().trim(),
  target: z.string().trim().optional(),
  csp: z.string().trim().optional(),
  durationS: zodRequiredNumber.pipe(z.number().gt(0))
});

export const ScanDefinitionsScheme = z.array(ScanDefinitionScheme);

export type ScanDefinitionType = z.infer<typeof ScanDefinitionScheme>;
export type ScanDefinitionsType = z.infer<typeof ScanDefinitionsScheme>;
