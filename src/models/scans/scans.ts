import { z } from 'zod';
import { ScanDefinitionsScheme } from './scanDefinitions';
import { ScanSequenceScheme } from './scanSequence';

export const ScansScheme = z.object({
  scanDefinitions: ScanDefinitionsScheme,
  scanSequence: ScanSequenceScheme
});

export type ScansType = z.infer<typeof ScansScheme>;
