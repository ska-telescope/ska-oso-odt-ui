import { z } from 'zod';

import {
  blankSdpConfigurationDependencies,
  SdpConfigurationDependenciesScheme
} from './sdpConfigurationDependencies';
import {
  blankSdpConfigurationWorkflow,
  SdpConfigurationWorkflowScheme
} from './sdpConfigurationWorkflow';

export const SdpConfigurationProcessingBlocksScheme = z.object({
  pb_id: z
    .union([
      z
        .string()
        .trim()
        .regex(/^pb-[a-z0-9]+-[0-9]{8}-[a-z0-9]+$/),
      z.null()
    ])
    .default(null),
  workflow: z.union([SdpConfigurationWorkflowScheme, z.null()]).default(null),
  parameters: z.union([z.object({}), z.null()]).default(null),
  dependencies: z
    .union([z.array(SdpConfigurationDependenciesScheme).optional(), z.null()])
    .default(null)
});

export type SdpConfigurationProcessingBlocksType = z.infer<
  typeof SdpConfigurationProcessingBlocksScheme
>;

export const sampleSdpConfigurationProcessingBlocks: SdpConfigurationProcessingBlocksType =
  {
    pb_id: 'pb-mvp01-20200325-00001',
    workflow: blankSdpConfigurationWorkflow,
    parameters: {},
    dependencies: [blankSdpConfigurationDependencies]
  };

export const blankSdpConfigurationProcessingBlocks: SdpConfigurationProcessingBlocksType =
  {
    pb_id: null,
    workflow: blankSdpConfigurationWorkflow,
    parameters: {},
    dependencies: [blankSdpConfigurationDependencies]
  };
