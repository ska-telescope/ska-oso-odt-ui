/**
 * This module contains common, helpful things to do with validating zod schemas
 **/
import { RefinementCtx, ZodIssueCode, z } from 'zod';
import { ValidationResult } from '../../lib/validators';
import { ZodTypeAny } from 'zod/lib/types';

export const addErrorToCtx = (
  ctx: RefinementCtx,
  validationResult: ValidationResult,
  path: Array<string | number>
) => {
  if (!validationResult.valid) {
    ctx.addIssue({
      path,
      code: ZodIssueCode.custom,
      message: validationResult.message
    });
  }
};

// The form will send a string value to zod, which is why we use coerce. However, if the string is empty it will
// be coerced into 0. So we need to check it is not empty first. See https://zod.dev/?id=you-can-use-pipe-to-fix-common-issues-with-zcoerce.
export const zodRequired = (zodType: ZodTypeAny) =>
  z.coerce.string().min(1, { message: 'Value required.' }).pipe(zodType);

export const zodRequiredNumber = zodRequired(z.coerce.number());

export const zodRequiredString = zodRequired(z.coerce.string());
