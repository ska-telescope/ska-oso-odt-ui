import { z } from 'zod';

export const DishConfigurationsScheme = z.object({
  dish_configuration_id: z.string().trim().default(''),
  receiver_band: z.string().trim().default('')
});

export type DishConfigurationsType = z.infer<typeof DishConfigurationsScheme>;

export const blankDishConfigurations: DishConfigurationsType = {
  dish_configuration_id: null,
  receiver_band: null
};
