import { z } from 'zod';

export const SdpConfigurationDependenciesScheme = z.object({
  pb_id: z
    .union([
      z
        .string()
        .trim()
        .regex(/^pb-[a-z0-9]+-[0-9]{8}-[a-z0-9]+$/),
      z.null()
    ])
    .default(null),
  kind: z.union([z.array(z.string().trim()).optional(), z.null()]).default([])
});

export type SdpConfigurationDependenciesType = z.infer<
  typeof SdpConfigurationDependenciesScheme
>;

export const sampleSdpConfigurationDependencies: SdpConfigurationDependenciesType =
  {
    pb_id: 'pb-mvp01-20200325-00001',
    kind: ['visibilities', 'calibration']
  };

export const blankSdpConfigurationDependencies: SdpConfigurationDependenciesType =
  {
    pb_id: null,
    kind: []
  };
