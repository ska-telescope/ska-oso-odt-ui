import { z } from 'zod';

export const LinkMapScheme = z.object({
  channelId: z.union([z.string().trim(), z.null()]).default(null),
  linkId: z.union([z.string().trim(), z.null()]).default(null)
});

export type LinkMapType = z.infer<typeof LinkMapScheme>;

export const sampleLinkMap: LinkMapType = {
  channelId: '1',
  linkId: 'link #1'
};

export const blankLinkMap: LinkMapType = {
  channelId: null,
  linkId: null
};
