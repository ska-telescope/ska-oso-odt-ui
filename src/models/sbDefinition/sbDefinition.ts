import { z } from 'zod';

import {
  LowSignalProcessorScheme,
  MidSignalProcessorScheme
} from '../signalProcessor/signalProcessor';
import { sbDefinitionRefinement } from './refinement';
import { GeneralScheme } from '../general/general';
import { ScriptSchema } from '../script/script';

import { targetsRefinement } from '../targets/refinement';
import { targetSchema } from '../targets/target';
import { ScansScheme } from '../scans/scans';
import {
  LowArrayConfigSchema,
  MidArrayConfigSchema
} from '../array/arrayConfig';

const CommonSbDefinitionScheme = z.object({
  general: GeneralScheme,
  script: ScriptSchema,
  targets: targetSchema.array().default([]).superRefine(targetsRefinement),
  scans: ScansScheme
});

export const MidSbDefinitionScheme = CommonSbDefinitionScheme.merge(
  z.object({
    array: MidArrayConfigSchema,
    signalProcessor: MidSignalProcessorScheme.array().default([])
  })
).superRefine(sbDefinitionRefinement);

export const LowSbDefinitionScheme = CommonSbDefinitionScheme.merge(
  z.object({
    array: LowArrayConfigSchema,
    signalProcessor: LowSignalProcessorScheme.array().default([])
  })
).superRefine(sbDefinitionRefinement);

export type LowSbDefinitionType = z.infer<typeof LowSbDefinitionScheme>;

export type MidSbDefinitionType = z.infer<typeof MidSbDefinitionScheme>;

export type SbDefinitionType = LowSbDefinitionType | MidSbDefinitionType;
