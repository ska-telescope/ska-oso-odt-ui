import { RefinementCtx } from 'zod';
import { TargetType } from './target';
import { validateNameIsUnique } from '../../lib/validators';
import { addErrorToCtx } from '../validation/common';
import { SbDefinitionType } from './sbDefinition';
import { LowSignalProcessorType } from '../signalProcessor/signalProcessor';
import { MidSpectralWindowSchemeType } from '../signalProcessor/spectralWindow';

export const sbDefinitionRefinement = (
  sbDefintion: SbDefinitionType,
  ctx: RefinementCtx
) => {
  // validateTargetNames(sbDefintion.targets, ctx);
  validateSignalProcessorName(sbDefintion.signalProcessor, ctx);
};

const validateSignalProcessorName = (
  signalProcessors: LowSignalProcessorType[] | MidSpectralWindowSchemeType[],
  ctx: RefinementCtx
) => {
  signalProcessors.forEach((signalProcessor, index) => {
    const nameValidationResult = validateNameIsUnique(
      signalProcessor,
      signalProcessors
    );
    addErrorToCtx(ctx, nameValidationResult, [
      'signalProcessor',
      index,
      'name'
    ]);
  });
};

export const validateTargetNames = (
  targets: TargetType[],
  ctx: RefinementCtx
) => {
  targets.forEach((target, index) => {
    const nameValidationResult = validateNameIsUnique(target, targets);
    // TODO when this is used for the top level sbd, need the targets in the path
    // addErrorToCtx(ctx, bandwidthValidationResult, ['targets', index, 'name']);
    addErrorToCtx(ctx, nameValidationResult, [index, 'name']);
  });
};
