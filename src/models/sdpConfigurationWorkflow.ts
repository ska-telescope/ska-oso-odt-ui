import { z } from 'zod';

export const SdpConfigurationWorkflowScheme = z.object({
  name: z.union([z.string().trim().optional(), z.null()]).default(null),
  kind: z.union([z.string().trim().optional(), z.null()]).default(null),
  version: z.union([z.string().trim().optional(), z.null()]).default(null)
});

export type SdpConfigurationWorkflowType = z.infer<
  typeof SdpConfigurationWorkflowScheme
>;

export const sampleSdpConfigurationWorkflow: SdpConfigurationWorkflowType = {
  name: 'vis_receive',
  kind: 'realtime',
  version: '0.1.0'
};

export const blankSdpConfigurationWorkflow: SdpConfigurationWorkflowType = {
  name: null,
  kind: null,
  version: null
};
