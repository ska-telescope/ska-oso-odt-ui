import { z } from 'zod';

export const GeneralScheme = z.object({
  name: z.string().optional(),
  sbdId: z.string().optional(),
  // All the metadata fields are defined as optional since while within the Metadata storage object some
  // of these fields are required, the Metadata within SBDefinition itself is optional and so these fields
  // are not necessarily always set in the storage.
  version: z.number().int().optional(),
  createdBy: z.string().optional(),
  createdOn: z.string().datetime().optional(),
  lastModifiedBy: z.string().optional(),
  lastModifiedOn: z.string().datetime().optional(),
  description: z.string().optional()
});

export type GeneralType = z.infer<typeof GeneralScheme>;
