import { z } from 'zod';
import { SelectOption } from '../components/shared/ui';

export enum Kind {
  git = 'git',
  filesystem = 'filesystem'
}

export const ScriptKindOptions: SelectOption[] = Object.keys(Kind).map(
  (item) => ({
    label: Kind[item as keyof typeof Kind],
    value: Kind[item as keyof typeof Kind]
  })
);

export const ArgFormatSchema = z.object({
  id: z.number(),
  arg: z.string(),
  kw: z.string()
});
export type ArgFormatType = z.infer<typeof ArgFormatSchema>;

export const FilesystemScriptSchema = z.object({
  identifier: z.string(),
  kind: z.literal(Kind.filesystem),
  path: z.string().trim().min(1, { message: 'Path is required' }),
  initArgs: z.array(ArgFormatSchema),
  mainArgs: z.array(ArgFormatSchema)
});
export type FilesystemScriptType = z.infer<typeof FilesystemScriptSchema>;

export const GitScriptSchema = z.object({
  identifier: z.string(),
  kind: z.literal(Kind.git),
  path: z.string().trim().min(1, { message: 'Path is required' }),
  repo: z.string().trim().min(1, { message: 'Repo is required' }),
  branch: z.string().optional(),
  commit: z.string().optional(),
  initArgs: z.array(ArgFormatSchema),
  mainArgs: z.array(ArgFormatSchema)
});
export type GitScriptType = z.infer<typeof GitScriptSchema>;

export const ActivitySchema = z.union([
  FilesystemScriptSchema,
  GitScriptSchema
]);

export type ActivityType = z.infer<typeof ActivitySchema>;
