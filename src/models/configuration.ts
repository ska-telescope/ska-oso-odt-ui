import { z } from 'zod';

export const AppConfigSchema = z.object({
  documentationURL: z.string().trim(),
  skaoHomepage: z.string().trim(),
  appTitle: z
    .object({
      name: z.string().trim()
    })
    .array(),
  currentVersion: z.string().trim(),
  receiverBands: z.array(
    z.object({
      name: z.string().trim(),
      id: z.string().trim()
    })
  ),
  workflowKind: z.array(z.string().trim()),
  fspFunctionModes: z.string().trim().array(),
  componentNames: z.array(
    z.object({
      key: z.string().trim(),
      name: z.string().trim()
    })
  ),
  procedureTypes: z.array(z.string().trim())
});

export type AppConfig = z.infer<typeof AppConfigSchema>;

export const AppTitleSchema = z.object({
  name: z.string().trim()
});

export type AppTitle = z.infer<typeof AppTitleSchema>;

export const ReceiverBandSchema = z.object({
  name: z.string().trim(),
  id: z.string().trim()
});

export type ReceiverBand = z.infer<typeof ReceiverBandSchema>;

export const ComponentNameSchema = z.object({
  key: z.string().trim(),
  name: z.string().trim()
});

export type ComponentName = z.infer<typeof ComponentNameSchema>;
