import { z } from 'zod';

import {
  MidSpectralWindowScheme,
  LowSpectralWindowScheme,
  MidBandsDefaults,
  MidBand
} from './spectralWindow';
import { zodRequiredNumber, zodRequiredString } from '../validation/common';
import {
  lowSignalProcessorRefinement,
  midSignalProcessorRefinement
} from './refinement';

const CommonSignalProcessorScheme = z.object({
  id: zodRequiredString,
  name: zodRequiredString
});

const midDefaultBand = MidBand.BAND_1;

export const MidSignalProcessorScheme = CommonSignalProcessorScheme.merge(
  z.object({
    band: z.nativeEnum(MidBand).default(midDefaultBand),
    fsOffsetMhz: zodRequiredNumber.default(
      MidBandsDefaults.get(midDefaultBand)!.fsOffsetMhz!
    ),
    spectralWindows: z.array(MidSpectralWindowScheme).default([])
  })
).superRefine(midSignalProcessorRefinement);

export const LowSignalProcessorScheme = CommonSignalProcessorScheme.merge(
  z.object({
    doPst: z.boolean().default(false),
    spectralWindows: z.array(LowSpectralWindowScheme).default([])
  })
).superRefine(lowSignalProcessorRefinement);

export type LowSignalProcessorType = z.infer<typeof LowSignalProcessorScheme>;

export type MidSignalProcessorType = z.infer<typeof MidSignalProcessorScheme>;
