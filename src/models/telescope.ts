import { z } from 'zod';

export const THEME_LIGHT = 'light';
export const THEME_DARK = 'dark';

export type Telescope = {
  code: string;
  name: string;
  location: string;
  position: {
    lat: number;
    lon: number;
  };
  image: string;
};

export type telescopeSelectorProps = {
  // optional
  ariaDescription?: string;
  ariaTitle?: string;
  disabled?: boolean;
  toolTip?: string;
};

export const TelescopeScheme = z.object({
  code: z.string().trim(),
  name: z.string().trim(),
  location: z.string().trim(),
  position: z
    .object({
      lat: z.number(),
      lon: z.number()
    })
    .optional(),
  image: z.string().trim()
});

export type TelescopeType = z.infer<typeof TelescopeScheme>;

export const TELESCOPE_LOW: TelescopeType = {
  code: 'low',
  name: 'SKA LOW',
  location: 'Dalgaranga Gold M',
  position: {
    lat: -27.685534514102958,
    lon: 117.08484475669175
  },
  image:
    'https://res.cloudinary.com/dmwc3xvv8/image/upload/v1612505143/ska_low_dzquiv.svg'
};

export const TELESCOPE_MID: Telescope = {
  code: 'mid',
  name: 'SKA MID',
  location: 'Carnarvon',
  position: {
    lat: -30.722597428175952,
    lon: 21.89239803559566
  },
  image:
    'https://res.cloudinary.com/dmwc3xvv8/image/upload/v1612505475/ska_mid_mnvuil.svg'
};

export const TelescopeList = [TELESCOPE_LOW, TELESCOPE_MID];
