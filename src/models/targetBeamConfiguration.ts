import { z } from 'zod';

export const TargetBeamConfigurationScheme = z.object({
  target_beam_id: z.string().trim(),
  target: z.string().trim(),
  subarray_beam_configuration: z.string().trim()
});

export type TargetBeamConfigurationType = z.infer<
  typeof TargetBeamConfigurationScheme
>;

export const sampleTargetBeamConfiguration: TargetBeamConfigurationType = {
  target_beam_id: 'target #1 with beam A config 1',
  target: 'target #1',
  subarray_beam_configuration: 'beam A config 1'
};

export const blankTargetBeamConfiguration: TargetBeamConfigurationType = {
  target_beam_id: null,
  target: null,
  subarray_beam_configuration: null
};
