import { z } from 'zod';

export const ProcessingBlockScheme = z.object({
  pb_id: z
    .union([
      z
        .string()
        .trim()
        .regex(/^pb-[a-z0-9]+-[0-9]{8}-[a-z0-9]+$/),
      z.null()
    ])
    .default(null),
  parameters: z.union([z.string().trim().optional(), z.null()]).default(null),
  name: z.union([z.string().trim().optional(), z.null()]).default(null),
  kind: z.union([z.string().trim().optional(), z.null()]).default(null),
  version: z.union([z.string().trim().optional(), z.null()]).default(null)
});

export type ProcessingBlockType = z.infer<typeof ProcessingBlockScheme>;

export const sampleProcessingBlock: ProcessingBlockType = {
  pb_id: 'pb-mvp01-20240212-00001',
  parameters: '',
  name: '',
  kind: 'batch',
  version: '1.0.0'
};

export const blankProcessingBlock: ProcessingBlockType = {
  pb_id: null,
  parameters: null,
  name: null,
  kind: null,
  version: null
};
