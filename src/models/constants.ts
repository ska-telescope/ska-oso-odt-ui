export const HEADER_HEIGHT = 70;
export const FOOTER_HEIGHT = 20;

export const MAX_NO_SUBARRAY_BEAMS = 48;
export const MAX_NO_CHANNEL_BLOCKS = 48;
export const MIN_PHASE_CENTRE = -20.0;
export const MAX_PHASE_CENTRE = 20.0;
export const MAX_START_CHANNEL = 376;
export const MIN_NO_CHANNELS = 8;
export const MAX_NO_CHANNELS = 48;
export const MIN_SUBSTATION_INDEX = 1;
export const MAX_SUBSTATION_INDEX = 8;
export const MAX_ANTENNA_WEIGHT = 256.0; // maximum number of antennas in a beam
