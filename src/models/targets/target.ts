import { z } from 'zod';

import { SolarSystemObjectName } from '../../generated/models/solar-system-object-name';

type nonEmpty = [string, ...string[]];

export const radialMotionKind = {
  VELOCITY: 'Velocity',
  REDSHIFT: 'Redshift'
} as const;

export const referenceFrame = {
  LSRK: 'lsrk',
  BARY: 'bary',
  TOPO: 'topo'
} as const;

export type ReferenceFrame =
  (typeof referenceFrame)[keyof typeof referenceFrame];

export const velocityDefinition = {
  RADIO: 'Radio',
  OPTICAL: 'Optical',
  RELATIVISTIC: 'Relativistic'
} as const;

export type VelocityDefinition =
  (typeof velocityDefinition)[keyof typeof velocityDefinition];

export const radialMotionSchema = z.discriminatedUnion('kind', [
  z.object({
    kind: z.literal(radialMotionKind.VELOCITY),
    velocity: z.coerce.number().default(0),
    referenceFrame: z
      .enum(Object.values(referenceFrame) as nonEmpty)
      .default(referenceFrame.LSRK),
    velocityDefinition: z
      .enum(Object.values(velocityDefinition) as nonEmpty)
      .default(velocityDefinition.OPTICAL)
  }),
  z.object({
    kind: z.literal(radialMotionKind.REDSHIFT),
    redshift: z.coerce.number().default(0)
  })
]);

export type RadialMotionType = z.infer<typeof radialMotionSchema>;

export const fieldPattern = {
  FIVEPOINT: 'Five Point',
  POINTINGCENTRES: 'Pointing Centres'
} as const;

export type FieldPattern = (typeof fieldPattern)[keyof typeof fieldPattern];

export const offsetsSchema = z.object({
  raOffset: z.coerce.number().default(0),
  decOffset: z.coerce.number().default(0)
});

export const fieldPatternSchema = z.discriminatedUnion('kind', [
  z.object({
    kind: z.literal(fieldPattern.FIVEPOINT),
    offset: z.coerce.number().gt(0).default(10)
  }),
  z.object({
    kind: z.literal(fieldPattern.POINTINGCENTRES),
    offsets: offsetsSchema
      .array()
      .nonempty()
      .max(1)
      .default([offsetsSchema.parse({})])
  })
]);

export type FieldPatternType = z.infer<typeof fieldPatternSchema>;

export const coordinateKind = {
  ICRS: 'ICRS',
  SSO: 'Solar System Object'
} as const;

export const coordinatesSchema = z.discriminatedUnion('kind', [
  z.object({
    kind: z.literal(coordinateKind.ICRS),
    ra: z
      .string()
      .regex(/^\d{1,2}:\d{1,2}:\d{1,2}((\.?)|(\.\d+))$/)
      .default('00:00:00'),
    dec: z
      .string()
      .regex(/^[-+]?\d{1,2}:\d{1,2}:\d{1,2}((\.?)|(\.\d+))$/)
      .default('00:00:00')
  }),
  z.object({
    kind: z.literal('Solar System Object'),
    name: z.nativeEnum(SolarSystemObjectName).default(SolarSystemObjectName.Sun)
  })
]);

export type CoordinatesType = z.infer<typeof coordinatesSchema>;

export const targetSchema = z.object({
  id: z.string().trim().default(''),
  name: z.string().trim().default(''),
  addPstBeam: z.boolean().default(false),
  coordinate: coordinatesSchema.default(
    coordinatesSchema.parse({
      kind: coordinateKind.ICRS
    })
  ),
  pointingPattern: fieldPatternSchema.default(
    fieldPatternSchema.parse({
      kind: fieldPattern.POINTINGCENTRES
    })
  ),
  radialMotion: radialMotionSchema.default(
    radialMotionSchema.parse({
      kind: radialMotionKind.VELOCITY
    })
  )
});

export type TargetType = z.infer<typeof targetSchema>;
