import { RefinementCtx } from 'zod';
import { TargetType } from './target';
import { validateTargetNames } from '../sbDefinition/refinement';

// TODO once targets is moved into the sbDefinition this can be moved also
export const targetsRefinement = (
  targets: TargetType[],
  ctx: RefinementCtx
) => {
  validateTargetNames(targets, ctx);
};
