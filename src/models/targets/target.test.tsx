import { describe, expect, test } from 'vitest';
import {
  targetSchema,
  TargetType,
  coordinateKind,
  fieldPattern,
  radialMotionKind,
  referenceFrame,
  velocityDefinition
} from './target';

describe('target', () => {
  describe('targetSchema', () => {
    test('should produce a target object with sensible defaults', () => {
      const result: TargetType = targetSchema.parse({});
      expect(result.name).toBe('');
      expect(result.coordinate).toStrictEqual({
        kind: coordinateKind.ICRS,
        ra: '00:00:00',
        dec: '00:00:00'
      });
      expect(result.pointingPattern).toStrictEqual({
        kind: fieldPattern.POINTINGCENTRES,
        offsets: [{ raOffset: 0, decOffset: 0 }]
      });
      expect(result.radialMotion).toStrictEqual({
        kind: radialMotionKind.VELOCITY,
        referenceFrame: referenceFrame.LSRK,
        velocityDefinition: velocityDefinition.OPTICAL,
        velocity: 0
      });
    });
  });
});
