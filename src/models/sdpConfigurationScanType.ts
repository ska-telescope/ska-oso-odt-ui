import { z } from 'zod';

import {
  blankSdpConfigurationChannels,
  SdpConfigurationChannelsScheme
} from './sdpConfigurationChannels';

export const SdpConfigurationScanTypeScheme = z.object({
  scan_type_id: z.union([z.string().trim().optional(), z.null()]).default(null),
  target: z.union([z.string().trim().optional(), z.null()]).default(null),
  channels: z
    .union([z.array(SdpConfigurationChannelsScheme), z.null()])
    .default(null)
});

export type SdpConfigurationScanTypeType = z.infer<
  typeof SdpConfigurationScanTypeScheme
>;

export const sampleSdpConfigurationScanType: SdpConfigurationScanTypeType = {
  scan_type_id: 'science_A',
  target: 'my science target',
  channels: [blankSdpConfigurationChannels]
};

export const blankSdpConfigurationScanType: SdpConfigurationScanTypeType = {
  scan_type_id: null,
  target: null,
  channels: [blankSdpConfigurationChannels]
};
