import { z } from 'zod';

export const MccsAllocationScheme = z.object({
  subarray_beam_ids: z.array(z.string().trim().optional()),
  station_ids: z.array(z.array(z.number().optional())),
  channel_blocks: z.array(z.number().optional())
});

export type MccsAllocationType = z.infer<typeof MccsAllocationScheme>;

export const sampleMccsAllocation: MccsAllocationType = {
  subarray_beam_ids: ['beam A'],
  station_ids: [[1, 2]],
  channel_blocks: [1]
};

export const blankMccsAllocation: MccsAllocationType = {
  subarray_beam_ids: [],
  station_ids: [[]],
  channel_blocks: []
};
