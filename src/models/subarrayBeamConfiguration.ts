import { z } from 'zod';

export const SubarrayBeamConfigurationScheme = z.object({
  subarray_beam_configuration_id: z.string().trim(),
  subarray_beam_id: z.string().trim().optional(),
  update_rate: z.union([z.number(), z.null()]).default(null),
  antenna_weights: z.array(z.number()).optional(),
  phase_centre: z.array(z.number()).optional(),
  channels: z.array(z.array(z.number()).optional())
});

export type SubarrayBeamConfigurationType = z.infer<
  typeof SubarrayBeamConfigurationScheme
>;

export const sampleSubarrayBeamConfiguration: SubarrayBeamConfigurationType = {
  subarray_beam_configuration_id: 'beam A config 1',
  subarray_beam_id: 'beam A',
  update_rate: 0.0,
  antenna_weights: [1.0, 1.0, 1.0],
  phase_centre: [0.0, 0.0],
  channels: [
    [0, 8, 1, 1],
    [8, 8, 2, 1]
  ]
};

export const blankSubarrayBeamConfiguration: SubarrayBeamConfigurationType = {
  subarray_beam_configuration_id: null,
  subarray_beam_id: null,
  update_rate: null,
  antenna_weights: [],
  phase_centre: [],
  channels: [[]]
};
