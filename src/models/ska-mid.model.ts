import { z } from 'zod';

export const SkaMidScheme = z.object({
  interface: z.string().trim(),
  sbd_id: z.string().trim(),
  telescope: z.string().trim(),
  metadata: z.object({
    version: z.string(),
    created_by: z.string().trim(),
    created_on: z.string().trim(),
    last_modified_on: z.string().trim(),
    last_modified_by: z.string().trim()
  }),
  activities: z.object({
    allocate: z.object({
      kind: z.string().trim(),
      path: z.string().trim(),
      function_args: z.object({
        init: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        }),
        run: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        })
      })
    }),
    observe: z.object({
      kind: z.string().trim(),
      path: z.string().trim(),
      repo: z.string().trim(),
      branch: z.string().trim(),
      function_args: z.object({
        init: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        }),
        run: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        })
      })
    })
  }),
  scan_definitions: z.array(
    z.union([
      z.object({
        scan_definition_id: z.string().trim(),
        scan_duration: z.number(),
        target: z.string().trim(),
        dish_configuration: z.string().trim(),
        scan_type: z.string().trim(),
        csp_configuration: z.string().trim()
      }),
      z.object({
        scan_duration: z.number(),
        target: z.string().trim(),
        dish_configuration: z.string().trim(),
        scan_type: z.string().trim(),
        scan_definition_id: z.string().trim()
      })
    ])
  ),
  scan_sequence: z.array(z.string().trim()),
  targets: z.array(
    z.object({
      target_id: z.string().trim(),
      reference_coordinate: z.object({
        kind: z.string().trim(),
        reference_frame: z.string().trim(),
        ra: z.string().trim(),
        dec: z.string().trim()
      }),
      pointing_pattern: z.object({
        active: z.string().trim(),
        parameters: z.array(
          z.object({
            kind: z.string().trim(),
            offset_x_arcsec: z.number(),
            offset_y_arcsec: z.number()
          })
        )
      })
    })
  ),
  sdp_configuration: z.object({
    eb_id: z.string().trim(),
    max_length: z.number(),
    scan_types: z.array(
      z.object({
        scan_type_id: z.string().trim(),
        target: z.string().trim(),
        channels: z.array(
          z.object({
            count: z.number(),
            start: z.number(),
            stride: z.number(),
            freq_min: z.number(),
            freq_max: z.number(),
            link_map: z.array(z.array(z.number()))
          })
        )
      })
    ),
    processing_blocks: z.array(
      z.union([
        z.object({
          pb_id: z.string().trim(),
          workflow: z.object({
            name: z.string().trim(),
            kind: z.string().trim(),
            version: z.string().trim()
          }),
          parameters: z.object({})
        }),
        z.object({
          pb_id: z.string().trim(),
          workflow: z.object({
            name: z.string().trim(),
            kind: z.string().trim(),
            version: z.string().trim()
          }),
          parameters: z.object({}),
          dependencies: z.array(
            z.object({
              pb_id: z.string().trim(),
              kind: z.array(z.string().trim())
            })
          )
        }),
        z.object({
          pb_id: z.string().trim(),
          workflow: z.object({
            name: z.string().trim(),
            kind: z.string().trim(),
            version: z.string().trim()
          }),
          parameters: z.object({}),
          dependencies: z.array(
            z.object({
              kind: z.array(z.string().trim()),
              pb_id: z.string().trim()
            })
          )
        })
      ])
    )
  }),
  csp_configurations: z.array(
    z.object({
      config_id: z.string().trim(),
      subarray: z.object({ subarray_name: z.string().trim() }),
      common: z.object({
        subarray_id: z.number(),
        band_5_tuning: z.array(z.number())
      }),
      cbf: z.object({
        fsp: z.array(
          z.union([
            z.object({
              fsp_id: z.number(),
              function_mode: z.string().trim(),
              frequency_slice_id: z.number(),
              integration_factor: z.number(),
              zoom_factor: z.number(),
              channel_averaging_map: z.array(z.array(z.number())),
              channel_offset: z.number(),
              output_link_map: z.array(z.array(z.number()))
            }),
            z.object({
              fsp_id: z.number(),
              function_mode: z.string().trim(),
              frequency_slice_id: z.number(),
              integration_factor: z.number(),
              zoom_factor: z.number(),
              zoom_window_tuning: z.number()
            })
          ])
        )
      })
    })
  ),
  dish_allocations: z.object({ receptor_ids: z.array(z.string().trim()) }),
  dish_configurations: z.array(
    z.object({
      dish_configuration_id: z.string().trim(),
      receiver_band: z.string().trim()
    })
  ),
  target_beam_configurations: z.array(z.unknown()),
  subarray_beam_configurations: z.array(z.unknown()),
  mccs_allocation: z.null()
});

export type SkaMidType = z.infer<typeof SkaMidScheme>;
