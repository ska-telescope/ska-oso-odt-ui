import { z } from 'zod';

import {
  blankSdpConfigurationProcessingBlocks,
  SdpConfigurationProcessingBlocksScheme
} from './sdpConfigurationProcessingBlocks';
import {
  blankSdpConfigurationScanType,
  SdpConfigurationScanTypeScheme
} from './sdpConfigurationScanType';

export const SdpConfigurationScheme = z.object({
  eb_id: z.union([z.string().trim().optional(), z.null()]).default(null),
  max_length: z.union([z.number().optional(), z.null()]).default(null),
  scan_types: z
    .union([z.array(SdpConfigurationScanTypeScheme).optional(), z.null()])
    .default(null),
  processing_blocks: z
    .union([
      z.array(SdpConfigurationProcessingBlocksScheme).optional(),
      z.null()
    ])
    .default(null)
});
export type SdpConfigurationType = z.infer<typeof SdpConfigurationScheme>;

export const sampleSdpConfiguration: SdpConfigurationType = {
  eb_id: 'eb-mvp01-20200325-00001',
  max_length: 100.0,
  scan_types: [blankSdpConfigurationScanType],
  processing_blocks: [blankSdpConfigurationProcessingBlocks]
};

export const blankSdpConfiguration: SdpConfigurationType = {
  eb_id: null,
  max_length: null,
  scan_types: [blankSdpConfigurationScanType],
  processing_blocks: [blankSdpConfigurationProcessingBlocks]
};
