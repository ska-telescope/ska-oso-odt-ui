import { z } from 'zod';

export const ChannelAveragingScheme = z.object({
  channel_id: z.union([z.number().optional(), z.null()]).default(null),
  channel_averaging: z.union([z.number().optional(), z.null()]).default(null)
});

export type ChannelAveragingType = z.infer<typeof ChannelAveragingScheme>;

export type channelAveragingProps = {
  channel_id?: number | null;
  channel_averaging?: number | null;
};

export const blankChannelAveraging: ChannelAveragingType = {
  channel_id: null,
  channel_averaging: null
};
