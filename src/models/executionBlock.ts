import { z } from 'zod';

export const ExecutionBlockScheme = z.object({
  eb_id: z
    .union([z.string().regex(/^eb-[a-z0-9]+-[0-9]{8}-[a-z0-9]+$/), z.null()])
    .default(null),
  max_length: z.union([z.number().optional(), z.null()]).default(null)
});

export type ExecutionBlockType = z.infer<typeof ExecutionBlockScheme>;

export const sampleExecutionBlock: ExecutionBlockType = {
  eb_id: 'eb-mvp01-20200325-00001',
  max_length: 10
};

export const blankExecutionBlock: ExecutionBlockType = {
  eb_id: null,
  max_length: null
};
