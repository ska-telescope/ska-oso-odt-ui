import { z } from 'zod';

export enum TelescopeName {
  ska_low = 'ska_low',
  ska_mid = 'ska_mid'
}

export const TelescopeNameScheme = z
  .union([z.nativeEnum(TelescopeName).optional(), z.null()])
  .default(null);

export type TelescopeNameType = keyof typeof TelescopeNameScheme;
