export enum ParametersActive {
  CrossScanParameters = 'CrossScanParameters',
  RasterParameters = 'RasterParameters',
  SinglePointParameters = 'SinglePointParameters'
}
