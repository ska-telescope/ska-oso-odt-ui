import { z } from 'zod';

export const CoordinateScheme = z.object({
  kind: z.string().trim(),
  name: z.union([z.string().trim(), z.null()]).default(null),
  reference_frame: z.union([z.string().trim(), z.null()]).default(null),
  az: z.union([z.number(), z.null()]).default(null),
  el: z.union([z.number(), z.null()]).default(null),
  ra: z.union([z.number(), z.null()]).default(null),
  dec: z.union([z.number(), z.null()]).default(null),
  unit: z.union([z.array(z.string().trim()), z.null()]).default(null)
});

export type CoordinateType = z.infer<typeof CoordinateScheme>;

export const blankCoordinate: CoordinateType = {
  kind: null,
  name: null,
  reference_frame: null,
  az: null,
  el: null,
  ra: null,
  dec: null,
  unit: null
};
