import { z } from 'zod';

import {
  blankPatternParameters,
  PatternParametersScheme
} from './patternParameters';

export const PatternScheme = z.object({
  active: z.string().trim(),
  parameters: z.array(PatternParametersScheme.optional())
});

export type PatternType = z.infer<typeof PatternScheme>;

export const samplePattern: PatternType = {
  active: 'CrossScanParameters',
  parameters: [blankPatternParameters]
};

export const blankPattern: PatternType = {
  active: null,
  parameters: [blankPatternParameters]
};
