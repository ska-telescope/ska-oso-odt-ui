import { z } from 'zod';

export const PatternParametersScheme = z.object({
  kind: z.string().trim().default(''),
  offset_x_arcsec: z.union([z.number(), z.null()]).default(null),
  offset_y_arcsec: z.union([z.number(), z.null()]).default(null),
  row_length_arcsec: z.union([z.number(), z.null()]).default(null),
  row_offset_arcsec: z.union([z.number(), z.null()]).default(null),
  n_rows: z.union([z.number(), z.null()]).default(null),
  pa: z.union([z.number(), z.null()]).default(null),
  unidirectional: z.union([z.boolean(), z.null()]).default(null),
  row_offset_angle: z.union([z.number(), z.null()]).default(null),
  offset_arcsec: z.union([z.number(), z.null()]).default(null)
});

export type PatternParametersType = z.infer<typeof PatternParametersScheme>;

export const samplePatternParameters: PatternParametersType = {
  kind: 'CrossScanParameters',
  offset_x_arcsec: 12,
  offset_y_arcsec: 0,
  row_length_arcsec: 1,
  row_offset_arcsec: -1,
  n_rows: 1,
  unidirectional: false,
  row_offset_angle: 30.0,
  offset_arcsec: 0.0
};

export const blankPatternParameters: PatternParametersType = {
  kind: null,
  offset_x_arcsec: null,
  offset_y_arcsec: null,
  row_length_arcsec: null,
  row_offset_arcsec: null,
  n_rows: null,
  unidirectional: false,
  row_offset_angle: null,
  offset_arcsec: null
};
