import { z } from 'zod';

export const SdpConfigurationChannelsScheme = z.object({
  count: z.union([z.number().optional(), z.null()]).default(null),
  start: z.union([z.number().optional(), z.null()]).default(null),
  stride: z.union([z.number().optional(), z.null()]).default(null),
  freq_min: z.union([z.number().optional(), z.null()]).default(null),
  freq_max: z.union([z.number().optional(), z.null()]).default(null),
  link_map: z
    .union([z.array(z.array(z.number()).optional()), z.null()])
    .default([])
});

export type SdpConfigurationChannelsType = z.infer<
  typeof SdpConfigurationChannelsScheme
>;

export const sampleSdpConfigurationChannels: SdpConfigurationChannelsType = {
  count: 744,
  start: 0,
  stride: 2,
  freq_min: 0.35e9,
  freq_max: 0.368e9,
  link_map: [
    [0, 0],
    [200, 1],
    [744, 2],
    [944, 3]
  ]
};

export const blankSdpConfigurationChannels: SdpConfigurationChannelsType = {
  count: null,
  start: null,
  stride: null,
  freq_min: null,
  freq_max: null,
  link_map: [[]]
};
