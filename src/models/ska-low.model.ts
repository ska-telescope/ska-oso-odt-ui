import { z } from 'zod';

export const SkaLowScheme = z.object({
  interface: z.string().trim(),
  sbd_id: z.string().trim(),
  telescope: z.string().trim(),
  metadata: z.object({
    version: z.string(),
    created_by: z.string().trim(),
    created_on: z.string().trim(),
    last_modified_by: z.null(),
    last_modified_on: z.null()
  }),
  activities: z.object({
    allocate: z.object({
      kind: z.string().trim(),
      path: z.string().trim(),
      function_args: z.object({
        init: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        }),
        run: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        })
      })
    }),
    observe: z.object({
      kind: z.string().trim(),
      path: z.string().trim(),
      repo: z.string().trim(),
      branch: z.string().trim(),
      commit: z.string().trim(),
      function_args: z.object({
        init: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        }),
        run: z.object({
          args: z.array(z.string().trim()),
          kwargs: z.object({ argname: z.string().trim() })
        })
      })
    })
  }),
  scan_definitions: z.array(
    z.object({
      scan_definition_id: z.string().trim(),
      scan_duration: z.number(),
      target_beam_configurations: z.array(z.string().trim()),
      target: z.string().trim()
    })
  ),
  scan_sequence: z.array(z.string().trim()),
  targets: z.array(
    z.union([
      z.object({
        target_id: z.string().trim(),
        reference_coordinate: z.object({ kind: z.string().trim() }),
        pointing_pattern: z.object({
          active: z.string().trim(),
          parameters: z.array(
            z.object({
              kind: z.string().trim(),
              unidirectional: z.boolean(),
              offset_arcsec: z.number()
            })
          )
        })
      }),
      z.object({
        target_id: z.string().trim(),
        reference_coordinate: z.object({
          kind: z.string().trim(),
          reference_frame: z.string().trim(),
          az: z.number(),
          el: z.number()
        }),
        pointing_pattern: z.object({
          active: z.string().trim(),
          parameters: z.array(
            z.object({
              kind: z.string().trim(),
              row_length_arcsec: z.number(),
              row_offset_arcsec: z.number(),
              n_rows: z.number()
            })
          )
        })
      })
    ])
  ),
  dish_configurations: z.array(z.unknown()),
  dish_allocations: z.null(),
  mccs_allocation: z.object({
    subarray_beam_ids: z.array(z.string().trim()),
    station_ids: z.array(z.array(z.number())),
    channel_blocks: z.array(z.number())
  }),
  target_beam_configurations: z.array(
    z.object({
      target_beam_id: z.string().trim(),
      target: z.string().trim(),
      subarray_beam_configuration: z.string().trim()
    })
  ),
  csp_configurations: z.array(z.unknown()),
  sdp_configuration: z.null(),
  subarray_beam_configurations: z.array(
    z.object({
      subarray_beam_configuration_id: z.string().trim(),
      subarray_beam_id: z.string().trim(),
      update_rate: z.number(),
      antenna_weights: z.array(z.number()),
      phase_centre: z.array(z.number()),
      channels: z.array(z.array(z.number()))
    })
  )
});

export type SkaLowType = z.infer<typeof SkaLowScheme>;
