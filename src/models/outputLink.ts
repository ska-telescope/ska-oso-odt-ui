import { z } from 'zod';

export const OutputLinkScheme = z.object({
  channelId: z.union([z.string().trim(), z.null()]).default(null),
  linkId: z.union([z.string().trim(), z.null()]).default(null)
});

export type OutputLinkType = z.infer<typeof OutputLinkScheme>;

export const sampleOutputLink: OutputLinkType = {
  channelId: 'channel #1',
  linkId: 'link #1'
};

export const blankOutputLink: OutputLinkType = {
  channelId: null,
  linkId: null
};
