import { z } from 'zod';
import { SubArrayLOW } from '../../generated/models/sub-array-low';
import { SubarrayOptionType } from '../components/sb/array/common';

/**
 * Mid array configuration schemas
 */

export const DefaultMidArrayAA05 = ['SKA001', 'SKA036', 'SKA063', 'SKA100'];

export enum MidArrayConfigTypeEnum {
  AA05 = 'AA0.5',
  Custom = 'Custom'
}

export const MidArrayConfigTypeOptions: SubarrayOptionType[] = Object.keys(
  MidArrayConfigTypeEnum
).map((item) => ({
  label: MidArrayConfigTypeEnum[item as keyof typeof MidArrayConfigTypeEnum],
  value: MidArrayConfigTypeEnum[item as keyof typeof MidArrayConfigTypeEnum]
}));

const dishesRegex = RegExp(
  `((^|, )(SKA((?!000)0[0-9][0-9]|1[0-2][0-9]|13[0-3])|MKT0([0-5][0-9]|6[0-3])))+$`
);
const atLeastOneDishSchema = z.string().refine(
  (value) => {
    return value.trim() !== '' && value.split(',').length > 1;
  },
  {
    message: 'At least two valid dishes must be selected',
    path: []
  }
);

// Schema to ensure the items match the pattern and are comma-separated
const dishesPatternSchema = z.string().refine(
  (value) => {
    return dishesRegex.test(value);
  },
  {
    message:
      'Dishes id list must be comma-separated and match the pattern SKA001+ or  MKT000+',
    path: []
  }
);

export const MidArrayConfigSchema = z.object({
  configuration: z.nativeEnum(MidArrayConfigTypeEnum),
  dishes: atLeastOneDishSchema.and(dishesPatternSchema)
});

export type MidArrayConfigType = z.infer<typeof MidArrayConfigSchema>;

/**
 * Low array configuration schemas
 */

export const LowArrayAA05P1 = [345, 350, 352, 431];
export const LowArrayAA05P2 = [345, 350, 352, 355, 431, 434];

export const DEFAULT_WEIGHTING_KEY_REF = 'uniform';
export const DEFAULT_MCCS_ALLOCATION_ID = 'mccs-allocation-78319';
export const SUBARRAY_BEAM_STRING_ID_PREFIX = 'Subarray Beam ';

export const DEFAULT_DISH_ALLOCATION_ID = 'dish-allocation-95719';

export enum LowArrayConfigTypeEnum {
  AA05P1 = 'AA0.5 (Phase 1)',
  AA05P2 = 'AA0.5 (Phase 2)',
  Custom = 'Custom'
}

export const LowArrayConfigurationEnumMapping = {
  [LowArrayConfigTypeEnum.AA05P1]: SubArrayLOW.Aa05,
  [LowArrayConfigTypeEnum.AA05P2]: SubArrayLOW.Aa05,
  [LowArrayConfigTypeEnum.Custom]: SubArrayLOW.Custom
};

export const LowArrayConfigurationToStationMapping = {
  [LowArrayConfigTypeEnum.AA05P1]: LowArrayAA05P1,
  [LowArrayConfigTypeEnum.AA05P2]: LowArrayAA05P2,
  [LowArrayConfigTypeEnum.Custom]: LowArrayAA05P1
};

export const LowArrayConfigTypeOptions: SubarrayOptionType[] = Object.keys(
  LowArrayConfigTypeEnum
).map((item) => ({
  label: LowArrayConfigTypeEnum[item as keyof typeof LowArrayConfigTypeEnum],
  value: LowArrayConfigTypeEnum[item as keyof typeof LowArrayConfigTypeEnum]
}));

// Schema to ensure the stations match the pattern and are comma-separated
const stationsRegex = RegExp(
  `((^|, )(C([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-1][0-9]|22[0-4])|[ENS]([1-9]|1[0-6])-[1-6]))+$`
);
const stationsPatternSchema = z.string().refine(
  (value) => {
    return stationsRegex.test(value);
  },
  {
    message: 'Station ID field must be comma-separated list of station IDs',
    path: []
  }
);

// Schema to ensure at least one station id
const atLeastOneStationSchema = z.string().refine(
  (value) => {
    return value.trim() !== '';
  },
  {
    message: 'At least one station id is required',
    path: []
  }
);

export const ApertureSchema = z.object({
  substationID: z.number(),
  weightingKey: z.string()
});
export type ApertureType = z.infer<typeof ApertureSchema>;

export const StationApertureSchema = z.record(
  stationsPatternSchema,
  z.array(ApertureSchema)
);
export type StationApertureType = z.infer<typeof StationApertureSchema>;

export const SubarrayBeamSchema = z.object({
  subarrayBeamID: z.string(),
  apertures: StationApertureSchema
});
export type SubarrayBeamType = z.infer<typeof SubarrayBeamSchema>;

export const LowArrayConfigSchema = z.object({
  configuration: z.nativeEnum(LowArrayConfigTypeEnum),
  stations: atLeastOneStationSchema.and(stationsPatternSchema),
  subarrayBeam: SubarrayBeamSchema
});

export type LowArrayConfigType = z.infer<typeof LowArrayConfigSchema>;

/**
 * TODO: Code block to generate the mapping of numeric station IDs to string IDs.
 *  The conversion logic is base on a Python version in a Jupyter notebook linked
 *  in the comments of the ADR-62 Confluence page:
 *  https://confluence.skatelescope.org/display/SWSI/ADR-62+Agree+standard+naming+and+identifications+for+LOW+Stations+and+sub-stations
 *  This should be a temporary fix and the mapping should in the future be pulled
 *  from OSD or tmdata or similar.
 */
interface ChunkSpec {
  firstID: number;
  lastID: number;
  firstCluster: number | null;
  lastCluster: number | null;
  labels: string[];
}

const chunks: ChunkSpec[] = [
  {
    firstID: 1,
    lastID: 224,
    firstCluster: null,
    lastCluster: null,
    labels: ['C']
  },
  {
    firstID: 225,
    lastID: 296,
    firstCluster: 1,
    lastCluster: 4,
    labels: ['E', 'N', 'S']
  },
  {
    firstID: 297,
    lastID: 386,
    firstCluster: 5,
    lastCluster: 9,
    labels: ['E', 'S', 'N']
  },
  {
    firstID: 387,
    lastID: 512,
    firstCluster: 10,
    lastCluster: 16,
    labels: ['E', 'S', 'N']
  }
];

export const stationIDMap: Map<number, string> = createStationIDMap(
  Array.from({ length: 512 }, (_, i) => i + 1)
);

export function createStationIDMap(stationIDs: number[]): Map<number, string> {
  const stationMap = new Map<number, string>();
  stationIDs.forEach((stationID) => {
    const chunk = chunks.find(
      (obj) => stationID >= obj.firstID && stationID <= obj.lastID
    );
    if (chunk) {
      if (!chunk.firstCluster || !chunk.lastCluster) {
        // This range is not in clusters. Just prepend the label to the ID
        stationMap.set(stationID, chunk.labels[0] + stationID);
      } else {
        // This range is in clusters.
        const clusters = chunk.lastCluster - chunk.firstCluster + 1;
        const offset = stationID - chunk.firstID;
        const block = Math.floor(Math.floor(offset / 6) / clusters);
        const clusterNumber = Math.floor(offset / 6) % clusters;
        stationMap.set(
          stationID,
          chunk.labels[block] +
            (chunk.firstCluster + clusterNumber) +
            '-' +
            ((offset % 6) + 1)
        );
      }
    }
  });
  return stationMap;
}

export function getLowDefaultConfig(
  lowArrayConfigInput: LowArrayConfigTypeEnum = LowArrayConfigTypeEnum.AA05P1
): LowArrayConfigType {
  // Get default configuration when a new array is selected
  const arrayStations =
    LowArrayConfigurationToStationMapping[lowArrayConfigInput];
  const aperturesForStations: StationApertureType = {};
  arrayStations.forEach((station) => {
    aperturesForStations[stationIDMap.get(station)!] = [
      {
        substationID: 1,
        weightingKey: DEFAULT_WEIGHTING_KEY_REF
      }
    ];
  });
  return {
    configuration: lowArrayConfigInput,
    stations: arrayStations
      .map((station) => stationIDMap.get(station))
      .join(', '),
    subarrayBeam: {
      subarrayBeamID: SUBARRAY_BEAM_STRING_ID_PREFIX + '1',
      apertures: aperturesForStations
    }
  };
}
