import { z } from 'zod';

export const DishAllocationsScheme = z.object({
  receptor_ids: z
    .union([z.array(z.string().trim()).optional(), z.null()])
    .default(null)
});

export type DishAllocationsType = z.infer<typeof DishAllocationsScheme>;

export const blankDishAllocations: DishAllocationsType = {
  receptor_ids: []
};
