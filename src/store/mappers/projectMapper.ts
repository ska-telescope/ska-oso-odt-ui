import { ProjectInput } from '../../generated/models/project-input';
import { ProjectType } from '../../models/project/project';
import { ObservingBlock } from '../../generated/models/observing-block';
import {
  ObsBlockSbDefinitionSchema,
  ObservingBlockType
} from '../../models/project/observingBlock';

export const mapPdmToProject = (pdmProject: ProjectInput): ProjectType => {
  const observingBlocks = (pdmProject.obs_blocks ?? []).map(
    fromPdmObservingBlock
  );

  return {
    name: pdmProject.name,
    code: pdmProject.prj_id,
    type: 'Science',
    status: 'TODO',
    observingBlocks
  };
};

export const mapProjectToPdm = (project: ProjectType): ProjectInput => {
  const observingBlocks = (project.observingBlocks ?? []).map(
    toPdmObservingBlock
  );

  return {
    prj_id: project.code,
    name: project.name,
    obs_blocks: observingBlocks
  };
};

const fromPdmObservingBlock = (
  pdmObservingBlock: ObservingBlock
): ObservingBlockType => {
  const sbDefinitions = (pdmObservingBlock.sbd_ids ?? []).map((sbdId) =>
    ObsBlockSbDefinitionSchema.parse({ id: sbdId })
  );
  return {
    id: pdmObservingBlock.obs_block_id,
    name: pdmObservingBlock.name,
    status: 'TODO',
    sbDefinitions
  };
};

const toPdmObservingBlock = (
  observingBlock: ObservingBlockType
): ObservingBlock => {
  const sbd_ids = observingBlock.sbDefinitions.map(
    (sbDefinition) => sbDefinition.id
  );
  return {
    obs_block_id: observingBlock.id,
    name: observingBlock.name,
    sbd_ids
  };
};
