import type { SBDefinitionInput } from '../../generated/models/sbdefinition-input';
import { SbDefinitionType } from '../../models/sbDefinition/sbDefinition';
import { toGeneral } from './generalMapper';
import { FromPdm } from './targetsMapper';
import {
  toLowSignalProcessor,
  toMidSignalProcessor
} from './signalProcessorMapper';
import { Activities } from '../../generated/models/activities';
import { toScript } from './scriptMapper';
import {
  mapPdmToScanDefinitionModel,
  mapPdmToScanSequenceModel
} from './scansMapper';
import { TelescopeType } from '../../generated/models/telescope-type';
import { mapPdmToLowArrayModel, mapPdmToMidArrayModel } from './arrayMapper';

const telescopeIsMid = (sbDefinition: SBDefinitionInput) =>
  sbDefinition.telescope == TelescopeType.SkaMid;

export const toSbDefinition = (
  sbDefinition: SBDefinitionInput
): SbDefinitionType => {
  const general = toGeneral(sbDefinition);
  const array = telescopeIsMid(sbDefinition)
    ? mapPdmToMidArrayModel(sbDefinition.dish_allocations ?? {})
    : mapPdmToLowArrayModel(sbDefinition.mccs_allocation ?? {});

  const targets = (sbDefinition.targets ?? []).map(FromPdm.convert);
  const signalProcessor = (sbDefinition.csp_configurations ?? []).map(
    telescopeIsMid(sbDefinition) ? toMidSignalProcessor : toLowSignalProcessor
  );
  const activities: Record<string, Activities> = sbDefinition.activities ?? {};
  const script = toScript(activities);
  const scans = {
    scanDefinitions: (sbDefinition.scan_definitions ?? []).map(
      mapPdmToScanDefinitionModel
    ),
    scanSequence: (sbDefinition.scan_sequence ?? []).map((scanSequenceId) =>
      mapPdmToScanSequenceModel(
        scanSequenceId,
        sbDefinition.scan_definitions,
        sbDefinition.targets
      )
    )
  };
  return { general, array, targets, script, scans, signalProcessor };
};
