import { ScanDefinitionType, ScanSequenceItemType } from '../../models';
import { ScanDefinition } from '../../generated/models/scan-definition';
import { Target } from '../../generated/models/target';
import { useStore } from '../store';

export const mapPdmToScanDefinitionModel = (
  scanDefinition: ScanDefinition
): ScanDefinitionType => {
  return {
    scanDefinitionId: scanDefinition.scan_definition_id,
    target: scanDefinition.target_ref,
    csp: scanDefinition.csp_configuration_ref,
    durationS: parseFloat((scanDefinition.scan_duration_ms * 1e-3).toFixed(3))
  };
};

export const mapPdmToScanSequenceModel = (
  scanSequenceId: string,
  scanDefinitions: ScanDefinition[],
  targets: Target[]
): ScanSequenceItemType => {
  const scanDefinition = scanDefinitions.find(
    (scanDefinition) => scanDefinition.scan_definition_id === scanSequenceId
  );

  return {
    scanDefinitionId: scanDefinition.scan_definition_id,
    target: targets.find(
      (target) => target.target_id === scanDefinition.target_ref
    )?.name
  };
};

export const mapScanDefinitionModelToMidPdm = (
  scanDefinition: ScanDefinitionType
): ScanDefinition => {
  return {
    scan_definition_id: scanDefinition.scanDefinitionId,
    target_ref: scanDefinition.target,
    csp_configuration_ref: scanDefinition.csp,
    scan_duration_ms: parseFloat((scanDefinition.durationS * 1e3).toFixed(3)),
    pointing_correction: 'MAINTAIN',
    dish_allocation_ref:
      useStore.getState().sbDefinition.dish_allocations?.dish_allocation_id ??
      ''
  };
};

export const mapScanDefinitionModelToLowPdm = (
  scanDefinition: ScanDefinitionType
): ScanDefinition => {
  return {
    scan_definition_id: scanDefinition.scanDefinitionId,
    target_ref: scanDefinition.target,
    csp_configuration_ref: scanDefinition.csp,
    scan_duration_ms: parseFloat((scanDefinition.durationS * 1e3).toFixed(3)),
    pointing_correction: 'MAINTAIN',
    mccs_allocation_ref:
      useStore.getState().sbDefinition.mccs_allocation?.mccs_allocation_id ?? ''
  };
};
