import {
  DEFAULT_DISH_ALLOCATION_ID,
  DEFAULT_MCCS_ALLOCATION_ID,
  DEFAULT_WEIGHTING_KEY_REF,
  LowArrayConfigType,
  LowArrayConfigTypeEnum,
  LowArrayConfigurationEnumMapping,
  MidArrayConfigType,
  MidArrayConfigTypeEnum,
  StationApertureType,
  stationIDMap,
  SUBARRAY_BEAM_STRING_ID_PREFIX,
  SubarrayBeamType
} from '../../models/array/arrayConfig';
import { SubArrayLOW } from '../../generated/models/sub-array-low';
import { SubarrayBeamConfiguration } from '../../generated/models/subarray-beam-configuration';
import { Aperture } from '../../generated/models/aperture';
import { DishAllocation } from '../../generated/models/dish-allocation';
import { MCCSAllocation } from '../../generated/models/mccsallocation';

export function mapMidArrayModelToPdm(midArrayConfigInput: MidArrayConfigType) {
  return {
    dish_allocation_id: DEFAULT_DISH_ALLOCATION_ID,
    selected_subarray_definition: midArrayConfigInput.configuration,
    dish_ids: midArrayConfigInput.dishes?.split(/\s*,\s*/).filter(Boolean)
  };
}

export const mapPdmToMidArrayModel = (
  dishAllocation: DishAllocation
): MidArrayConfigType => {
  const dishIDs = dishAllocation.dish_ids ?? [];
  return {
    configuration:
      dishAllocation.selected_subarray_definition === 'AA0.5'
        ? MidArrayConfigTypeEnum.AA05
        : MidArrayConfigTypeEnum.Custom,
    dishes: dishIDs.join(', ')
  };
};

export function mapLowArrayModelToPdm(
  lowArrayConfigInput: LowArrayConfigType
): MCCSAllocation {
  const convertedApertureList: Aperture[] = [];
  Object.keys(lowArrayConfigInput.subarrayBeam.apertures).forEach((station) => {
    lowArrayConfigInput.subarrayBeam.apertures[station].forEach((aperture) => {
      convertedApertureList.push({
        station_id: [...stationIDMap.keys()].find(
          (k) => stationIDMap.get(k) == station
        ),
        substation_id: aperture.substationID ?? 1,
        weighting_key: aperture.weightingKey ?? DEFAULT_WEIGHTING_KEY_REF
      });
    });
  });
  return {
    mccs_allocation_id: DEFAULT_MCCS_ALLOCATION_ID,
    selected_subarray_definition:
      LowArrayConfigurationEnumMapping[lowArrayConfigInput.configuration],
    subarray_beams: [
      {
        subarray_beam_id: Number(
          lowArrayConfigInput.subarrayBeam.subarrayBeamID.substring(
            SUBARRAY_BEAM_STRING_ID_PREFIX.length,
            lowArrayConfigInput.subarrayBeam.subarrayBeamID.length
          )
        ),
        number_of_channels: 96, // TODO: remove once optional in PDM
        apertures: convertedApertureList
      } as SubarrayBeamConfiguration
    ]
  };
}

export const mapPdmToLowArrayModel = (
  mccsAllocation: MCCSAllocation
): LowArrayConfigType => {
  let arrayName = LowArrayConfigTypeEnum.Custom;
  // If array configuration type is AA0.5, determine if it is Phase 1 or Phase 2
  if (mccsAllocation!.selected_subarray_definition === SubArrayLOW.Aa05) {
    arrayName =
      mccsAllocation!.subarray_beams[0].apertures.length === 4
        ? LowArrayConfigTypeEnum.AA05P1
        : LowArrayConfigTypeEnum.AA05P2;
  }

  const aperturesForStations: StationApertureType = {};
  mccsAllocation!.subarray_beams[0].apertures.forEach((aperture) => {
    if (stationIDMap.get(aperture.station_id)! in aperturesForStations) {
      aperturesForStations[stationIDMap.get(aperture.station_id)!].push({
        substationID: aperture.substation_id,
        weightingKey: aperture.weighting_key ?? DEFAULT_WEIGHTING_KEY_REF
      });
    } else {
      aperturesForStations[stationIDMap.get(aperture.station_id)!] = [
        {
          substationID: aperture.substation_id,
          weightingKey: aperture.weighting_key ?? DEFAULT_WEIGHTING_KEY_REF
        }
      ];
    }
  });

  return {
    configuration: arrayName,
    stations:
      mccsAllocation!.subarray_beams[0]?.apertures
        ?.map((station) => stationIDMap.get(station.station_id))
        .join(', ') ?? '',
    subarrayBeam: {
      subarrayBeamID:
        SUBARRAY_BEAM_STRING_ID_PREFIX +
        mccsAllocation!.subarray_beams[0].subarray_beam_id,
      apertures: aperturesForStations
    } as SubarrayBeamType
  };
};
