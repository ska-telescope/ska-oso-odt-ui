import { describe, test, expect } from 'vitest';
import {
  LowArrayConfigTypeEnum,
  MidArrayConfigTypeEnum,
  LowArrayConfigType,
  MidArrayConfigType,
  DEFAULT_WEIGHTING_KEY_REF,
  DEFAULT_MCCS_ALLOCATION_ID,
  SUBARRAY_BEAM_STRING_ID_PREFIX,
  DEFAULT_DISH_ALLOCATION_ID
} from '../../models/array/arrayConfig';
import { MCCSAllocation } from '../../generated/models/mccsallocation';
import { DishAllocation } from '../../generated/models/dish-allocation';
import {
  mapLowArrayModelToPdm,
  mapMidArrayModelToPdm,
  mapPdmToLowArrayModel,
  mapPdmToMidArrayModel
} from './arrayMapper';

const EXAMPLE_MID_ARRAY_CONFIG: DishAllocation = {
  dish_allocation_id: 'dish-allocation-95719',
  selected_subarray_definition: 'AA0.5',
  dish_ids: ['SKA001', 'SKA036', 'SKA063', 'SKA100']
};

const EXAMPLE_MID_ARRAY_INPUT: MidArrayConfigType = {
  configuration: MidArrayConfigTypeEnum.AA05,
  dishes: 'SKA001, SKA036, SKA063, SKA100'
};

const EXAMPLE_MID_CUSTOM_ARRAY_CONFIG: DishAllocation = {
  dish_allocation_id: DEFAULT_DISH_ALLOCATION_ID,
  selected_subarray_definition: 'Custom',
  dish_ids: ['SKA036', 'SKA063']
};

const EXAMPLE_MID_CUSTOM_ARRAY_INPUT: MidArrayConfigType = {
  configuration: MidArrayConfigTypeEnum.Custom,
  dishes: 'SKA036, SKA063'
};

export const EXAMPLE_LOW_ARRAY_INPUT: LowArrayConfigType = {
  configuration: LowArrayConfigTypeEnum.AA05P1,
  stations: 'S8-1, S8-6, S9-2, S10-3',
  subarrayBeam: {
    subarrayBeamID: SUBARRAY_BEAM_STRING_ID_PREFIX + '1',
    apertures: {
      'S8-1': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S8-6': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S9-2': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S10-3': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ]
    }
  }
};

export const EXAMPLE_LOW_AA05P2_ARRAY_INPUT: LowArrayConfigType = {
  configuration: LowArrayConfigTypeEnum.AA05P2,
  stations: 'S8-1, S8-6, S9-2, S9-5, S10-3, S10-6',
  subarrayBeam: {
    subarrayBeamID: SUBARRAY_BEAM_STRING_ID_PREFIX + '1',
    apertures: {
      'S8-1': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S8-6': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S9-2': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S9-5': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S10-3': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ],
      'S10-6': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ]
    }
  }
};

const EXAMPLE_LOW_CUSTOM_ARRAY_INPUT: LowArrayConfigType = {
  configuration: LowArrayConfigTypeEnum.Custom,
  stations: 'S8-6',
  subarrayBeam: {
    subarrayBeamID: SUBARRAY_BEAM_STRING_ID_PREFIX + '1',
    apertures: {
      'S8-6': [
        {
          substationID: 1,
          weightingKey: DEFAULT_WEIGHTING_KEY_REF
        }
      ]
    }
  }
};

export const EXAMPLE_LOW_ARRAY_CONFIG: MCCSAllocation = {
  mccs_allocation_id: DEFAULT_MCCS_ALLOCATION_ID,
  selected_subarray_definition: 'AA0.5',
  subarray_beams: [
    {
      subarray_beam_id: 1,
      number_of_channels: 96, // TODO: Placeholder just for now, remove when PDM is released
      apertures: [
        {
          station_id: 345,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 350,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 352,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 431,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        }
      ]
    }
  ]
};

const EXAMPLE_LOW_AA05P2_ARRAY_CONFIG: MCCSAllocation = {
  mccs_allocation_id: DEFAULT_MCCS_ALLOCATION_ID,
  selected_subarray_definition: 'AA0.5',
  subarray_beams: [
    {
      subarray_beam_id: 1,
      number_of_channels: 96, // TODO: Placeholder just for now, remove when PDM is released
      apertures: [
        {
          station_id: 345,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 350,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 352,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 355,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 431,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        },
        {
          station_id: 434,
          substation_id: 1,
          weighting_key: DEFAULT_WEIGHTING_KEY_REF
        }
      ]
    }
  ]
};

const EXAMPLE_LOW_CUSTOM_ARRAY_CONFIG: MCCSAllocation = {
  mccs_allocation_id: DEFAULT_MCCS_ALLOCATION_ID,
  selected_subarray_definition: 'Custom',
  subarray_beams: [
    {
      subarray_beam_id: 1,
      number_of_channels: 96, // TODO: Placeholder just for now, remove when PDM is released
      apertures: [
        {
          station_id: 350,
          substation_id: 1,
          weighting_key: 'uniform'
        }
      ]
    }
  ]
};

describe('PDM to Array mapper', () => {
  test.each([
    [EXAMPLE_LOW_AA05P2_ARRAY_CONFIG, EXAMPLE_LOW_AA05P2_ARRAY_INPUT],
    [EXAMPLE_LOW_CUSTOM_ARRAY_CONFIG, EXAMPLE_LOW_CUSTOM_ARRAY_INPUT]
  ])(
    'should map the MCCSAllocation to the Low Array Type',
    (pdmMccsAllocation: MCCSAllocation, expectedOutput: LowArrayConfigType) => {
      const lowArrayConfig = mapPdmToLowArrayModel(pdmMccsAllocation);
      expect(lowArrayConfig).toStrictEqual(expectedOutput);
    }
  );

  test.each([
    [EXAMPLE_MID_CUSTOM_ARRAY_CONFIG, EXAMPLE_MID_CUSTOM_ARRAY_INPUT],
    [EXAMPLE_MID_ARRAY_CONFIG, EXAMPLE_MID_ARRAY_INPUT]
  ])(
    'should map the DishAllocation to the Mid Array Type',
    (pdmDishAllocation: DishAllocation, expectedOutput: MidArrayConfigType) => {
      const midArrayConfig = mapPdmToMidArrayModel(pdmDishAllocation);
      expect(midArrayConfig).toStrictEqual(expectedOutput);
    }
  );
});

describe('Array to PDM mapper', () => {
  test.each([
    [EXAMPLE_LOW_ARRAY_INPUT, EXAMPLE_LOW_ARRAY_CONFIG],
    [EXAMPLE_LOW_AA05P2_ARRAY_INPUT, EXAMPLE_LOW_AA05P2_ARRAY_CONFIG],
    [EXAMPLE_LOW_CUSTOM_ARRAY_INPUT, EXAMPLE_LOW_CUSTOM_ARRAY_CONFIG]
  ])(
    'should map the Low Array type to an MCCSAllocation',
    (
      lowArrayConfigType: LowArrayConfigType,
      expectedOutput: MCCSAllocation
    ) => {
      const pdmMccsAllocation = mapLowArrayModelToPdm(lowArrayConfigType);
      expect(pdmMccsAllocation).toStrictEqual(expectedOutput);
    }
  );

  test.each([
    [EXAMPLE_MID_ARRAY_INPUT, EXAMPLE_MID_ARRAY_CONFIG],
    [EXAMPLE_MID_CUSTOM_ARRAY_INPUT, EXAMPLE_MID_CUSTOM_ARRAY_CONFIG]
  ])(
    'should map the Mid Array type to an DishAllocation',
    (
      midArrayConfigType: MidArrayConfigType,
      expectedOutput: DishAllocation
    ) => {
      const pdmDishAllocation = mapMidArrayModelToPdm(midArrayConfigType);
      expect(pdmDishAllocation).toStrictEqual(expectedOutput);
    }
  );
});
