import { describe, test } from 'vitest';
import { expect } from 'vitest';
import { Activities } from '../../generated/models/activities';
import { ScriptKind, ScriptType } from '../../models/script/script';
import { fromScript, toScript } from './scriptMapper';

const EXAMPLE_GIT_SCRIPT_WITH_BRANCH: ScriptType = {
  kind: ScriptKind.git,
  branch: 'example-git-branch',
  commit: '',
  selected: 'branch',
  repo: 'https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git',
  path: 'scripts/allocate_and_observe_sb.py',
  initArgs: [],
  mainArgs: []
};

const EXAMPLE_SBD_GIT_ACTIVITY_WITH_BRANCH: Record<string, Activities> = {
  observe: {
    kind: 'git',
    branch: 'example-git-branch',
    repo: 'https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git',
    path: 'git://scripts/allocate_and_observe_sb.py',
    function_args: {
      init: {
        kwargs: {},
        args: []
      },
      main: {
        kwargs: {},
        args: []
      }
    }
  }
};

const EXAMPLE_GIT_SCRIPT_WITH_ARGS: ScriptType = {
  kind: ScriptKind.git,
  branch: 'example-git-branch',
  commit: '',
  selected: 'branch',
  repo: 'https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git',
  path: 'scripts/allocate_and_observe_sb.py',
  initArgs: [
    { kw: 'arg1', value: 'value1' },
    { kw: 'arg2', value: 'value2' }
  ],
  mainArgs: [
    { kw: 'subarray_id', value: 1 },
    { kw: 'arg4', value: 'value4' }
  ]
};

const EXAMPLE_SBD_GIT_ACTIVITY_WITH_ARGS: Record<string, Activities> = {
  observe: {
    kind: 'git',
    branch: 'example-git-branch',
    repo: 'https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git',
    path: 'git://scripts/allocate_and_observe_sb.py',
    function_args: {
      init: {
        kwargs: {
          arg1: 'value1',
          arg2: 'value2'
        },
        args: []
      },
      main: {
        kwargs: {
          subarray_id: 1,
          arg4: 'value4'
        },
        args: []
      }
    }
  }
};

const EXAMPLE_GIT_SCRIPT_WITH_COMMIT: ScriptType = {
  kind: ScriptKind.git,
  commit: '867294e6',
  branch: 'master',
  selected: 'commit',
  repo: 'https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git',
  path: 'scripts/allocate_and_observe_sb.py',
  initArgs: [],
  mainArgs: []
};

const EXAMPLE_SBD_GIT_ACTIVITY_WITH_COMMIT: Record<string, Activities> = {
  observe: {
    kind: 'git',
    commit: '867294e6',
    repo: 'https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git',
    path: 'git://scripts/allocate_and_observe_sb.py',
    function_args: {
      init: {
        kwargs: {},
        args: []
      },
      main: {
        kwargs: {},
        args: []
      }
    }
  }
};

const EXAMPLE_FILESYSTEM_SCRIPT: ScriptType = {
  kind: ScriptKind.filesystem,
  path: 'scripts/allocate_and_observe_sb.py',
  initArgs: [],
  mainArgs: []
};

const EXAMPLE_SBD_FILESYSTEM_ACTIVITY: Record<string, Activities> = {
  observe: {
    kind: 'filesystem',
    path: 'file://scripts/allocate_and_observe_sb.py',
    function_args: {
      init: {
        kwargs: {},
        args: []
      },
      main: {
        kwargs: {},
        args: []
      }
    }
  }
};

describe('Mappers between Script and PDM Activity types', () => {
  test.each([
    [EXAMPLE_GIT_SCRIPT_WITH_BRANCH, EXAMPLE_SBD_GIT_ACTIVITY_WITH_BRANCH],
    [EXAMPLE_GIT_SCRIPT_WITH_COMMIT, EXAMPLE_SBD_GIT_ACTIVITY_WITH_COMMIT],
    [EXAMPLE_GIT_SCRIPT_WITH_ARGS, EXAMPLE_SBD_GIT_ACTIVITY_WITH_ARGS],
    [EXAMPLE_FILESYSTEM_SCRIPT, EXAMPLE_SBD_FILESYSTEM_ACTIVITY]
  ])(
    'should map different script types to correct PDM Activity objects',
    (formInput, expectedActivity) => {
      const activity = fromScript(formInput);
      expect(activity).toStrictEqual(expectedActivity);

      const script = toScript(expectedActivity);
      expect(script).toStrictEqual(formInput);
    }
  );
});
