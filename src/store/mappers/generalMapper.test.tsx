import { describe, test, expect } from 'vitest';
import { GeneralType } from '../../models';
import { MetadataInput } from '../../generated/models/metadata-input';
import { fromGeneral, toGeneral } from './generalMapper';
import { SBDefinitionInput } from '../../generated/models/sbdefinition-input';

const EXAMPLE_SBDEFINITION_ID: string = 'sbd-example-123';

const EXAMPLE_GENERAL: GeneralType = {
  sbdId: EXAMPLE_SBDEFINITION_ID,
  name: 'SBD 123',
  description: 'SBD 123 Description',
  version: 1,
  createdBy: 'Example User',
  createdOn: '2024-12-12',
  lastModifiedBy: 'Example User',
  lastModifiedOn: '2024-12-12'
};

const EXAMPLE_PDM_METADATA_CONFIG: MetadataInput = {
  version: 1,
  created_by: 'Example User',
  created_on: '2024-12-12',
  last_modified_by: 'Example User',
  last_modified_on: '2024-12-12'
};

const EXAMPLE_PDM_SB_DEFINITION: SBDefinitionInput = {
  sbd_id: EXAMPLE_SBDEFINITION_ID,
  name: 'SBD 123',
  description: 'SBD 123 Description',
  metadata: EXAMPLE_PDM_METADATA_CONFIG
};

describe('PDM to General mappers', () => {
  test('should map PDM General', () => {
    const general = toGeneral(EXAMPLE_PDM_SB_DEFINITION);
    expect(general).toStrictEqual(EXAMPLE_GENERAL);
  });
});

describe('General to PDM mappers', () => {
  test('should map General to PDM', () => {
    const sbDefinition = fromGeneral(EXAMPLE_GENERAL);
    expect(sbDefinition).toStrictEqual(EXAMPLE_PDM_SB_DEFINITION);
  });
});
