import { describe, test, expect } from 'vitest';
import { ScanDefinition } from '../../generated/models/scan-definition';
import { Target } from '../../generated/models/target';
import {
  ScanDefinitionType,
  ScanDefinitionsType,
  ScanSequenceItemType
} from '../../models';
import {
  mapPdmToScanDefinitionModel,
  mapPdmToScanSequenceModel,
  mapScanDefinitionModelToLowPdm,
  mapScanDefinitionModelToMidPdm
} from './scansMapper';

const EXAMPLE_PDM_LOW_SCAN_DEFINITION: ScanDefinition = {
  scan_definition_id: 'scan-definition-xyz',
  target_ref: 'target-abc',
  scan_duration_ms: 1400,
  csp_configuration_ref: 'csp-config-123',
  pointing_correction: 'MAINTAIN',
  mccs_allocation_ref: ''
};

const EXAMPLE_PDM_MID_SCAN_DEFINITION: ScanDefinition = {
  scan_definition_id: 'scan-definition-xyz',
  target_ref: 'target-abc',
  scan_duration_ms: 1400,
  csp_configuration_ref: 'csp-config-123',
  pointing_correction: 'MAINTAIN',
  dish_allocation_ref: ''
};

const EXAMPLE_ZOD_SCAN_DEFINITION: ScanDefinitionType = {
  scanDefinitionId: 'scan-definition-xyz',
  csp: 'csp-config-123',
  target: 'target-abc',
  durationS: 1.4
};

const EXAMPLE_ZOD_SCAN_SEQUENCE: ScanSequenceItemType = {
  scanDefinitionId: 'scan-definition-xyz',
  target: 'Target abc'
};

const EXAMPLE_TARGETS: Target[] = [
  { target_id: 'target-abc', name: 'Target abc' }
];

describe('PDM to ScanDefinitions mapper', () => {
  test.each([
    [EXAMPLE_PDM_LOW_SCAN_DEFINITION, EXAMPLE_ZOD_SCAN_DEFINITION],
    [EXAMPLE_PDM_MID_SCAN_DEFINITION, EXAMPLE_ZOD_SCAN_DEFINITION]
  ])(
    'should return the display model for ScanDefinition converted from the store',
    (initialState: ScanDefinition, expectedOutput: ScanDefinitionsType) => {
      const scanDefinitions = mapPdmToScanDefinitionModel(initialState);
      expect(scanDefinitions).toStrictEqual(expectedOutput);
    }
  );
});

describe('ScanDefinitions to PDM mapper', () => {
  test('it should correctly map to Low', () => {
    const scanDefinitions = mapScanDefinitionModelToLowPdm(
      EXAMPLE_ZOD_SCAN_DEFINITION
    );
    expect(scanDefinitions).toStrictEqual(EXAMPLE_PDM_LOW_SCAN_DEFINITION);
  });

  test('it should correctly map to Mid', () => {
    const scanDefinitions = mapScanDefinitionModelToMidPdm(
      EXAMPLE_ZOD_SCAN_DEFINITION
    );
    expect(scanDefinitions).toStrictEqual(EXAMPLE_PDM_MID_SCAN_DEFINITION);
  });
});

describe('PDM to ScanSequence mapper', () => {
  test.each([
    [
      [EXAMPLE_PDM_LOW_SCAN_DEFINITION],
      EXAMPLE_PDM_LOW_SCAN_DEFINITION.scan_definition_id,
      EXAMPLE_ZOD_SCAN_SEQUENCE
    ],
    [
      [EXAMPLE_PDM_MID_SCAN_DEFINITION],
      EXAMPLE_PDM_MID_SCAN_DEFINITION.scan_definition_id,
      EXAMPLE_ZOD_SCAN_SEQUENCE
    ]
  ])(
    'should return the display model for ScanDefinition converted from the store',
    (
      initialScanDefinitions: ScanDefinition[],
      initialScanSequence: string[],
      expectedOutput: ScanSequenceItemType
    ) => {
      const scanSequence = mapPdmToScanSequenceModel(
        initialScanSequence,
        initialScanDefinitions,
        EXAMPLE_TARGETS
      );
      expect(scanSequence).toStrictEqual(expectedOutput);
    }
  );
});
