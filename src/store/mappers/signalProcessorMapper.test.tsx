import { describe, test, expect } from 'vitest';
import { CSPConfiguration } from '../../generated/models/cspconfiguration';
import { LowSignalProcessorType, MidSignalProcessorType } from '../../models';
import { FSPFunctionMode } from '../../generated/models/fspfunction-mode';

import {
  fromLowSignalProcessor,
  fromMidSignalProcessor,
  toLowSignalProcessor,
  toMidSignalProcessor
} from './signalProcessorMapper';

const EXAMPLE_LOW_SBD_CSP_CONFIGURATION_WITHOUT_PST: CSPConfiguration = {
  config_id: 'csp-configuration-12345',
  name: 'CSP Config 1',
  lowcbf: {
    do_pst: false,
    correlation_spws: [
      {
        spw_id: 1,
        logical_fsp_ids: [],
        zoom_factor: 0,
        centre_frequency: 151171875,
        number_of_channels: 384,
        integration_time_ms: 283
      }
    ]
  }
};

const EXAMPLE_LOW_SBD_CSP_CONFIGURATION_WITH_PST: CSPConfiguration = {
  config_id: 'csp-configuration-12345',
  name: 'CSP Config 2',
  lowcbf: {
    do_pst: true,
    correlation_spws: [
      {
        spw_id: 1,
        logical_fsp_ids: [],
        zoom_factor: 0,
        centre_frequency: 151171875,
        number_of_channels: 904,
        integration_time_ms: 849
      }
    ]
  }
};

const EXAMPLE_LOW_SIGNAL_PROCESSOR_WITHOUT_PST: LowSignalProcessorType = {
  id: 'csp-configuration-12345',
  name: 'CSP Config 1',
  doPst: false,
  spectralWindows: [
    {
      spwId: 1,
      mode: FSPFunctionMode.Corr,
      centreFrequencyMhz: 151.171875,
      bandwidthMhz: 300,
      resolution: '5.43 (10.76)',
      averageTimeS: 0.283
    }
  ]
};

const EXAMPLE_LOW_SIGNAL_PROCESSOR_WITH_PST: LowSignalProcessorType = {
  id: 'csp-configuration-12345',
  name: 'CSP Config 2',
  doPst: true,
  spectralWindows: [
    {
      spwId: 1,
      mode: FSPFunctionMode.Corr,
      centreFrequencyMhz: 151.171875,
      bandwidthMhz: 706.25,
      resolution: '5.43 (10.76)',
      averageTimeS: 0.849
    },
    {
      spwId: 2,
      mode: 'PST',
      centreFrequencyMhz: 151.171875,
      bandwidthMhz: 706.25,
      resolution: '3.62 (7.17)',
      averageTimeS: 0.207
    }
  ]
};

const EXAMPLE_MID_SBD_CSP_CONFIGURATION_WITH_TWO_SPWS: CSPConfiguration = {
  config_id: 'csp-configuration-12345',
  name: 'CSP Config 1',
  midcbf: {
    frequency_band: 1,
    subbands: [
      {
        frequency_slice_offset: { value: 1.23, unit: 'MHz' },
        correlation_spws: [
          {
            spw_id: 1,
            logical_fsp_ids: [],
            zoom_factor: 0,
            centre_frequency: 2e6,
            number_of_channels: 52548,
            time_integration_factor: 1
          },
          {
            spw_id: 2,
            logical_fsp_ids: [],
            zoom_factor: 0,
            centre_frequency: 3e6,
            number_of_channels: 44643,
            time_integration_factor: 4
          }
        ]
      }
    ]
  },
  cbf: {
    fsps: [
      {
        frequency_slice_id: 3,
        fsp_id: 1,
        function_mode: FSPFunctionMode.Corr,
        integration_factor: 10,
        zoom_factor: 0
      }
    ]
  }
};

const EXAMPLE_MID_SBD_CSP_CONFIGURATION_WITH_ONE_SPW: CSPConfiguration = {
  config_id: 'csp-configuration-12345',
  name: 'CSP Config 2',
  midcbf: {
    frequency_band: 2,
    subbands: [
      {
        frequency_slice_offset: { value: 4.56, unit: 'MHz' },
        correlation_spws: [
          {
            spw_id: 1,
            logical_fsp_ids: [],
            zoom_factor: 0,
            centre_frequency: 4e6,
            number_of_channels: 59530,
            time_integration_factor: 6
          }
        ]
      }
    ]
  },
  cbf: {
    fsps: [
      {
        frequency_slice_id: 6,
        fsp_id: 1,
        function_mode: FSPFunctionMode.Corr,
        integration_factor: 10,
        zoom_factor: 0
      }
    ]
  }
};

const EXAMPLE_MID_SIGNAL_PROCESSOR_WITH_TWO_SPWS: MidSignalProcessorType = {
  id: 'csp-configuration-12345',
  name: 'CSP Config 1',
  band: 1,
  fsOffsetMhz: 1.23,
  spectralWindows: [
    {
      spwId: 1,
      mode: FSPFunctionMode.Corr,
      centreFrequencyMhz: 2,
      bandwidthMhz: 706.24512,
      resolution: '13.44 (2014.61)',
      averageTimeFactor: 1
    },
    {
      spwId: 2,
      mode: FSPFunctionMode.Corr,
      centreFrequencyMhz: 3,
      bandwidthMhz: 600.00192,
      resolution: '13.44 (1343.07)',
      averageTimeFactor: 4
    }
  ]
};

const EXAMPLE_MID_SIGNAL_PROCESSOR_WITH_ONE_SPW: MidSignalProcessorType = {
  id: 'csp-configuration-12345',
  name: 'CSP Config 2',
  band: 2,
  fsOffsetMhz: 4.56,
  spectralWindows: [
    {
      spwId: 1,
      mode: FSPFunctionMode.Corr,
      centreFrequencyMhz: 4,
      bandwidthMhz: 800.0832,
      resolution: '13.44 (1007.30)',
      averageTimeFactor: 6
    }
  ]
};

describe('PDM to SignalProcessor mappers', () => {
  test.each([
    [
      EXAMPLE_LOW_SBD_CSP_CONFIGURATION_WITH_PST,
      EXAMPLE_LOW_SIGNAL_PROCESSOR_WITH_PST
    ],
    [
      EXAMPLE_LOW_SBD_CSP_CONFIGURATION_WITHOUT_PST,
      EXAMPLE_LOW_SIGNAL_PROCESSOR_WITHOUT_PST
    ]
  ])(
    'should map PDM toLowSignalProcessor',
    (
      initialState: CSPConfiguration,
      expectedOutput: LowSignalProcessorType
    ) => {
      const lowSignalProcessor = toLowSignalProcessor(initialState);
      expect(lowSignalProcessor).toStrictEqual(expectedOutput);
    }
  );

  test.each([
    [
      EXAMPLE_MID_SBD_CSP_CONFIGURATION_WITH_TWO_SPWS,
      EXAMPLE_MID_SIGNAL_PROCESSOR_WITH_TWO_SPWS
    ],
    [
      EXAMPLE_MID_SBD_CSP_CONFIGURATION_WITH_ONE_SPW,
      EXAMPLE_MID_SIGNAL_PROCESSOR_WITH_ONE_SPW
    ]
  ])(
    'should map PDM toMidSignalProcessor',
    (
      initialState: CSPConfiguration,
      expectedOutput: MidSignalProcessorType
    ) => {
      const midSignalProcessor = toMidSignalProcessor(initialState);
      expect(midSignalProcessor).toStrictEqual(expectedOutput);
    }
  );
});

describe('SignalProcessor to PDM mappers', () => {
  test.each([
    [
      EXAMPLE_MID_SIGNAL_PROCESSOR_WITH_TWO_SPWS,
      EXAMPLE_MID_SBD_CSP_CONFIGURATION_WITH_TWO_SPWS
    ]
    // [EXAMPLE_MID_SIGNAL_PROCESSOR_WITH_ONE_SPW, EXAMPLE_MID_SBD_CSP_CONFIGURATION_WITH_ONE_SPW]
  ])(
    'should map PDM fromMidSignalProcessor',
    (
      formInput: MidSignalProcessorType,
      expectedStorageContent: CSPConfiguration
    ) => {
      const cspConfigurations = fromMidSignalProcessor(formInput);
      expect(cspConfigurations).toStrictEqual(expectedStorageContent);
    }
  );

  test.each([
    [
      EXAMPLE_LOW_SIGNAL_PROCESSOR_WITH_PST,
      EXAMPLE_LOW_SBD_CSP_CONFIGURATION_WITH_PST
    ],
    [
      EXAMPLE_LOW_SIGNAL_PROCESSOR_WITHOUT_PST,
      EXAMPLE_LOW_SBD_CSP_CONFIGURATION_WITHOUT_PST
    ]
  ])(
    'should map PDM fromLowSignalProcessor',
    (
      formInput: LowSignalProcessorType,
      expectedStorageContent: CSPConfiguration
    ) => {
      const cspConfigurations = fromLowSignalProcessor(formInput);
      expect(cspConfigurations).toStrictEqual(expectedStorageContent);
    }
  );
});
