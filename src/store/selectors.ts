import { TelescopeType } from '../generated/models/telescope-type';
import { SchedulingBlockDefinitionState } from './store';
import { SbDefinitionType } from '../models/sbDefinition/sbDefinition';
import { toSbDefinition } from './mappers/sbDefinitionMapper';
import { ProjectState } from './projectStore';
import { ProjectType } from '../models/project/project';
import { mapPdmToProject } from './mappers/projectMapper';

export const projectSelector = (state: ProjectState): ProjectType =>
  mapPdmToProject(state.project);

export const sbDefinitionSelector = (
  state: SchedulingBlockDefinitionState
): SbDefinitionType => {
  return toSbDefinition(state.sbDefinition);
};

export const telescopeIsMidSelector = (state: SchedulingBlockDefinitionState) =>
  state.sbDefinition.telescope === TelescopeType.SkaMid;
