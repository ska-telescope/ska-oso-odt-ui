import { produce } from 'immer';
import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';
import { immer } from 'zustand/middleware/immer';

import { ProjectType } from '../models/project/project';

import { getDefaultProject } from '../store/defaults';

import { ProjectInput } from '../generated/models/project-input';
import { mapProjectToPdm } from './mappers/projectMapper';

type InProgressObservingBlock = {
  obsBlockId: string;
  index: number;
};

export type ProjectState = {
  project: ProjectInput;
  updateProject: (project: ProjectType) => void;
  newProject: () => void;
  setPdmProject: (project: ProjectInput) => void;
  inProgressObservingBlock?: InProgressObservingBlock;
  setInProgressObservingBlock: (obsBlockId: string) => void;
};

// TODO this is currently a separate store. When the full Project -> Observing Block -> SB flow
//  is in place with the API calls, we should consider if we want to stick with a separate store
//  or combine them, possibly using slices: https://zustand.docs.pmnd.rs/guides/slices-pattern
export const useProjectStore = create<ProjectState>()(
  persist(
    immer((set) => ({
      project: getDefaultProject(),
      updateProject: (project: ProjectType) =>
        set((state: ProjectState) => mergeProject(state, project)),
      newProject: () =>
        set({
          project: getDefaultProject()
        }),
      // The API returns a PDM object which we want to put back in the store,
      // so we need a function that handles PDM rather than zod objects.
      setPdmProject: (project: ProjectInput) =>
        set((state: ProjectState) => ({
          ...state,
          project
        })),
      setInProgressObservingBlock: (obsBlockId: string) =>
        set((state) => ({
          inProgressObservingBlock: {
            obsBlockId,
            index: findObsBlockIndex(obsBlockId, state.project)
          }
        }))
    })),
    {
      name: 'project-store', // name of the item in localStorage.
      storage: createJSONStorage(() => localStorage)
    }
  )
);

export const mergeProject = (
  state: ProjectState,
  project: ProjectType
): ProjectState => {
  return produce(state, (draft: ProjectState) => {
    draft.project = {
      ...draft.project,
      ...mapProjectToPdm(project)
    };
  });
};

const findObsBlockIndex = (obsBlockId: string, project: ProjectInput) =>
  project.obs_blocks?.findIndex(
    (observingBlock) => observingBlock.obs_block_id === obsBlockId
  );
