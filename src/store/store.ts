import { produce } from 'immer';
import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';
import { immer } from 'zustand/middleware/immer';

import type { SBDefinitionInput } from '../generated/models/sbdefinition-input';
import { TelescopeType } from '../generated/models/telescope-type';
import { SbDefinitionType } from '../models/sbDefinition/sbDefinition';
import {
  fromLowSignalProcessor,
  fromMidSignalProcessor
} from './mappers/signalProcessorMapper';
import { telescopeIsMidSelector } from './selectors';
import { DEFAULT_INITIALISED_SB, getDefaultSbDefinition } from './defaults';
import { fromScript } from './mappers/scriptMapper';
import { fromGeneral } from './mappers/generalMapper';
import { ToPdm } from './mappers/targetsMapper';
import {
  mapScanDefinitionModelToLowPdm,
  mapScanDefinitionModelToMidPdm
} from './mappers/scansMapper';
import {
  mapLowArrayModelToPdm,
  mapMidArrayModelToPdm
} from './mappers/arrayMapper';

export type SchedulingBlockDefinitionState = {
  sbDefinition: SBDefinitionInput;
  updateSbDefinition: (sbDefinition: SbDefinitionType) => void;
  newSbDefinition: (telescope: TelescopeType) => void;
  setPdmSbDefinition: (pdmSbDefinition: SBDefinitionInput) => void;
};

export const useStore = create<SchedulingBlockDefinitionState>()(
  persist(
    immer((set) => ({
      sbDefinition: DEFAULT_INITIALISED_SB,
      updateSbDefinition: (sbDefinition: SbDefinitionType) =>
        set((state: SchedulingBlockDefinitionState) =>
          mergeSbDefinition(state, sbDefinition)
        ),
      newSbDefinition: (telescope: TelescopeType) =>
        set({
          sbDefinition: getDefaultSbDefinition(telescope)
        }),
      // The API returns a PDM object which we want to put back in the store,
      // so we need a function that handles PDM rather than Zod objects.
      setPdmSbDefinition: (sbDefinition: SBDefinitionInput) =>
        set((state: SchedulingBlockDefinitionState) => ({
          ...state,
          sbDefinition
        }))
    })),
    {
      name: 'sbd-store', // name of the item in localStorage.
      storage: createJSONStorage(() => localStorage)
    }
  )
);

export const mergeSbDefinition = (
  state: SchedulingBlockDefinitionState,
  sbDefinition: SbDefinitionType
): SchedulingBlockDefinitionState => {
  const pdmCspConfigurations = (sbDefinition.signalProcessor ?? []).map(
    telescopeIsMidSelector(state)
      ? fromMidSignalProcessor
      : fromLowSignalProcessor
  );
  const modelActivity = fromScript(sbDefinition.script);
  const pdmReceiverField = telescopeIsMidSelector(state)
    ? { dish_allocations: mapMidArrayModelToPdm(sbDefinition.array) }
    : { mccs_allocation: mapLowArrayModelToPdm(sbDefinition.array) };
  const pdmTargets = sbDefinition.targets.map(ToPdm.convert);
  const pdmScanDefinitions = sbDefinition.scans.scanDefinitions.map(
    telescopeIsMidSelector(state)
      ? mapScanDefinitionModelToMidPdm
      : mapScanDefinitionModelToLowPdm
  );
  const pdmScanSequence = sbDefinition.scans.scanSequence.map(
    (item) => item.scanDefinitionId
  );
  return produce(state, (draft: SchedulingBlockDefinitionState) => {
    draft.sbDefinition = {
      ...draft.sbDefinition,
      ...fromGeneral(sbDefinition.general),
      ...pdmReceiverField,
      targets: pdmTargets,
      activities: { ...modelActivity },
      csp_configurations: pdmCspConfigurations,
      scan_definitions: pdmScanDefinitions,
      scan_sequence: pdmScanSequence
    };
  });
};
