import { customAlphabet } from 'nanoid';

export const numericId = customAlphabet('1234567890', 5);

export const makeScanDefinitionId = () => `scan-definition-${numericId()}`;

export const makeTargetIdAndName = () => {
  const number = numericId();
  return { id: `target-${number}`, name: `Target ${number}` };
};

export const makeCspConfigurationIdAndName = () => {
  const number = numericId();
  return { id: `csp-configuration-${number}`, name: `Config ${number}` };
};

export const makeObservingBlockIdAndName = () => {
  const number = numericId();
  return { id: `obs-block-${number}`, name: `Observing Block ${number}` };
};
