/**
 * This module contains astronomy related pure functions and constants that are used throughout the app.
 *
 * It is expected to grow and then be refactored into smaller modules.
 */
const SPEED_OF_LIGHT_MS = 299792458;

const velocityEquivalentKms = (
  resolutionHz: number,
  centreFrequencyHz: number
): number => {
  return (1e-3 * SPEED_OF_LIGHT_MS * resolutionHz) / centreFrequencyHz;
};

export const velocityEquivalentKmsDisplayValue = (
  resolutionHz: number,
  centreFrequencyHz: number,
  decimalPlaces = 2
): string => {
  const result = velocityEquivalentKms(resolutionHz, centreFrequencyHz);
  return isFinite(result) ? result.toFixed(decimalPlaces) : '-';
};
