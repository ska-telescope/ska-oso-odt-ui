import { Configuration } from '../generated/models/configuration';
import apiService, { ApiResult } from '../services/apiService';

let osd: Configuration | null = null;

export const fetchOsd = async (): Promise<ApiResult<Configuration>> => {
  // Don't call the API if constants are already cached
  if (osd) return { data: osd };

  const result = await apiService.loadOsd();
  if (result.data) {
    osd = result.data;
  }
  return result;
};

export const getOsd = () => {
  if (!osd) throw new Error('OSD not fetched yet.');
  return osd;
};

// TODO these should all be moved to the OSD
export const MID_CHANNEL_WIDTH_KHZ = 13.44;
export const LOW_STATION_CHANNEL_WIDTH_MHZ = 0.78125;
export const LOW_CHANNEL_BLOCK_MHZ = 8 * LOW_STATION_CHANNEL_WIDTH_MHZ; // = 6.25 MHz
export const LOW_CONTINUUM_CHANNEL_WIDTH_KHZ = (24 * 781.25) / 3456;
export const LOW_PST_CHANNEL_WIDTH_KHZ = (16 * 781.25) / 3456;
export const MID_MAX_BANDWIDTH_MHZ = 800;
export const LOW_MAX_BANDWIDTH_MHZ = 75;
