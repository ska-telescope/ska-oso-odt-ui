/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * Class to hold the parameters relevant only for the current sub-array device.  :param sub-array_name: Name of the sub-array
 * @export
 * @interface SubarrayConfiguration
 */
export interface SubarrayConfiguration {
    /**
     * 
     * @type {string}
     * @memberof SubarrayConfiguration
     */
    'subarray_name': string;
}

