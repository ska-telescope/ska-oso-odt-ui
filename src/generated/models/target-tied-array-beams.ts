/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/**
 * 
 * @export
 * @interface TargetTiedArrayBeams
 */
export interface TargetTiedArrayBeams {
    /**
     * 
     * @type {any}
     * @memberof TargetTiedArrayBeams
     */
    'pst_beams'?: any;
    /**
     * 
     * @type {any}
     * @memberof TargetTiedArrayBeams
     */
    'pss_beams'?: any;
    /**
     * 
     * @type {any}
     * @memberof TargetTiedArrayBeams
     */
    'vlbi_beams'?: any;
}

