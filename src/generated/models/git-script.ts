/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import { PythonArgumentsInput } from './python-arguments-input';

/**
 * Represents an GitScript to be run as an activity in an SKA scheduling block.
 * @export
 * @interface GitScript
 */
export interface GitScript {
    /**
     * 
     * @type {{ [key: string]: PythonArgumentsInput; }}
     * @memberof GitScript
     */
    'function_args'?: { [key: string]: PythonArgumentsInput; };
    /**
     * 
     * @type {string}
     * @memberof GitScript
     */
    'kind'?: GitScriptKindEnum;
    /**
     * 
     * @type {string}
     * @memberof GitScript
     */
    'repo': string;
    /**
     * 
     * @type {string}
     * @memberof GitScript
     */
    'path': string;
    /**
     * 
     * @type {string}
     * @memberof GitScript
     */
    'branch'?: string;
    /**
     * 
     * @type {string}
     * @memberof GitScript
     */
    'commit'?: string;
}

export const GitScriptKindEnum = {
    Git: 'git'
} as const;

export type GitScriptKindEnum = typeof GitScriptKindEnum[keyof typeof GitScriptKindEnum];


