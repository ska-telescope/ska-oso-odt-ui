/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * CrossScanParameters defines the properties of an observing pattern that uses a cross scan observing pattern, typically used for pointing calibrations.
 * @export
 * @interface CrossScanParameters
 */
export interface CrossScanParameters {
    /**
     * 
     * @type {string}
     * @memberof CrossScanParameters
     */
    'kind'?: CrossScanParametersKindEnum;
    /**
     * 
     * @type {number}
     * @memberof CrossScanParameters
     */
    'offset_arcsec'?: number;
}

export const CrossScanParametersKindEnum = {
    CrossScanParameters: 'CrossScanParameters'
} as const;

export type CrossScanParametersKindEnum = typeof CrossScanParametersKindEnum[keyof typeof CrossScanParametersKindEnum];


