/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import { QuantityInput } from './quantity-input';

/**
 * SpiralParameters defines the properties of holography observations to support MID holography observing  These parameters are accepted by the MeerKAT holography script but are not included in the trajectory JSON.  * scan_extent: Diameter of beam pattern to measure, in degrees (default 10.0). * track_time: Extra time in seconds for scanning antennas to track when   passing over the target (default 10.0) * cycle_track_time: Extra time or scanning antennas to track when passing   over target (default 30.0) * slow_time: Time in seconds to slow down at start and end of each spiral arm   (default 6.0) * sample_time: Time in seconds to spend on each sample point generated   (default 0.25) * scan_speed: Scan speed in degrees per second (default 0.1) * slew_speed: Speed at which to slew in degrees per second, or if negative number   then this multiplied by scan_speed (default -1.0) * twist_factor Spiral twist factor (0.0 for straight radial, 1.0 standard spiral)   (default standard spiral = 1.0) * high_el_slowdown_factor: Factor by which to slow down nominal scanning speed at   90 degree elevation, linearly scaled from factor of 1 at 60 degrees elevation   (default 2.0)
 * @export
 * @interface SpiralParameters
 */
export interface SpiralParameters {
    /**
     * 
     * @type {string}
     * @memberof SpiralParameters
     */
    'kind'?: SpiralParametersKindEnum;
    /**
     * 
     * @type {QuantityInput}
     * @memberof SpiralParameters
     */
    'scan_extent'?: QuantityInput;
    /**
     * 
     * @type {QuantityInput}
     * @memberof SpiralParameters
     */
    'track_time'?: QuantityInput;
    /**
     * 
     * @type {QuantityInput}
     * @memberof SpiralParameters
     */
    'cycle_track_time'?: QuantityInput;
    /**
     * 
     * @type {QuantityInput}
     * @memberof SpiralParameters
     */
    'slow_time'?: QuantityInput;
    /**
     * 
     * @type {QuantityInput}
     * @memberof SpiralParameters
     */
    'sample_time'?: QuantityInput;
    /**
     * 
     * @type {QuantityInput}
     * @memberof SpiralParameters
     */
    'scan_speed'?: QuantityInput;
    /**
     * 
     * @type {QuantityInput}
     * @memberof SpiralParameters
     */
    'slew_speed'?: QuantityInput;
    /**
     * 
     * @type {number}
     * @memberof SpiralParameters
     */
    'twist_factor'?: number;
    /**
     * 
     * @type {number}
     * @memberof SpiralParameters
     */
    'high_el_slowdown_factor'?: number;
}

export const SpiralParametersKindEnum = {
    SpiralParameters: 'SpiralParameters'
} as const;

export type SpiralParametersKindEnum = typeof SpiralParametersKindEnum[keyof typeof SpiralParametersKindEnum];


