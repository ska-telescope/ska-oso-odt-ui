import React from 'react';
import { Card } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { OsoContentTitle } from '../../components/shared/ui/contentTitle';

const exportSb = () => (
  <Card data-testid="cardId" variant="outlined">
    <Grid>
      <Grid
        container
        direction="row"
        display="flex"
        alignItems="start"
        marginLeft="1em"
        marginTop="1em"
      >
        <OsoContentTitle testId="exportTitle" title="Export Scheduling Block" />
      </Grid>
    </Grid>
  </Card>
);

export default exportSb;
