import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import exportSb from './exportSb';

describe('exportSb', () => {
  test('should return only title', () => {
    render(exportSb());
    const heading = screen.getByRole('heading', {
      name: 'Export Scheduling Block'
    });
    expect(heading).toHaveAttribute('data-testid', 'exportTitle');
    expect(heading.className).not.toBeNull();
  });
});
