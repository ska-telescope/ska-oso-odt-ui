import React from 'react';
import Grid from '@mui/material/Grid2';
import { ProjectSearch } from 'src/components/home/projectSearch';
import { ProjectCreate } from 'src/components/home/projectCreate';

export const Home = (): JSX.Element => {
  return (
    <Grid container rowSpacing={2} columnSpacing={3} pr={5} pl={5} pt={4}>
      <Grid size={{ xs: 6 }}>
        <ProjectCreate />
      </Grid>
      <Grid size={{ xs: 6 }}>
        <ProjectSearch />
      </Grid>
    </Grid>
  );
};
