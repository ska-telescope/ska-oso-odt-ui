import { Box } from '@mui/material';

import { ProjectHeader } from '../../components/header/projectHeader';
import { FormProvider, useForm, useWatch } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { ProjectSchema, ProjectType } from '../../models/project/project';
import { useProjectStore } from '../../store/projectStore';
import { projectSelector } from '../../store/selectors';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { isEqual } from 'lodash';
import { ProjectDetails } from '../../components/project/projectDetails';
import Grid from '@mui/material/Grid2';
import { OsoContentTitle } from '../../components/shared/ui/contentTitle';
import React from 'react';
import { ObservingBlocksTable } from '../../components/project/observingBlocksTable';

export const Project = () => {
  const project = useProjectStore(projectSelector);
  const setProject = useProjectStore((state) => state.updateProject);

  const formMethods = useForm<ProjectType>({
    mode: 'onBlur',
    resolver: zodResolver(ProjectSchema),
    defaultValues: project
  });

  const { handleSubmit, control } = formMethods;

  const formValues = useWatch<ProjectType>({
    control
  });

  useDeepCompareEffect(() => {
    if (!isEqual(formValues, project)) {
      handleSubmit(() => setProject(formValues))();
    }
  }, [formValues]);

  return (
    <Box>
      <FormProvider {...formMethods}>
        <ProjectHeader />
        <OsoContentTitle title="Project" testId="scan-title" />
        <Grid container rowSpacing={1} columnSpacing={1}>
          <Grid size={{ xs: 12, md: 6, lg: 6 }}>
            <ProjectDetails />
          </Grid>
          <Grid size={{ xs: 12, md: 6, lg: 6 }}>
            <ObservingBlocksTable />
          </Grid>
        </Grid>
      </FormProvider>
    </Box>
  );
};
