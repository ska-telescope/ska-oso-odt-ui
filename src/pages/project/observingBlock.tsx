import { Box } from '@mui/material';

import { ProjectHeader } from '../../components/header/projectHeader';
import Grid from '@mui/material/Grid2';
import { OsoContentTitle } from '../../components/shared/ui/contentTitle';
import React from 'react';
import { FormProvider, useForm, useWatch } from 'react-hook-form';
import { useProjectStore } from '../../store/projectStore';
import { projectSelector } from '../../store/selectors';
import { ProjectSchema, ProjectType } from '../../models/project/project';
import { zodResolver } from '@hookform/resolvers/zod';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { isEqual } from 'lodash';
import { ObservingBlockDetails } from '../../components/observingBlock/observingBlockDetails';
import { SbDefinitionsTable } from '../../components/observingBlock/sbDefinitionsTable';

export const ObservingBlock = () => {
  const project = useProjectStore(projectSelector);
  const setProject = useProjectStore((state) => state.updateProject);

  // TODO should the observing block page have its own form or share the project page form? If
  //  the latter then there needs to be some common root component where the form is defined.
  //  This solution is kind of the worst of both worlds, where a separate form is defined but at the Project level
  const formMethods = useForm<ProjectType>({
    mode: 'onBlur',
    resolver: zodResolver(ProjectSchema),
    defaultValues: project
  });

  const { handleSubmit, control } = formMethods;

  const formValues = useWatch<ProjectType>({
    control
  });

  useDeepCompareEffect(() => {
    if (!isEqual(formValues, project)) {
      handleSubmit(() => setProject(formValues))();
    }
  }, [formValues]);

  return (
    <Box>
      <FormProvider {...formMethods}>
        <ProjectHeader />
        <OsoContentTitle title="Observing Block" testId="scan-title" />
        <Grid container rowSpacing={1} columnSpacing={1}>
          <Grid size={{ xs: 6 }}>
            <ObservingBlockDetails />
          </Grid>
          <Grid size={{ xs: 6 }}>
            <SbDefinitionsTable />
          </Grid>
        </Grid>
      </FormProvider>
    </Box>
  );
};
