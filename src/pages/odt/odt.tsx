import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { Box } from '@mui/material';

import { OdtRoutes } from '../../components/app/routes';

export const ODT = () => {
  const basename = window.env?.BASE_URL;
  return (
    <Box>
      <Router
        basename={basename}
        future={{
          v7_relativeSplatPath: true,
          v7_startTransition: true,
          v7_fetcherPersist: true,
          v7_normalizeFormMethod: true,
          v7_partialHydration: true,
          v7_skipActionStatusRevalidation: true
        }}
      >
        <OdtRoutes />
      </Router>
    </Box>
  );
};
