import React from 'react';
import { Card } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { OsoContentTitle } from '../../components/shared/ui/contentTitle';
import { ScanDefinitions } from '../../components/sb/scans/scanDefinitions';
import { ScanSequence } from '../../components/sb/scans/scanSequence';

export const Scans = (): JSX.Element => (
  <Card data-testid="contentId" variant="outlined">
    <Grid
      container
      direction="row"
      display="flex"
      alignItems="start"
      marginLeft="1em"
      marginTop="1em"
    >
      <OsoContentTitle title="Scans" testId="scan-title" />
    </Grid>
    <Grid padding="20px">
      <Grid container rowSpacing={1} columnSpacing={1}>
        <Grid size={{ xs: 12, md: 6, lg: 6 }}>
          <ScanDefinitions />
        </Grid>
        <Grid size={{ xs: 12, md: 6, lg: 6 }}>
          <ScanSequence />
        </Grid>
      </Grid>
    </Grid>
  </Card>
);
