import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { Scripts } from './scripts';
import {
  TEST_FORM_SCRIPT,
  WithSbDefinitionFormContext
} from '../../components/testUtils';
import React from 'react';
import { TelescopeType } from '../../generated/models/telescope-type';

describe('Scripts', () => {
  test('should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext
        telescopeType={TelescopeType.SkaMid}
        initialState={TEST_FORM_SCRIPT}
      >
        <Scripts />
      </WithSbDefinitionFormContext>
    );

    const heading = screen.getByRole('heading', {
      name: 'Script'
    });
    expect(heading).toBeInTheDocument();
  });
});
