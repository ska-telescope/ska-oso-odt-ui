import { Card } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { useFormContext } from 'react-hook-form';
import { OsoContentTitle } from '../../components/shared/ui/contentTitle';
import { useState } from 'react';
import { TargetsList } from '../../components/sb/target/targetsList';
import { TargetDetails } from '../../components/sb/target/targetDetails';
import { OsoTitleWrapper } from '../../components/shared/ui';

const DEFAULT_SELECTION = 0;
const ctrlName = 'targets' as const;

export const Targets = (): JSX.Element => {
  const [selectedTargetIndex, setSelectedTargetIndex] =
    useState<number>(DEFAULT_SELECTION);
  const { getValues } = useFormContext();
  const targets = getValues(ctrlName);
  // TODO sort the formatting and make the structure less complex/nested
  return (
    <Card variant="outlined">
      <Grid
        container
        direction="row"
        display="flex"
        alignItems="start"
        marginLeft="1em"
        marginTop="1em"
      >
        <OsoContentTitle title="Targets" />
      </Grid>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={3}
        direction="row"
        width="100%"
      >
        <Grid size={{ xs: 12, md: 3, lg: 2 }}>
          <TargetsList
            selectedTargetIndex={selectedTargetIndex}
            setSelectedTargetIndex={setSelectedTargetIndex}
          />
        </Grid>
        <Grid size={{ xs: 11, sx: 12, md: 9, lg: 10 }}>
          <OsoTitleWrapper>
            {targets.length > 0 && (
              <TargetDetails selectedTargetIndex={selectedTargetIndex} />
            )}
          </OsoTitleWrapper>
        </Grid>
      </Grid>
    </Card>
  );
};
