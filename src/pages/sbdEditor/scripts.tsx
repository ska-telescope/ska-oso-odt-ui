import React from 'react';
import { Card, CardContent } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { ScriptDetails } from '../../components';
import { ArgTable } from '../../components/sb/script/argTable';
import { OsoContentTitle } from '../../components/shared/ui/contentTitle';

export const Scripts = (): JSX.Element => (
  <Card data-testid="sbScript" variant="outlined">
    <Grid
      container
      direction="row"
      display="flex"
      alignItems="start"
      marginLeft="1em"
      marginTop="1em"
    >
      <OsoContentTitle title="Script" />
    </Grid>
    <CardContent>
      <Grid container direction="row" spacing={1}>
        <Grid size={{ xs: 12, md: 6, lg: 4 }}>
          <ScriptDetails />
        </Grid>

        <Grid size={{ xs: 12, md: 6, lg: 4 }}>
          <ArgTable title="Init Stage Arguments" argName="initArgs" />
        </Grid>
        <Grid size={{ xs: 12, md: 6, lg: 4 }}>
          <ArgTable title="Run Stage Arguments" argName="mainArgs" />
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);
