import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';

import noMatch from './noMatch';

describe('noMatch', () => {
  test('should return a message about something went wrong', () => {
    render(noMatch());

    const content = screen.getByRole('heading', {
      name: /something went wrong! unable to locate http:\/\/localhost:3000\/ page\./i
    });
    expect(content).toBeInTheDocument();
  });
});
