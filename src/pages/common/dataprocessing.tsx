import React from 'react';
import { Card } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { OsoContentTitle } from '../../components/shared/ui/contentTitle';

export const DataProcessing = (): JSX.Element => {
  return (
    <Card data-testid="contentId" variant="outlined">
      <Grid
        container
        direction="row"
        display="flex"
        alignItems="start"
        marginLeft="1em"
        marginTop="1em"
      >
        <OsoContentTitle title="Data Processing" />
      </Grid>
      <Grid padding="20px">
        Controls for data processing will appear in a future version of the ODT.
      </Grid>
    </Card>
  );
};
