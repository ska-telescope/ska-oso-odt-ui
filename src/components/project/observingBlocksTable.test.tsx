import React from 'react';
import { act, fireEvent, render, screen, within } from '@testing-library/react';
import {
  WithProjectFormContext,
  TEST_PROJECT_FORM,
  getTableRows
} from 'src/components/testUtils';
import { ObservingBlocksTable } from './observingBlocksTable';

describe('ObservingBlocksTable component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <ObservingBlocksTable />
      </WithProjectFormContext>
    );

    const rows = getTableRows('Observing Blocks');

    expect(rows.length).eq(2);
    const dataCells = within(rows[0]).getAllByRole('cell');
    expect(dataCells.length).eq(4);
    expect(within(dataCells[0]).getByRole('textbox').value).eq(
      'Observing Block 97521'
    );
    expect(within(dataCells[1]).getByRole('textbox').value).eq('TODO');
  });

  test('should add a new Observing Block when the add button is clicked', () => {
    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <ObservingBlocksTable />
      </WithProjectFormContext>
    );

    const addButton = screen.getByRole('button', {
      name: 'Add New Observing Block'
    });

    act(() => {
      fireEvent.click(addButton);
    });
    const rows = getTableRows('Observing Blocks');
    expect(rows.length).eq(3);
  });

  test('should delete the correct Observing Block row when the delete icon is clicked', () => {
    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <ObservingBlocksTable />
      </WithProjectFormContext>
    );
    // Delete the first Observing Block
    const deleteButton = screen.getByRole('button', {
      name: `observingBlocks.0.delete`
    });

    act(() => {
      fireEvent.click(deleteButton);
    });
    const rows = getTableRows('Observing Blocks');
    expect(rows.length).eq(1);
    const dataCells = within(rows[0]).getAllByRole('cell');
    // Now the first row should be the initial second row
    expect(within(dataCells[0]).getByRole('textbox').value).eq(
      'Observing Block 58258'
    );
  });
});
