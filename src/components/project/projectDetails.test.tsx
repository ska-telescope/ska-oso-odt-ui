import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import {
  WithProjectFormContext,
  TEST_PROJECT_FORM
} from 'src/components/testUtils';
import React from 'react';
import { ProjectDetails } from './projectDetails';

describe('ProjectDetails', () => {
  test('should display the values from the form', () => {
    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <ProjectDetails />
      </WithProjectFormContext>
    );
    expect(screen.getByLabelText('Name').value).eq(TEST_PROJECT_FORM.name);
    expect(screen.getByLabelText('Code').value).eq(TEST_PROJECT_FORM.code);
    expect(screen.getByLabelText('Type').value).eq(TEST_PROJECT_FORM.type);
    expect(screen.getByLabelText('Status').value).eq(TEST_PROJECT_FORM.status);
  });
});
