import React from 'react';
import {
  createTableDataFromFormValues,
  DisplayType,
  OsoDataTableColumnDefinitions,
  OsoTitleWrapper
} from '../shared/ui';

import { OsoDataTable } from '../shared/ui';
import { useFormContext } from 'react-hook-form';
import { OperationsButtonGroup } from '../shared/operationButtonGroup';
import { makeObservingBlockIdAndName } from '../../lib/id-generator';
import { ObservingBlockSchema } from '../../models/project/observingBlock';
import { useNavigate } from 'react-router-dom';
import { RoutePaths } from '../app/routes';
import { useProjectStore } from '../../store/projectStore';

const ctrlName = 'observingBlocks';

export const ObservingBlocksTable = (): JSX.Element => {
  const { getValues, setValue } = useFormContext();

  const setInProgressObservingBlock = useProjectStore(
    (state) => state.setInProgressObservingBlock
  );

  const navigate = useNavigate();

  const addObservingBlock = () => {
    const newObservingBlock = ObservingBlockSchema.parse({
      ...makeObservingBlockIdAndName()
    });
    setValue(ctrlName, [...(getValues(ctrlName) ?? []), newObservingBlock]);
  };

  const deleteObservingBlock = (index: number) => {
    setValue(ctrlName, getValues(ctrlName).toSpliced(index, 1));
  };

  const navigateToObsBlockByIndex = (obsBlockIndex: number) => {
    const obsBlockId = getValues(ctrlName)[obsBlockIndex].id;
    setInProgressObservingBlock(obsBlockId);
    navigate(RoutePaths.ObservingBlock);
  };

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'id',
      isIdField: true
    },
    {
      displayType: [DisplayType.StaticField],
      title: 'Name',
      columnName: 'name'
    },
    {
      displayType: [DisplayType.StaticField],
      title: 'Status',
      columnName: 'status'
    },
    {
      displayType: [DisplayType.EditButton],
      title: '',
      columnName: 'edit',
      description: 'Click to open this Observing Block',
      onClick: navigateToObsBlockByIndex
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      columnName: 'delete',
      description: 'Click to delete this Observing Block',
      onClick: deleteObservingBlock
    }
  ];

  const tableContent = createTableDataFromFormValues(
    columns,
    getValues(ctrlName)
  );

  return (
    <OsoTitleWrapper title={'Observing Blocks'}>
      <OsoDataTable baseName={ctrlName} data={tableContent} columns={columns} />
      <OperationsButtonGroup
        title="Observing Block"
        count={1}
        handleAdd={addObservingBlock}
      />
    </OsoTitleWrapper>
  );
};
