import { Stack } from '@mui/material';
import { OsoTitleWrapper } from '../shared/ui';
import { ControlledTextField } from '../shared/controlled';

export const ProjectDetails = () => {
  return (
    <OsoTitleWrapper title="General">
      <Stack spacing={2}>
        <ControlledTextField name={`name`} label="Name" />
        <ControlledTextField name={`code`} label="Code" disabled />
        <ControlledTextField name={`type`} label="Type" />
        <ControlledTextField name={`status`} label="Status" disabled />
      </Stack>
    </OsoTitleWrapper>
  );
};
