import { Suspense, useState } from 'react';
import { CssBaseline, Paper, ThemeProvider } from '@mui/material';
import {
  CopyrightModal,
  Footer,
  Spacer,
  SPACER_VERTICAL,
  THEME_DARK,
  THEME_LIGHT
} from '@ska-telescope/ska-gui-components';
import { version as releaseVersion } from '../../../package.json';
import { FOOTER_HEIGHT, HEADER_HEIGHT } from '../../models/constants';
import { ODT } from '../../pages/odt/odt';
import theme from '../../services/theme/theme';
import { Loader, OdtHeader } from '../index';
import { fetchOsd } from '../../lib/osd';
import { AlertDialog } from '../shared/dialogs';

const App = () => {
  const [showCopyright, setShowCopyright] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [themeMode, setThemeMode] = useState(THEME_LIGHT);

  const toggleTheme = () =>
    themeMode === THEME_LIGHT
      ? setThemeMode(THEME_DARK)
      : setThemeMode(THEME_LIGHT);

  const version = releaseVersion;

  fetchOsd().then((result) => {
    if (result.error) {
      setErrorMessage(result.error);
    }
  });

  return (
    <ThemeProvider theme={theme(themeMode)}>
      <AlertDialog
        open={!!errorMessage}
        title="Error while fetching OSD data."
        content={errorMessage}
        onClose={() => setErrorMessage('')}
      />
      <CssBaseline enableColorScheme />
      <Suspense fallback={<Loader />}>
        <CopyrightModal
          copyrightFunc={(value: boolean) => setShowCopyright(value)}
          show={showCopyright}
        />
        <OdtHeader
          title="Observation Design Tool"
          skao="SKAO WebSite"
          themeMode={themeMode}
          toggleThemeMode={toggleTheme}
        />
        <Paper>
          <Spacer size={HEADER_HEIGHT} axis={SPACER_VERTICAL} />
          <ODT />
          <Spacer size={FOOTER_HEIGHT} axis={SPACER_VERTICAL} />
        </Paper>
        <Footer testId="footerId" version={`Version ${version}`} />
      </Suspense>
    </ThemeProvider>
  );
};

export default App;
