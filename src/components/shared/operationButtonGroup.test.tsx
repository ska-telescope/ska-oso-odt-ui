import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { OperationsButtonGroup } from './operationButtonGroup';

describe('OperationsButtonGroup', () => {
  const title = 'Operation Research';
  const shortTitle = title.replaceAll(' ', '');
  const handleAdd = vi.fn();
  const handleDelete = vi.fn();
  const count = 1;

  const validateButtons = () => {
    const addButton = screen.getByRole('button', {
      name: 'Add New ' + title
    });
    expect(addButton).toHaveAttribute('data-testid', 'btnAdd' + shortTitle);

    const deleteButton = screen.getByRole('button', {
      name: 'Delete Existing ' + title
    });
    expect(deleteButton).toHaveAttribute(
      'data-testid',
      'btnDelete' + shortTitle
    );
  };

  test('showTitle=false should return 2 buttons', () => {
    render(
      <OperationsButtonGroup
        title={title}
        count={count}
        handleAdd={handleAdd}
        handleDelete={handleDelete}
        showTitle={false}
      />
    );

    validateButtons();
  });

  test('showTitle=true should return title with 2 buttons', () => {
    render(
      <OperationsButtonGroup
        title={title}
        count={count}
        handleAdd={handleAdd}
        handleDelete={handleDelete}
        showTitle={true}
      />
    );

    const titleField = screen.getByText(title + ' (' + count.toString() + ')');
    expect(titleField).toBeInTheDocument();

    validateButtons();
  });
});
