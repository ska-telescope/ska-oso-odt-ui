import React, { useState } from 'react';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import apiService from '../../services/apiService';
import { useProjectStore } from '../../store/projectStore';
import { Alert, Snackbar } from '@mui/material';
import { AlertDialog } from './dialogs';
import { ProjectInput } from 'src/generated/models/project-input';
import { mapPdmToProject } from '../../store/mappers/projectMapper';
import { useFormContext } from 'react-hook-form';

// TODO refactor these buttons, moving some of the API call stuff out? Seems we have 3 layers of API function calls and error handling
export const SaveProjectButton = () => {
  const [successAlert, setSuccessAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const { reset } = useFormContext();

  const [pdmProject, setPdmProject] = useProjectStore((state) => [
    state.project,
    state.setPdmProject
  ]);

  const handleSubmitProject = async () => {
    try {
      const response = pdmProject.prj_id
        ? await apiService.updateProjectFn(pdmProject)
        : await apiService.saveProjectFn(pdmProject);

      if (response.data) {
        setPdmProject(response.data as ProjectInput);
        // Reset the form state to reflect the API response
        reset(mapPdmToProject(response.data as ProjectInput));
        setSuccessAlert(true);
      } else {
        setErrorMessage(response.error!);
      }
      setTimeout(() => {}, 2000);
    } catch (e) {
      setErrorMessage(e?.message);
    }
  };

  return (
    <div>
      <Snackbar
        open={successAlert}
        onClose={() => setSuccessAlert(false)}
        autoHideDuration={3000} // Automatically hide after 3 seconds
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }} // Position
      >
        <Alert severity="success" variant="filled">
          Project has been saved successfully to the ODA.
        </Alert>
      </Snackbar>
      <AlertDialog
        open={!!errorMessage}
        title="Error while saving to the ODA."
        content={errorMessage}
        onClose={() => setErrorMessage('')}
      />
      <Button
        testId="save-sb"
        ariaDescription="Click to save the changes back to backend"
        label="Save to ODA"
        onClick={handleSubmitProject}
        color={ButtonColorTypes.Secondary}
        variant={ButtonVariantTypes.Contained}
      />
    </div>
  );
};
