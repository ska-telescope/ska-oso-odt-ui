import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';

import { FormAlert, FormInfo, FormWarning } from './alert';

describe('alert', () => {
  describe('FormAlert', () => {
    test('should return error icon and error text', () => {
      const error = 'error message';
      render(<FormAlert error={error} />);

      const errorIcon = screen.getByTestId('ErrorIcon');
      const errorContent = screen.getByText(error);

      expect(errorIcon).toHaveAttribute('focusable', 'false');
      expect(errorIcon).toHaveAttribute('aria-hidden', 'true');
      expect(errorIcon).toHaveAttribute('viewBox', '0 0 24 24');
      expect(errorContent).toBeInTheDocument();
    });
  });

  describe('FormWarning', () => {
    test('should return warning icon and warning text', () => {
      const warning = 'warning message';
      render(<FormWarning warning={warning} />);

      const warningIcon = screen.getByTestId('WarningIcon');
      const warningContent = screen.getByText(warning);

      expect(warningIcon).toHaveAttribute('focusable', 'false');
      expect(warningIcon).toHaveAttribute('aria-hidden', 'true');
      expect(warningIcon).toHaveAttribute('viewBox', '0 0 24 24');
      expect(warningContent).toBeInTheDocument();
    });
  });

  describe('FormInfo', () => {
    test('should return info icon and info text', () => {
      const info = 'information message';
      render(<FormInfo content={info} />);

      const infoIcon = screen.getByTestId('InfoIcon');
      const infoContent = screen.getByText(info);

      expect(infoIcon).toHaveAttribute('focusable', 'false');
      expect(infoIcon).toHaveAttribute('aria-hidden', 'true');
      expect(infoIcon).toHaveAttribute('viewBox', '0 0 24 24');
      expect(infoContent).toBeInTheDocument();
    });
  });
});
