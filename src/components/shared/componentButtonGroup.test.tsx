import '@testing-library/jest-dom';
import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { ComponentButtonGroup } from './componentButtonGroup';

describe('ComponentButtonGroup', () => {
  test('should return 3 buttons and appropriate attributes', () => {
    render(<ComponentButtonGroup isValid disabledAll />);

    // 3 button components
    const reset = screen.getByRole('button', { name: /reset/i });
    const save = screen.getByRole('button', { name: /save/i });
    const cancel = screen.getByRole('button', { name: /cancel/i });

    // reset button
    expect(reset).toBeInTheDocument();
    expect(reset).toBeVisible();
    expect(reset).toHaveAttribute('aria-label', 'reset');
    expect(reset).toHaveAttribute('type', 'button');
    expect(reset).toHaveAttribute('data-testid', 'btnReset');

    // save button
    expect(save).toBeInTheDocument();
    expect(save).toBeVisible();
    expect(save).toHaveAttribute('aria-label', 'save');
    expect(save).toHaveAttribute('type', 'submit');
    expect(save).toHaveAttribute('data-testid', 'btnSave');

    // cancel button
    expect(cancel).toBeInTheDocument();
    expect(cancel).toBeVisible();
    expect(cancel).toHaveAttribute('aria-label', 'cancel');
    expect(cancel).toHaveAttribute('type', 'button');
    expect(cancel).toHaveAttribute('data-testid', 'btnCancel');
  });

  test('with isValid=true & disabledAll=true should return reset & save buttons disabled', () => {
    render(<ComponentButtonGroup isValid={true} disabledAll={true} />);

    // reset & save buttons disabled
    expect(screen.getByRole('button', { name: /reset/i })).toBeDisabled();
    expect(screen.getByRole('button', { name: /save/i })).toBeDisabled();
    expect(screen.getByRole('button', { name: /cancel/i })).toBeEnabled();
  });

  test('with isValid=true & disabledAll=false should return all buttons enabled', () => {
    render(<ComponentButtonGroup isValid={true} disabledAll={false} />);

    // all buttons enabled
    expect(screen.getByRole('button', { name: /reset/i })).toBeEnabled();
    expect(screen.getByRole('button', { name: /save/i })).toBeEnabled();
    expect(screen.getByRole('button', { name: /cancel/i })).toBeEnabled();
  });

  test('with isValid=true & disabledAll=false should return reset & save buttons disabled', () => {
    render(<ComponentButtonGroup isValid={true} disabledAll={false} />);

    // reset & save buttons disabled
    expect(screen.getByRole('button', { name: /reset/i })).toBeEnabled();
    expect(screen.getByRole('button', { name: /save/i })).toBeEnabled();
    expect(screen.getByRole('button', { name: /cancel/i })).toBeEnabled();
  });

  test('with isValid=false & disabledAll=false should return save button disabled', () => {
    render(<ComponentButtonGroup isValid={false} disabledAll={false} />);

    // save button disabled
    expect(screen.getByRole('button', { name: /reset/i })).toBeEnabled();
    expect(screen.getByRole('button', { name: /save/i })).toBeDisabled();
    expect(screen.getByRole('button', { name: /cancel/i })).toBeEnabled();
  });

  test('click reset should return something', () => {
    render(<ComponentButtonGroup isValid={true} disabledAll={false} />);

    const reset = screen.getByRole('button', { name: /reset/i });
    act(() => {
      fireEvent.click(reset);
    });
  });

  test('click save should return something', () => {
    render(<ComponentButtonGroup isValid={true} disabledAll={false} />);

    const reset = screen.getByRole('button', { name: /reset/i });
    act(() => {
      fireEvent.click(reset);
    });
  });

  test('click reset should return something', () => {
    render(<ComponentButtonGroup isValid={true} disabledAll={false} />);

    const reset = screen.getByRole('button', { name: /reset/i });
    act(() => {
      fireEvent.click(reset);
    });
  });
});
