import React from 'react';
import { Typography } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';

export type PopupDialogProps = {
  title: string;
  content: string[];
  open: boolean;
  onClose: () => void | null;
  onDialogResponse: (response: string) => void | null;
  cancelButtonLabel: string | null;
  continueButtonLabel: string | null;
  children?: string | JSX.Element | JSX.Element[];
};

export const PopupDialog = ({
  open = true,
  onClose,
  onDialogResponse,
  title = 'Confirmation',
  content = [],
  cancelButtonLabel = 'Cancel',
  children,
  continueButtonLabel = 'Continue'
}: PopupDialogProps): JSX.Element => {
  const handleCallback = (status: string) => {
    if (
      status === 'continue' &&
      onDialogResponse &&
      typeof onDialogResponse === 'function'
    ) {
      onDialogResponse(status);
    }
    if (status === 'cancel' && onClose && typeof onClose === 'function') {
      onClose();
    }
  };

  const handleContinue = () => {
    handleCallback('continue');
  };

  const handleCancel = () => {
    handleCallback('cancel');
  };

  return (
    <Dialog
      open={open}
      onClose={handleCancel}
      aria-labelledby="dialog-title"
      aria-describedby="dialog-description"
      id="dialog-change"
    >
      <DialogTitle data-testid="dialog-title" id="dialog-title">
        {title}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="dialog-description" component="div">
          {content?.length > 0 &&
            content.map((line, index) => (
              <Typography key={index} variant="body1">
                {line}
              </Typography>
            ))}
        </DialogContentText>
        {children}
      </DialogContent>
      <DialogActions sx={{ p: '24px' }}>
        <Button
          ariaDescription={`${cancelButtonLabel} Button`}
          color={ButtonColorTypes.Secondary}
          label={cancelButtonLabel}
          onClick={handleCancel}
          data-testid={`${cancelButtonLabel.toLowerCase()}Id`}
          testId={`${cancelButtonLabel.toLowerCase()}Id`}
          variant={ButtonVariantTypes.Contained}
        />
        <Button
          ariaDescription={`${continueButtonLabel} Button`}
          color={ButtonColorTypes.Secondary}
          label={continueButtonLabel}
          onClick={handleContinue}
          data-testid={`${continueButtonLabel.toLowerCase()}Id`}
          testId={`${continueButtonLabel.toLowerCase()}Id`}
          variant={ButtonVariantTypes.Contained}
        />
      </DialogActions>
    </Dialog>
  );
};
