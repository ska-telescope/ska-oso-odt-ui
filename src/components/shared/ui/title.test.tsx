import '@testing-library/jest-dom';
import React from 'react';
import { render } from '@testing-library/react';
import { describe, test } from 'vitest';
import { validate } from './helpers';
import { OsoTitle } from './title';

describe('OsoTitle', () => {
  test('with title should return appropriate components', () => {
    const title = 'Ska Title';
    const titleId = 'title-id';
    render(<OsoTitle title={title} titleId={titleId} />);
    validate.titleField(title, titleId);
  });

  test('with null title should return empty string', () => {
    const titleId = 'title-id';
    const { container } = render(
      <OsoTitle title={undefined} titleId={titleId} />
    );
    expect(container.innerText).to.eq('');
  });
});

describe('OsoTitle', () => {
  test('with null title should return empty string', () => {
    const titleId = 'title-id';
    const { container } = render(
      <OsoTitle title={undefined} titleId={titleId} />
    );
    expect(container.innerText).to.eq('');
  });
});
