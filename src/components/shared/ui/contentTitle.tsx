import React from 'react';
import { Typography } from '@mui/material';
import { Variant } from '@mui/material/styles/createTypography';
import { Colors } from '@ska-telescope/ska-gui-components';

type ContentTitleProps = {
  title: string;
  /* optional fields */
  variant?: Variant;
  testId?: string;
  marginLeft?: string;
};

export const OsoContentTitle = ({
  title,
  variant,
  testId,
  marginLeft,
  ...otherProps
}: ContentTitleProps): JSX.Element => {
  return (
    <Typography
      data-testid={testId ?? 'functionId'}
      variant={variant ?? 'h5'}
      color={Colors().STATUS_FG_4}
      pl={marginLeft ?? 2}
      textAlign="left"
      {...otherProps}
    >
      {title}
    </Typography>
  );
};
