import {
  Box,
  Button,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  SvgIcon
} from '@mui/material';

import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import { useTheme } from '@mui/material/styles';
import { OsoTitleWrapper } from './titleWrapper';

type OsoListComponentProps = {
  title: string;
  name: string;
  selectedIndex: number;
  setSelectedIndex: (newIndex: number) => void;
  names: string[];
  deleteItem?: (index: number) => void;
  Icon?: typeof SvgIcon;
  withDelete?: boolean;
  disabledDelete?: boolean;
  children?: JSX.Element;
};

export const OsoListComponent = ({
  title,
  name,
  selectedIndex,
  setSelectedIndex,
  names,
  withDelete = true,
  disabledDelete = false,
  deleteItem,
  Icon,
  children
}: OsoListComponentProps): JSX.Element => {
  const theme = useTheme();

  const renderListItem = (tgtName: string, index: number): JSX.Element => (
    <ListItemButton
      id={tgtName}
      key={tgtName}
      selected={selectedIndex === index}
      onClick={() => setSelectedIndex(index)}
      style={{
        borderStyle: 'solid',
        borderWidth: '1px',
        borderColor: 'lightgray',
        alignItems: 'center'
      }}
    >
      {Icon && (
        <ListItemIcon>
          <Icon />
        </ListItemIcon>
      )}
      <ListItemText primary={tgtName} />
      {withDelete && (
        <ListItemIcon sx={{ opacity: disabledDelete ? 0.38 : 1 }}>
          <Button
            onClick={(e: React.MouseEventHandler<SVGSVGElement>) => {
              deleteItem(index);
              if (selectedIndex >= index && selectedIndex != 0) {
                // This ensures the element you have selected stays selected if you delete one that is earlier in the list.
                setSelectedIndex(selectedIndex - 1);
              }
              e.stopPropagation();
              e.preventDefault();
            }}
            startIcon={<DeleteIcon />}
            aria-disabled={disabledDelete}
            aria-label={`delete-${index}`}
            disabled={disabledDelete}
          />
        </ListItemIcon>
      )}
    </ListItemButton>
  );

  return (
    <OsoTitleWrapper title={title}>
      <Box sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        <List
          component="ul"
          aria-label={name}
          sx={{
            // selected and (selected + hover) states
            '&& .Mui-selected, && .Mui-selected:hover': {
              bgcolor: theme.palette.primary.dark
            }
          }}
        >
          {names.map((tgt, index) => renderListItem(tgt, index))}
        </List>
      </Box>
      {children}
    </OsoTitleWrapper>
  );
};
