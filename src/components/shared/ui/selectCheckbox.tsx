import React from 'react';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select from '@mui/material/Select';
import type { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { OsoTitleWrapper } from './titleWrapper';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

type SelectCheckboxProps = {
  title: string;
  name: string;
  options: string[];
  width: number;
  onChange: (event: SelectChangeEvent<string[] | undefined>) => void;
  /* optional fields */
  multiple?: boolean;
  defaultValue?: string[];
  renderFn?: (value: string[]) => string;
  testId?: string;
  children?: string | JSX.Element | JSX.Element[];
};

export const OsoSelectCheckbox = ({
  title,
  name,
  testId,
  options,
  defaultValue,
  multiple,
  onChange: handleChange,
  renderFn,
  width = 320,
  children
}: SelectCheckboxProps) => {
  const testIdUsed = testId ?? name;
  const selectId = multiple
    ? `${testIdUsed}-multiple-select`
    : `${testIdUsed}-select`;
  const titleId = `${testIdUsed}-title`;

  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width
      }
    }
  };

  return (
    <OsoTitleWrapper title={title} testId={titleId}>
      <Select
        fullWidth
        labelId={selectId}
        name={name}
        id={selectId}
        data-testid={selectId}
        data-cy={selectId}
        value={defaultValue ?? options[0]}
        multiple={
          multiple ?? (defaultValue ? typeof defaultValue !== 'string' : false)
        }
        onChange={handleChange}
        input={<OutlinedInput label={title} />}
        renderValue={(selected) =>
          renderFn ? renderFn(selected ?? []) : selected
        }
        MenuProps={MenuProps}
        size="small"
        style={{ marginBottom: '10px' }}
      >
        {options.map((option: string) => {
          const optionId = `${testIdUsed}-option-${option}`;
          return (
            <MenuItem key={option} value={option}>
              <Checkbox
                data-testid={optionId}
                data-cy={optionId}
                sx={{ backgroundColor: 'lightgrey' }}
                checked={
                  defaultValue ? defaultValue.indexOf(option) > -1 : false
                }
              />
              <ListItemText primary={option} />
            </MenuItem>
          );
        })}
      </Select>
      {children ?? null}
    </OsoTitleWrapper>
  );
};
