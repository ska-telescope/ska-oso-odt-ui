import '@testing-library/jest-dom';
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { validate } from './helpers';
import { OsoSelectCheckbox } from './selectCheckbox';

describe('OsoSelectCheckbox', () => {
  const options = ['option 1', 'option 2', 'option 3', 'option 4'];
  const title = 'Ska Select Checkbox';
  const name = 'checkbox';
  const titleId = `${name}-title`;
  const onChange = vi.fn();

  const validateRadioGroupComponents = () => {
    const combobox = screen.getByRole('combobox');
    expect(combobox).toBeInTheDocument();
    expect(combobox).toHaveAttribute('aria-expanded', 'false');
    expect(combobox).toHaveAttribute('aria-haspopup', 'listbox');
  };

  test('without testId and children should return appropriate components', () => {
    render(
      <OsoSelectCheckbox
        title={title}
        name={name}
        options={options}
        onChange={onChange}
        width={400}
      />
    );

    validate.titleTestField(title, titleId);
    validateRadioGroupComponents();
  });

  test('without children should return appropriate components', () => {
    render(
      <OsoSelectCheckbox
        title={title}
        testId={name}
        name={name}
        options={options}
        onChange={onChange}
        width={400}
      />
    );

    validate.titleTestField(title, titleId);
    validateRadioGroupComponents();
  });

  test('with children should return appropriate components', () => {
    const childContent = 'Sample Children';
    const children = <p id="XXX">{childContent}</p>;
    render(
      <OsoSelectCheckbox
        title={title}
        testId={name}
        name={name}
        options={options}
        onChange={onChange}
        width={400}
      >
        {children}
      </OsoSelectCheckbox>
    );

    validate.titleTestField(title, titleId);
    validateRadioGroupComponents();
    validate.childField(childContent);
  });

  test('with user interaction should call mock function', () => {
    render(
      <OsoSelectCheckbox
        title={title}
        name={name}
        options={options}
        onChange={onChange}
        width={400}
      />
    );

    // Simulate user interaction to select the new option
    const textbox = screen.getByRole('textbox', { hidden: true });
    fireEvent.change(textbox, { target: { value: 'option 2' } });
    expect(onChange).toHaveBeenCalled();
  });
});
