import React from 'react';
import { useTheme } from '@mui/material/styles';

type TitleProps = {
  title: string | undefined;
  titleId: string;
  marginBottom?: string;
};

export const OsoTitle = ({ title, titleId, marginBottom }: TitleProps) => {
  const theme = useTheme();
  return title ? (
    <legend
      style={{
        fontSize: '12px',
        color: theme.palette.primary.dark,
        marginBottom: marginBottom ?? '5px'
      }}
      id={titleId}
      data-testid={titleId}
      data-cy={titleId}
    >
      &nbsp;{title}&nbsp;
    </legend>
  ) : null;
};
