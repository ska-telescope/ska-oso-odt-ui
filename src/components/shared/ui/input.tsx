import React from 'react';
import { TextField } from '@mui/material';
import type { ChangeEvent } from 'react';
import { OsoTitle } from './title';
import { useTheme } from '@mui/material/styles';

type InputBaseProps = {
  title: string;
  name: string;
  /* optional fields */
  disabled?: boolean;
  error?: boolean;
  testId?: string;
  defaultValue?: string | number;
  placeholder?: string;
  helperText?: string | null;
  size?: 'small' | 'medium' | null;
  type?: 'password' | 'time' | 'text' | 'number' | 'date' | 'file';
  children?: string | JSX.Element | JSX.Element[];
};

type inputWithOnChangeProps = {
  handleChange?: never;
  onChange: (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
};

type inputWithHandleChangeProps = {
  handleChange: (name: string, value: string) => void;
  onChange?: never;
};

// Combine the props into a single mutually exclusive type (with onChange and handleChange)
type InputProps = InputBaseProps &
  (inputWithOnChangeProps | inputWithHandleChangeProps);

export const OsoInput = ({
  title,
  testId,
  name,
  error,
  placeholder,
  type,
  disabled,
  size,
  helperText,
  defaultValue,
  handleChange,
  onChange,
  children
}: InputProps) => {
  const theme = useTheme();
  const testIdUsed = testId ?? name;
  const titleId = `${testIdUsed}-title`;
  const inputId = `${testIdUsed}-input`;

  const inputChangeHandle = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const newValue: string = event.target?.value;
    if (onChange) {
      onChange(event);
    } else if (handleChange) {
      handleChange(name, newValue);
    }
  };

  return (
    <fieldset
      style={{
        flexDirection: 'row',
        marginTop: '0px',
        marginBottom: '20px',
        marginRight: '20px',
        borderRadius: '10px',
        border: '1px solid ' + theme.palette.primary.dark,
        width: '100%'
      }}
    >
      <OsoTitle title={title} titleId={titleId} />
      <TextField
        disabled={disabled ?? false}
        fullWidth
        helperText={helperText}
        placeholder={placeholder}
        aria-labelledby="demo-row-radio-buttons-group-label"
        name={name}
        type={type ?? 'text'}
        id={inputId}
        value={defaultValue}
        onChange={inputChangeHandle}
        error={error ?? false}
        size={size ?? 'small'}
      />
      {children ?? null}
    </fieldset>
  );
};
