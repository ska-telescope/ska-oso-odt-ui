import React from 'react';
import { OsoTitle } from './title';
import { useTheme } from '@mui/material/styles';

type TitleWrapperProps = {
  title?: string;
  /* optional fields */
  testId?: string;
  children?: string | JSX.Element | JSX.Element[];
};

export const OsoTitleWrapper = ({
  title,
  testId,
  children,
  ...otherProps
}: TitleWrapperProps) => {
  const theme = useTheme();
  const titleComponent = title ? (
    <OsoTitle
      title={title}
      titleId={testId ?? `${title.replaceAll(' ', '')}-title`}
    />
  ) : (
    <br />
  );

  return (
    <fieldset
      style={{
        marginTop: '0px',
        borderRadius: '10px',
        border: '1px solid ' + theme.palette.primary.dark,
        width: '100%'
      }}
      {...otherProps}
    >
      {titleComponent}
      {children}
    </fieldset>
  );
};
