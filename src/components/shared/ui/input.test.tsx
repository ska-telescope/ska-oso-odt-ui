import '@testing-library/jest-dom';
import React from 'react';
import {
  act,
  cleanup,
  fireEvent,
  render,
  screen
} from '@testing-library/react';
import { describe, expect, beforeEach, test, vi } from 'vitest';
import { validate } from './helpers';
import { OsoInput } from './input';

type FieldObject = {
  field: string;
  value: string;
};

describe('OsoInput', () => {
  const title = 'Ska Input';
  const name = 'field';
  const titleId = `${name}-title`;

  const checkFields: FieldObject[] = [
    { field: 'text', value: 'ABC' },
    { field: 'number', value: '123' },
    { field: 'date', value: '2024-01-01' },
    { field: 'file', value: 'abc.json' },
    { field: 'password', value: 'test123' },
    { field: 'time', value: '1230' }
  ];

  const validateInputComponents = (error: boolean) => {
    const textbox = screen.getByRole('textbox');
    expect(textbox).toHaveAttribute('aria-invalid', error ? 'true' : 'false');
    expect(textbox).toHaveAttribute('id', `${name}-input`);
    expect(textbox).toHaveAttribute('name', name);
    expect(textbox).toHaveAttribute('type', 'text');
    expect(textbox).toHaveAttribute('value', '');
  };

  describe('with onChange', () => {
    checkFields.map((option: FieldObject) => {
      describe(`for ${option.field}`, () => {
        const onChange = vi.fn();
        beforeEach(() => {
          cleanup();
        });

        test('without testId and children should return appropriate components', () => {
          render(<OsoInput title={title} name={name} onChange={onChange} />);

          validate.titleField(title, titleId);
          validateInputComponents(false);
        });

        test('without children should return appropriate components', () => {
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              onChange={onChange}
            />
          );

          validate.titleField(title, titleId);
          validateInputComponents(false);
        });

        test('with children should return appropriate components', () => {
          const childContent = 'Sample Children';
          const children = <p id="XXX">{childContent}</p>;
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              onChange={onChange}
            >
              {children}
            </OsoInput>
          );

          validate.titleField(title, titleId);
          validateInputComponents(false);
          validate.childField(childContent);
        });

        test('with error should return appropriate components', () => {
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              onChange={onChange}
              error={true}
            />
          );

          validate.titleField(title, titleId);
          validateInputComponents(true);
        });

        test('with user changes should return appropriate components', () => {
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              onChange={onChange}
            />
          );

          // Simulate user interaction to enter the new value
          const textbox = screen.getByRole('textbox');
          act(() => {
            fireEvent.change(textbox, {
              target: { value: option.value }
            });
          });
          expect(onChange).toHaveBeenCalledOnce();
        });
      });
    });
  });

  describe('with handleChange', () => {
    checkFields.map((option: FieldObject) => {
      describe(`for ${option.field}`, () => {
        const handleChange = vi.fn();
        beforeEach(() => {
          cleanup();
        });

        test('without testId and children should return appropriate components', () => {
          render(
            <OsoInput title={title} name={name} handleChange={handleChange} />
          );

          validate.titleField(title, titleId);
          validateInputComponents(false);
        });

        test('without children should return appropriate components', () => {
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              handleChange={handleChange}
            />
          );

          validate.titleField(title, titleId);
          validateInputComponents(false);
        });

        test('with children should return appropriate components', () => {
          const childContent = 'Sample Children';
          const children = <p id="XXX">{childContent}</p>;
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              handleChange={handleChange}
            >
              {children}
            </OsoInput>
          );

          validate.titleField(title, titleId);
          validateInputComponents(false);
          validate.childField(childContent);
        });

        test('with error should return appropriate components', () => {
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              handleChange={handleChange}
              error={true}
            />
          );

          validate.titleField(title, titleId);
          validateInputComponents(true);
        });

        test('with user changes should call the mock function once', () => {
          render(
            <OsoInput
              title={title}
              testId={name}
              name={name}
              handleChange={handleChange}
            />
          );

          // Simulate user interaction to enter the new value
          const textbox = screen.getByRole('textbox');
          act(() => {
            fireEvent.change(textbox, { target: { value: option.value } });
          });
          expect(handleChange).toHaveBeenCalledOnce();
        });
      });
    });
  });
});
