import React from 'react';
import { Checkbox, FormControlLabel, RadioGroup } from '@mui/material';
import { useState, type SyntheticEvent } from 'react';
import { OsoTitle } from './title';
import { useTheme } from '@mui/material/styles';

type CheckboxGroupBaseProps = {
  title: string;
  name: string;
  options: string[];
  /* optional fields */
  defaultValue?: string[];
  testId?: string;
  horizontal?: boolean;
  children?: string | JSX.Element | JSX.Element[];
};

type CheckboxGroupWithOnChangeProps = {
  onChange: (event: SyntheticEvent) => void;
  handleChange?: never;
};

type CheckboxGroupWithHandleChangeProps = {
  onChange?: never;
  handleChange: (name: string, value: string[]) => void;
};

// Combine the props into a single mutually exclusive type (with onChange and handleChange)
type CheckboxGroupProps = CheckboxGroupBaseProps &
  (CheckboxGroupWithOnChangeProps | CheckboxGroupWithHandleChangeProps);

export const OsoCheckboxGroup = ({
  title,
  testId,
  name,
  defaultValue,
  handleChange,
  onChange,
  options,
  children,
  horizontal
}: CheckboxGroupProps) => {
  const [localValue, setLocalValue] = useState<string[]>(defaultValue ?? []);
  const theme = useTheme();
  const testIdUsed = testId ?? name;
  const titleId = `${testIdUsed}-title`;

  const checkboxGroupStyle = {
    marginLeft: horizontal ? '20px' : '1px'
  };
  const checkboxChangeHandle = (event: SyntheticEvent, checked: boolean) => {
    const value: string = event.target?.value;
    // Add or delete of the options
    const newValues: string[] = checked
      ? [...localValue, value]
      : localValue.filter((elem) => elem !== value);
    if (onChange) {
      onChange(event);
    } else if (handleChange) {
      handleChange(name, newValues);
    }
    setLocalValue(newValues);
  };

  const createCheckboxElement = (option: string) => {
    const optionId = `${testIdUsed}-radio-${option.replaceAll(' ', '')}`;
    return (
      <FormControlLabel
        style={{ marginRight: '20px' }}
        data-cy={optionId}
        id={optionId}
        key={optionId}
        value={option}
        control={<Checkbox />}
        checked={localValue.includes(option)}
        label={option}
        onChange={checkboxChangeHandle}
      />
    );
  };

  return (
    <fieldset
      style={{
        border: '1px solid ' + theme.palette.primary.dark,
        borderRadius: '10px',
        flexDirection: 'row',
        marginBottom: '20px',
        marginRight: '20px',
        marginTop: '0px',
        width: '100%'
      }}
    >
      <OsoTitle title={title} titleId={titleId} />
      <RadioGroup
        row={horizontal ?? false}
        aria-labelledby="demo-row-radio-buttons-group-label"
        name={name}
        style={checkboxGroupStyle}
      >
        {options
          .filter(Boolean)
          .map((option: string) => createCheckboxElement(option))}
      </RadioGroup>
      {children ?? null}
    </fieldset>
  );
};
