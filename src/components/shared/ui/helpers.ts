import { screen } from '@testing-library/react';
import { expect } from 'vitest';

export const validate = {
  titleField: (expectedTitle: string, expectedTitleId: string) => {
    const titleField = screen.getByText(expectedTitle);
    expect(titleField).toBeInTheDocument();
    expect(titleField).toBeVisible();
    expect(titleField).toHaveAttribute('data-cy', expectedTitleId);
    expect(titleField).toHaveAttribute('id', expectedTitleId);
    expect(titleField).toHaveAttribute('data-testid', expectedTitleId);
  },

  titleTestField: (expectedTitle: string, expectedTitleId: string) => {
    const titleField = screen.getByTestId(expectedTitleId);
    expect(titleField).toBeInTheDocument();
    expect(titleField).toBeVisible();
    expect(titleField.innerText).contains(expectedTitle);
  },

  childField: (simpleChild: string) => {
    const childComponent = screen.getByText(simpleChild);
    expect(childComponent).toBeInTheDocument();
    expect(childComponent).toBeVisible();
  }
};
