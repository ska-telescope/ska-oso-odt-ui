import '@testing-library/jest-dom';
import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { validate } from './helpers';
import { OsoSelect } from './select';

describe('OsoSelect', () => {
  const options = [
    { value: 'Test 1', label: 'test1' },
    { value: 'Test 2', label: 'test2' },
    { value: 'Test 3', label: 'test3' },
    { value: 'Test 4', label: 'test4' },
    { value: 'Test 5', label: 'test5' },
    { value: 'Test 6', label: 'test6' }
  ];
  const title = 'Ska Selection Dropdown';
  const name = 'value';

  const validateSelectComponents = (value: string) => {
    const hiddenInput = screen.getByRole('textbox', { hidden: true });
    expect(hiddenInput).toHaveAttribute('aria-invalid', 'false');
    expect(hiddenInput).toHaveAttribute('name', name);
    expect(hiddenInput).toHaveAttribute('value', value);

    const combobox = screen.getByRole('combobox');
    expect(combobox).toBeInTheDocument();
    expect(combobox).toHaveAttribute('aria-expanded', 'false');
    expect(combobox).toHaveAttribute('aria-haspopup', 'listbox');
  };

  describe('with onChange', () => {
    const onChange = vi.fn();
    test('without testId and children should return appropriate components', () => {
      render(
        <OsoSelect
          title={title}
          name={name}
          options={options}
          defaultValue=""
          onChange={onChange}
        />
      );

      validateSelectComponents('');
    });

    test('without children should return appropriate components', () => {
      render(
        <OsoSelect
          title={title}
          testId={name}
          name={name}
          defaultValue=""
          options={options}
          onChange={onChange}
        />
      );

      validateSelectComponents('');
    });

    test('with children should return appropriate components', () => {
      const childContent = 'Sample Children';
      const children = <p id="XXX">{childContent}</p>;
      render(
        <OsoSelect
          title={title}
          testId={name}
          name={name}
          defaultValue="Test 1"
          options={options}
          onChange={onChange}
        >
          {children}
        </OsoSelect>
      );

      validateSelectComponents('Test 1');
      validate.childField(childContent);
    });

    test('with user interaction should call mock function', async () => {
      render(
        <OsoSelect
          title={title}
          name={name}
          options={options}
          defaultValue=""
          onChange={onChange}
        />
      );

      const newOption = 'Test 3';
      // Simulate user interaction to select the new option
      const textbox = screen.getByRole('textbox', { hidden: true });
      act(() => {
        fireEvent.change(textbox, { target: { value: newOption } });
      });
      expect(onChange).toHaveBeenCalled();
    });
  });

  describe('with handleChange', () => {
    const handleChange = vi.fn();
    test('without testId and children should return appropriate components', () => {
      render(
        <OsoSelect
          title={title}
          name={name}
          options={options}
          defaultValue=""
          handleChange={handleChange}
        />
      );

      validateSelectComponents('');
    });

    test('without children should return appropriate components', () => {
      render(
        <OsoSelect
          title={title}
          testId={name}
          name={name}
          defaultValue=""
          options={options}
          handleChange={handleChange}
        />
      );

      validateSelectComponents('');
    });

    test('with children should return appropriate components', () => {
      const childContent = 'Sample Children';
      const children = <p id="XXX">{childContent}</p>;
      render(
        <OsoSelect
          title={title}
          testId={name}
          name={name}
          defaultValue="Test 1"
          options={options}
          handleChange={handleChange}
        >
          {children}
        </OsoSelect>
      );

      validateSelectComponents('Test 1');
      validate.childField(childContent);
    });

    test('with user interaction should call mock function', async () => {
      render(
        <OsoSelect
          title={title}
          name={name}
          options={options}
          defaultValue=""
          handleChange={handleChange}
        />
      );

      // Simulate user interaction to select the new option
      const textbox = screen.getByRole('textbox', { hidden: true });
      act(() => {
        fireEvent.change(textbox, { target: { value: 'Test 3' } });
      });
      expect(handleChange).toHaveBeenCalled();
    });
  });
});
