import { IconButton, SvgIcon } from '@mui/material';

export type OsoIconButtonProps = {
  index: number;
  name: string;
  disabled: boolean;
  handleClick?: (index: number) => void;
  description?: string;
  Icon: typeof SvgIcon; // The MUI Icon component
};

export const OsoIconButton = ({
  index,
  name,
  disabled,
  handleClick,
  description = '',
  Icon
}: OsoIconButtonProps): JSX.Element => {
  const onClick = (index: number): void => {
    if (handleClick !== undefined) {
      handleClick(index);
    }
  };
  return (
    <IconButton
      name={name}
      sx={{ width: '100%' }}
      aria-description={description}
      aria-label={name}
      disabled={disabled}
      onClick={() => onClick(index)}
    >
      <Icon />
    </IconButton>
  );
};
