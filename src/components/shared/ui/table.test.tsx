import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { OsoTable } from './table';
import type { GridColDef } from '@mui/x-data-grid';
import { validate } from './helpers';

describe('OsoTable', () => {
  const data = [
    {
      name: 'Poland',
      language: 'Polish',
      population: 3,
      capital: 'Warsaw',
      status: 'Created',
      created: new Date()
    },
    {
      name: 'Bulgaria',
      language: 'Bulgarian',
      population: 2,
      capital: 'Sofia',
      status: 'InPreparation',
      created: new Date()
    }
  ];
  const columns: GridColDef[] = [
    {
      field: 'name',
      headerName: 'Name',
      width: 200,
      editable: true,
      align: 'center',
      headerAlign: 'center'
    },
    {
      field: 'language',
      headerName: 'Language',
      width: 200,
      editable: true,
      align: 'center',
      headerAlign: 'center',
      type: 'singleSelect',
      valueOptions: [
        'Bulgarian',
        'English',
        'Chinese',
        'German',
        'Hungarian',
        'Lithuanian',
        'Moldovan'
      ]
    },
    {
      field: 'population',
      headerName: 'Population',
      width: 100,
      editable: true,
      type: 'number',
      align: 'center',
      headerAlign: 'center'
    },
    {
      field: 'capital',
      headerName: 'Capital',
      width: 200,
      editable: true,
      align: 'center',
      headerAlign: 'center'
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 200,
      editable: true,
      align: 'center',
      headerAlign: 'center',
      type: 'singleSelect',
      valueOptions: ['Created', 'InDraft', 'InPreparation', 'Completed']
    },
    {
      field: 'created',
      headerName: 'Created On',
      width: 100,
      editable: true,
      type: 'date',
      align: 'center',
      headerAlign: 'center'
    }
  ];
  const title = 'Ska Table';
  const titleId = 'table';
  const handleSelect = vi.fn();

  const gotoPreviousPage = 'Go to previous page';
  const gotoNextPage = 'Go to next page';

  const validateCommonComponents = (presentationCount: number) => {
    const presentation = screen.getAllByRole('presentation');
    expect(presentation.length).toBe(presentationCount);
    expect(presentation.at(0)).toHaveAttribute('data-id', 'gridPanelAnchor');

    // Should have page number and counter of total page
    const pageNoField = screen.getByText(/1–2 of 2/);
    expect(pageNoField).toBeInTheDocument();
    expect(pageNoField).toBeVisible();
  };

  const validateGrid = (count: string) => {
    const grid = screen.getByRole('grid');
    expect(grid).toBeInTheDocument();
    expect(grid).toHaveAttribute('aria-colcount', count);
  };

  const validateButtons = (buttonCount: number) => {
    // Should have previous and next table navigation buttons
    const buttons = screen.getAllByRole('button');
    expect(buttons.length).toBe(buttonCount);
    expect(buttons.at(buttonCount - 2)).toHaveAttribute(
      'aria-label',
      gotoPreviousPage
    );
    expect(buttons.at(buttonCount - 2)).toHaveAttribute(
      'title',
      gotoPreviousPage
    );
    expect(buttons.at(buttonCount - 1)).toHaveAttribute(
      'aria-label',
      gotoNextPage
    );
    expect(buttons.at(buttonCount - 1)).toHaveAttribute('title', gotoNextPage);
  };

  const validateButtonFields = (
    container: HTMLElement,
    buttonFieldCount: number
  ) => {
    const buttonFields = container.querySelectorAll('button');
    expect(buttonFields.length).toBe(buttonFieldCount);
    expect(buttonFields[buttonFieldCount - 4]).toHaveAttribute(
      'aria-label',
      'Sort'
    );
    expect(buttonFields[buttonFieldCount - 3]).toHaveAttribute(
      'aria-label',
      'Menu'
    );
    expect(buttonFields[buttonFieldCount - 2]).toHaveAttribute(
      'aria-label',
      gotoPreviousPage
    );
    expect(buttonFields[buttonFieldCount - 2]).toHaveAttribute(
      'title',
      gotoPreviousPage
    );
    expect(buttonFields[buttonFieldCount - 1]).toHaveAttribute(
      'aria-label',
      gotoNextPage
    );
    expect(buttonFields[buttonFieldCount - 1]).toHaveAttribute(
      'title',
      gotoNextPage
    );
  };

  const validateCommonComponentsWithoutActions = (container: HTMLElement) => {
    validateButtonFields(container, 4);
    validateButtons(2);
  };

  describe('checkboxSelection=false', () => {
    describe('withActions=false', () => {
      test('without title, testId and children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={null}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={false}
          />
        );
        validateCommonComponents(13);
        validateGrid('6');
        validateCommonComponentsWithoutActions(container);
      });

      test('without testId and children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={false}
          />
        );
        const expectedId = `${title.replaceAll(' ', '')}-title`;
        validate.titleField(title, expectedId);
        validateCommonComponents(13);
        validateGrid('6');
        validateCommonComponentsWithoutActions(container);
      });

      test('without children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={false}
          />
        );

        validate.titleField(title, titleId);
        validateCommonComponents(13);
        validateGrid('6');
        validateCommonComponentsWithoutActions(container);
      });

      test('with children component should return appropriate components', () => {
        const childContent = 'Sample Children';
        const children = <p id="XXX">{childContent}</p>;
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={false}
          >
            {children}
          </OsoTable>
        );

        validate.titleField(title, titleId);
        validateCommonComponents(13);
        validateGrid('6');
        validateCommonComponentsWithoutActions(container);
        validate.childField(childContent);
      });
    });
    describe('withActions=true', () => {
      test('without title, testId and children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={null}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={true}
            checkboxSelection={false}
          />
        );
        validateCommonComponents(13);
        validateGrid('7');
        validateButtons(3);
        validateButtonFields(container, 5);
      });
      test('without testId and children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={true}
            checkboxSelection={false}
          />
        );
        const expectedId = `${title.replaceAll(' ', '')}-title`;
        validate.titleField(title, expectedId);
        validateCommonComponents(13);
        validateGrid('8');
        validateButtons(3);
        validateButtonFields(container, 5);
      });

      test('without children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={true}
            checkboxSelection={false}
          />
        );

        validate.titleField(title, titleId);
        validateCommonComponents(13);
        validateGrid('9');
        validateButtons(3);
        validateButtonFields(container, 5);
      });

      test('with children component should return appropriate components', () => {
        const childContent = 'Sample Children';
        const children = <p id="XXX">{childContent}</p>;
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={false}
          >
            {children}
          </OsoTable>
        );

        validate.titleField(title, titleId);
        validateCommonComponents(13);
        validateGrid('9');
        validateButtons(2);
        validateButtonFields(container, 4);
        validate.childField(childContent);
      });
    });
  });
  describe('checkboxSelection=true', () => {
    describe('withActions=false', () => {
      test('without testId and children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={true}
          />
        );
        const expectedId = `${title.replaceAll(' ', '')}-title`;
        validate.titleField(title, expectedId);
        validateCommonComponents(15);
        validateGrid('10');
        validateCommonComponentsWithoutActions(container);
      });

      test('without children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={true}
          />
        );

        validate.titleField(title, titleId);
        validateCommonComponents(15);
        validateGrid('10');
        validateCommonComponentsWithoutActions(container);
      });

      test('with children component should return appropriate components', () => {
        const childContent = 'Sample Children';
        const children = <p id="XXX">{childContent}</p>;
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={true}
          >
            {children}
          </OsoTable>
        );

        validate.titleField(title, titleId);
        validateCommonComponents(15);
        validateGrid('10');
        validateCommonComponentsWithoutActions(container);
        validate.childField(childContent);
      });
    });
    describe('withActions=true', () => {
      test('without testId and children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={true}
            checkboxSelection={true}
          />
        );
        const expectedId = `${title.replaceAll(' ', '')}-title`;
        validate.titleField(title, expectedId);
        validateCommonComponents(15);
        validateGrid('11');
        validateButtons(3);
        validateButtonFields(container, 5);
      });

      test('without children component should return appropriate components', () => {
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={true}
            checkboxSelection={true}
          />
        );

        validate.titleField(title, titleId);
        validateCommonComponents(15);
        validateGrid('12');
        validateButtons(3);
        validateButtonFields(container, 5);
      });

      test('with children component should return appropriate components', () => {
        const childContent = 'Sample Children';
        const children = <p id="XXX">{childContent}</p>;
        const { container } = render(
          <OsoTable
            title={title}
            testId={titleId}
            data={data}
            columns={columns}
            handleSelect={handleSelect}
            withActions={false}
            checkboxSelection={true}
          >
            {children}
          </OsoTable>
        );

        validate.titleField(title, titleId);
        validateCommonComponents(15);
        validateGrid('12');
        validateButtons(2);
        validateButtonFields(container, 4);
        validate.childField(childContent);
      });
    });
  });
});
