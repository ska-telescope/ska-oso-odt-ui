import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { validate } from './helpers';
import { OsoTitleWrapper } from './titleWrapper';

describe('OsoTitleWrapper', () => {
  const title = 'Ska List Box';
  const testId = 'title-id';

  const validateCommonComponent = (
    expectedTitle: string,
    expectedTitleId: string
  ) => {
    validate.titleField(expectedTitle, expectedTitleId);

    const inputBox = screen.getByRole('group', { name: expectedTitle });
    expect(inputBox).toBeInTheDocument();
    expect(inputBox).toBeVisible();
  };

  test('without child and testId should return appropriate components', () => {
    render(<OsoTitleWrapper title={title} />);
    validateCommonComponent(title, 'SkaListBox-title');
  });

  test('without child component should return appropriate components', () => {
    render(<OsoTitleWrapper title={title} testId={testId} />);
    validateCommonComponent(title, testId);
  });

  test('with child component should return appropriate components', () => {
    const childContent = 'Sample Children';
    const children = <p id="XXX">{childContent}</p>;
    render(
      <OsoTitleWrapper title={title} testId={testId}>
        {children}
      </OsoTitleWrapper>
    );

    validateCommonComponent(title, testId);
    validate.childField(childContent);
  });
});
