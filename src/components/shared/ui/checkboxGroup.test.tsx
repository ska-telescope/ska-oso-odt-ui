import '@testing-library/jest-dom';
import React from 'react';
import { cleanup, render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { validate } from './helpers';
import { OsoCheckboxGroup } from './checkboxGroup';
import userEvent from '@testing-library/user-event';

describe('OsoCheckboxGroup', () => {
  const options = ['option 1', 'option 2', 'option 3', 'option 4'];
  const title = 'Ska Radio Group Box';
  const name = 'radio-group';

  const validateRadioGroupComponents = () => {
    // With same number pf options and test ids
    options.map((option) => {
      const optionField = screen.getByText(option);
      expect(optionField).toBeInTheDocument();
    });
  };

  describe('with onChange', () => {
    const onChange = vi.fn();
    afterEach(() => {
      cleanup();
    });

    test('without testId and children should return appropriate components', () => {
      render(
        <OsoCheckboxGroup
          title={title}
          name={name}
          options={options}
          onChange={onChange}
        />
      );

      validate.titleField(title, name + '-title');
      validateRadioGroupComponents();
    });

    test('without children should return appropriate components', () => {
      render(
        <OsoCheckboxGroup
          title={title}
          testId={name}
          name={name}
          options={options}
          onChange={onChange}
        />
      );

      validate.titleField(title, name + '-title');
      validateRadioGroupComponents();
    });

    test('with children should return appropriate components', () => {
      const childContent = 'Sample Children';
      const children = <p id="XXX">{childContent}</p>;
      render(
        <OsoCheckboxGroup
          title={title}
          testId={name}
          name={name}
          options={options}
          onChange={onChange}
        >
          {children}
        </OsoCheckboxGroup>
      );

      validate.titleField(title, name + '-title');
      validateRadioGroupComponents();
      validate.childField(childContent);
    });

    test('with user change should call mock function', async () => {
      render(
        <OsoCheckboxGroup
          title={title}
          name={name}
          options={options}
          onChange={onChange}
        />
      );
      // Simulate user interaction to enter the new value
      const optionsList = screen.getByText(/option 2/i);
      const user = userEvent.setup();
      await user.click(optionsList);
      expect(onChange).toHaveBeenCalled();
    });
  });

  describe('with handleChange', () => {
    const handleChange = vi.fn();
    afterEach(() => {
      cleanup();
    });
    test('without testId and children should return appropriate components', () => {
      render(
        <OsoCheckboxGroup
          title={title}
          name={name}
          options={options}
          handleChange={handleChange}
        />
      );

      validate.titleField(title, name + '-title');
      validateRadioGroupComponents();
    });

    test('without children should return appropriate components', () => {
      render(
        <OsoCheckboxGroup
          title={title}
          testId={name}
          name={name}
          options={options}
          handleChange={handleChange}
        />
      );

      validate.titleField(title, name + '-title');
      validateRadioGroupComponents();
    });

    test('with children should return appropriate components', () => {
      const childContent = 'Sample Children';
      const children = <p id="XXX">{childContent}</p>;
      render(
        <OsoCheckboxGroup
          title={title}
          testId={name}
          name={name}
          options={options}
          handleChange={handleChange}
        >
          {children}
        </OsoCheckboxGroup>
      );

      validate.titleField(title, name + '-title');
      validateRadioGroupComponents();
      validate.childField(childContent);
    });

    test('with user change should call mock function', async () => {
      render(
        <OsoCheckboxGroup
          title={title}
          name={name}
          options={options}
          handleChange={handleChange}
        />
      );
      // Simulate user interaction to select the new value
      const user = userEvent.setup();
      await user.click(screen.getByText(/option 3/i));
      expect(handleChange).toHaveBeenCalled();
    });
  });
});
