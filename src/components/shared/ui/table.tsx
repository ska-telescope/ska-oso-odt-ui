import React from 'react';
import { useEffect, useState } from 'react';
import { OsoTitleWrapper } from './titleWrapper';
import { alpha, styled, useTheme } from '@mui/material/styles';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
import {
  DataGrid,
  GridActionsCellItem,
  gridClasses,
  GridRowEditStopReasons,
  GridRowModes,
  GridToolbarContainer
} from '@mui/x-data-grid';
import type {
  GridColDef,
  GridEventListener,
  GridRowId,
  GridRowModel,
  GridRowModesModel,
  GridRowParams,
  GridRowSelectionModel,
  GridRowsProp,
  GridSlots
} from '@mui/x-data-grid';
import { randomId } from '@mui/x-data-grid-generator';

type EditToolbarProps = {
  setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void;
  setRowModesModel: (
    newModel: (oldModel: GridRowModesModel) => GridRowModesModel
  ) => void;
};

const ODD_OPACITY = 0.2;

const StripedDataGrid = styled(DataGrid)(({ theme }) => ({
  [`& .${gridClasses.row}.even`]: {
    backgroundColor:
      theme.palette.mode === 'light'
        ? theme.palette.grey[200]
        : theme.palette.grey[600], // TODO: issue with theme change
    '&:hover': {
      backgroundColor: alpha(theme.palette.primary.main, ODD_OPACITY),
      '@media (hover: none)': {
        backgroundColor: 'transparent'
      }
    },
    '& .MuiSelected': {
      backgroundColor: alpha(
        theme.palette.primary.main,
        ODD_OPACITY + theme.palette.action.selectedOpacity
      ),
      '&:hover': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          ODD_OPACITY +
            theme.palette.action.selectedOpacity +
            theme.palette.action.hoverOpacity
        ),
        // Reset on touch devices, it doesn't add specificity
        '@media (hover: none)': {
          backgroundColor: alpha(
            theme.palette.primary.main,
            ODD_OPACITY + theme.palette.action.selectedOpacity
          )
        }
      }
    }
  }
}));

type TableProps = {
  title?: string;
  data: Array<object | number[] | string[]>;
  columns: GridColDef[];
  handleSelect: (value: number[]) => void;
  handleProcessRowUpdateError?: (error: unknown) => void;
  /* optional fields */
  testId?: string;
  rowsToDisplay?: number;
  withActions?: boolean;
  checkboxSelection?: boolean;
  disableMultipleRowSelection?: boolean;
  handleCellChange?: (
    row: number,
    col: number,
    name: string,
    value: string
  ) => void;
  children?: string | JSX.Element | JSX.Element[];
};

function EditToolbar(props: EditToolbarProps) {
  const { setRows, setRowModesModel } = props;

  const handleClick = () => {
    const id = randomId();
    setRows((oldRows) => [...oldRows, { id, isNew: true }]);
    setRowModesModel((oldModel) => ({
      ...oldModel,
      [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' }
    }));
  };
  return (
    <GridToolbarContainer>
      <Button
        aria-description="Click to add a new row of data"
        name="btnAddNewRow"
        data-cy="btnAddNewRow"
        id="btnAddNewRow"
        sx={{ color: 'blue' }}
        variant="outlined"
        startIcon={<AddIcon />}
        onClick={handleClick}
      >
        Add record
      </Button>
    </GridToolbarContainer>
  );
}

export const OsoTable = ({
  title,
  testId,
  data,
  columns,
  handleSelect,
  withActions,
  handleProcessRowUpdateError,
  handleCellChange,
  checkboxSelection,
  disableMultipleRowSelection,
  children
}: TableProps) => {
  const [rows, setRows] = useState(data);
  const [dataTowModesModel, setDataRowModesModel] = useState<GridRowModesModel>(
    {}
  );
  const theme = useTheme();
  const withTitle = title !== null && title !== undefined;
  const localTitle = withTitle ? title.replaceAll(' ', '') : 'table';
  const titleId = testId ?? `${localTitle}-title`;

  const handleRowEditStop: GridEventListener<'rowEditStop'> = (
    params,
    event
  ) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };

  const handleEditClick = (id: GridRowId) => () => {
    setDataRowModesModel({
      ...dataTowModesModel,
      [id]: { mode: GridRowModes.Edit }
    });
  };

  const handleSaveClick = (id: GridRowId) => () => {
    setDataRowModesModel({
      ...dataTowModesModel,
      [id]: { mode: GridRowModes.View }
    });
  };

  const handleDeleteClick = (id: GridRowId) => () => {
    const newRows = rows.filter((row) => row.id !== id);
    setRows(newRows);
  };

  const handleCancelClick = (id: GridRowId) => () => {
    setDataRowModesModel({
      ...dataTowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true }
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow!.isNew) {
      setRows(rows.filter((row) => row?.id !== id));
    }
  };

  const processRowUpdate = (
    newRow: GridRowModel,
    oldRow: GridRowModel
  ): GridRowModel => {
    const updatedRow = { ...newRow, isNew: false };
    setRows(rows.map((row) => (row?.id === newRow.id ? updatedRow : row)));
    if (handleCellChange) {
      columns.map((column: GridColDef, idx: number) => {
        const name = column.field;
        if (newRow[name] !== oldRow[name]) {
          handleCellChange(newRow.id, idx, name, newRow[name]);
        }
      });
    }
    return updatedRow;
  };

  const handleRowModesModelChange = (newRowModesModel: GridRowModesModel) => {
    setDataRowModesModel(newRowModesModel);
  };

  const handleRowClick = (params: GridRowParams<GridRowParams>) => {
    const newRow = params.id
      ? typeof params.id === 'string'
        ? Number.parseInt(params.id)
        : params.id
      : null;
    if (handleSelect && newRow) {
      // We can pass back params.row for the selected row
      handleSelect([newRow]);
    }
  };

  // Handle external changes and refresh the table content
  useEffect(() => {
    setRows(data);
  }, [data]);

  const action = {
    SaveButton: (id: GridRowId) => {
      const saveId = `btnSave-${id}`;
      return (
        <GridActionsCellItem
          aria-description="Click to save the changes of this row"
          name={saveId}
          data-cy={saveId}
          id={saveId}
          key="saveAction"
          icon={<SaveIcon />}
          label="Save"
          sx={{
            color: 'primary.main'
          }}
          onClick={handleSaveClick(id)}
        />
      );
    },
    CancelButton: (id: GridRowId) => {
      const cancelId = `btnCancel-${id}`;
      return (
        <GridActionsCellItem
          aria-description="Click to cancel the changes of this row"
          name={cancelId}
          data-cy={cancelId}
          id={cancelId}
          key="cancelAction"
          icon={<CancelIcon />}
          label="Cancel"
          className="textPrimary"
          onClick={handleCancelClick(id)}
          color="inherit"
        />
      );
    },
    EditButton: (id: GridRowId) => {
      const editId = `btnEdit-${id}`;
      return (
        <GridActionsCellItem
          aria-description="Click to edit the content of this row"
          name={editId}
          data-cy={editId}
          id={editId}
          key="editAction"
          icon={<EditIcon />}
          label="Edit"
          className="textPrimary"
          onClick={handleEditClick(id)}
          color="inherit"
        />
      );
    },
    DeleteButton: (id: GridRowId) => {
      const deleteId = `btnDelete-${id}`;
      return (
        <GridActionsCellItem
          aria-description="Click to delete this row"
          name={deleteId}
          data-cy={deleteId}
          id={deleteId}
          key="deleteAction"
          icon={<DeleteIcon />}
          label="Delete"
          onClick={handleDeleteClick(id)}
          color="inherit"
        />
      );
    }
  };

  const appendActionsColumn = (): GridColDef => {
    return {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      width: 100,
      cellClassName: 'actions',
      getActions: ({ id }) => {
        if (dataTowModesModel[id]?.mode === GridRowModes.Edit) {
          return [action.SaveButton(id), action.CancelButton(id)];
        }
        return [action.EditButton(id), action.DeleteButton(id)];
      }
    };
  };

  if (withActions ?? false) {
    columns.push(appendActionsColumn());
  }

  const dataRows = rows.map(
    (item: object | number[] | string[], index: number) => ({
      ...item,
      id: index
    })
  );

  const onRowSelectionModelChange = (
    rowSelectionModel: GridRowSelectionModel
  ) => {
    handleSelect(rowSelectionModel.map((row) => parseInt(row.toString())));
  };

  return (
    <OsoTitleWrapper title={title} testId={titleId}>
      <Box
        sx={{
          height: '100v',
          width: '100%',
          '& .actions': {
            color: 'text.secondary'
          },
          '& .textPrimary': {
            color: 'text.primary'
          }
        }}
      >
        <div
          style={{
            maxHeight: '100v',
            maxWidth: '98%',
            overflowY: 'auto'
          }}
        >
          <StripedDataGrid
            rows={dataRows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: { page: 0, pageSize: 5 }
              }
            }}
            editMode="cell"
            sx={{
              '& .MuiDataGrid-columnHeader': {
                backgroundColor: theme.palette.primary.dark,
                color: theme.palette.primary.contrastText
              },
              '& .MuiDataGrid-filler': {
                backgroundColor: theme.palette.action.selected
              },
              '& .MuiSelected': {
                backgroundColor: theme.palette.action.active
              }
            }}
            pageSizeOptions={[5, 10, 25, 50]}
            autoHeight
            checkboxSelection={checkboxSelection ?? false}
            hideFooterSelectedRowCount
            showCellVerticalBorder
            onRowClick={handleRowClick}
            rowModesModel={dataTowModesModel}
            onRowModesModelChange={handleRowModesModelChange}
            onRowSelectionModelChange={onRowSelectionModelChange}
            onRowEditStop={handleRowEditStop}
            onProcessRowUpdateError={handleProcessRowUpdateError}
            processRowUpdate={processRowUpdate}
            getRowClassName={(params: {
              indexRelativeToCurrentPage: number;
            }) =>
              params.indexRelativeToCurrentPage % 2 === 0 ? 'even' : 'odd'
            }
            disableMultipleRowSelection={disableMultipleRowSelection ?? false}
            slots={{
              toolbar: withActions
                ? (EditToolbar as GridSlots['toolbar'])
                : null
            }}
            slotProps={{
              toolbar: {
                setRows: setRows,
                setRowModesModel: setDataRowModesModel
              }
            }}
          />
        </div>
        <div style={{ width: '100%' }}>{children ?? null}</div>
      </Box>
    </OsoTitleWrapper>
  );
};
