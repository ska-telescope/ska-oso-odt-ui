import React from 'react';
import { Select, MenuItem, InputLabel, FormControl } from '@mui/material';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

export type SelectOption = {
  label: string;
  value: string;
};

export const createSelectOptions = (opts: {
  [key: string]: string;
}): SelectOption[] => {
  return Object.values(opts).map((val) => ({ label: val, value: val }));
};

type SelectBaseProps = {
  title: string;
  name: string;
  options: SelectOption[];
  /* optional fields */
  defaultValue?: string;
  placeholder?: string;
  testId?: string;
  children?: string | JSX.Element | JSX.Element[];
};

type SelectWithOnChangeProps = {
  onChange: (event: SelectChangeEvent<string>) => void;
  handleChange?: never;
};

type SelectWithHandleChangeProps = {
  onChange?: never;
  handleChange: (name: string, value: string) => void;
};

// Combine the props into a single mutually exclusive type (with onChange and handleChange)
type SelectProps = SelectBaseProps &
  (SelectWithOnChangeProps | SelectWithHandleChangeProps);

export const OsoSelect = ({
  title,
  name,
  testId,
  options,
  defaultValue,
  placeholder,
  onChange,
  handleChange,
  children
}: SelectProps) => {
  const testIdUsed = testId ?? name;
  const titleId = `${testIdUsed}-title`;

  const selectChangeHandle = (event: SelectChangeEvent<string>) => {
    if (onChange) {
      onChange(event);
    } else if (handleChange) {
      handleChange(name, event.target.value);
    }
  };

  return (
    <FormControl fullWidth>
      <InputLabel id={titleId}>{title}</InputLabel>
      <Select
        label={title}
        labelId={titleId}
        aria-labelledby={titleId}
        name={name}
        id={titleId}
        data-testid={titleId}
        data-cy={titleId}
        value={defaultValue}
        variant={'outlined'}
        onChange={selectChangeHandle}
        placeholder={placeholder}
      >
        {options.map((option: SelectOption) => {
          const optionId = `${testIdUsed}-option-${option.value}`;
          return (
            <MenuItem
              key={optionId}
              value={option.value}
              id={optionId}
              data-testid={optionId}
              data-cy={optionId}
            >
              {option.label}
            </MenuItem>
          );
        })}
      </Select>
      {children ?? null}
    </FormControl>
  );
};
