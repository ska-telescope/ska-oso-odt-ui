import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { SaveSbButton } from './saveSbButton';
import {
  TEST_FORM_LOW_SB_DEFINITION,
  TEST_FORM_MID_SB_DEFINITION,
  WithSbDefinitionFormContext
} from '../testUtils';
import { TelescopeType } from '../../generated/models/telescope-type';
import { SbDefinitionType } from '../../models/sbDefinition/sbDefinition';

describe('SaveSbButton', () => {
  test.each([
    [TelescopeType.SkaMid, TEST_FORM_MID_SB_DEFINITION],
    [TelescopeType.SkaLow, TEST_FORM_LOW_SB_DEFINITION]
  ])(
    'should return save button with appropriate components',
    (telescope: TelescopeType, initialSB: SbDefinitionType) => {
      render(
        <WithSbDefinitionFormContext
          telescopeType={telescope}
          initialState={initialSB}
        >
          <SaveSbButton />
        </WithSbDefinitionFormContext>
      );

      const saveButton = screen.getByRole('button', {
        name: 'Save to ODA'
      });
      expect(saveButton).toBeInTheDocument();
      expect(saveButton).toHaveAttribute('type', 'submit');
      expect(saveButton).toHaveAttribute(
        'aria-describedby',
        'Click to save the changes back to backend'
      );
      expect(saveButton).toHaveAttribute('data-testid', 'save-sb');
    }
  );
});
