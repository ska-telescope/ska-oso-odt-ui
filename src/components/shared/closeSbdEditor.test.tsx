import '@testing-library/jest-dom';
import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { CloseSbdEditor } from './closeSbdEditor';

describe('CloseSbdEditor', () => {
  test('should return appropriate components', () => {
    render(<CloseSbdEditor />);

    const button = screen.getByRole('button', {
      name: /Go to Observing Block/i
    });

    expect(button).toBeInTheDocument();
    expect(button).toBeVisible();
    expect(button).toHaveAttribute('data-testid', 'close-sbd-editor');
    expect(button).toHaveAttribute('aria-label', 'Go to Observing Block');
    expect(button).toHaveAttribute(
      'aria-describedby',
      'Close the current editor and return to the Observing Block'
    );
    expect(button).toHaveAttribute('type', 'submit');
    act(() => {
      fireEvent.click(button);
    });
  });
});
