import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import user from '@testing-library/user-event';
import { PopupDialog } from './popupDialog.js';

describe('PopupDialog', () => {
  const title = 'Pop Up Dialog';
  const content = ['content line1', 'content line2'];
  describe('with open=false', () => {
    test('should return empty content', () => {
      const { container } = render(
        <PopupDialog title={title} content={content} open={false} />
      );
      expect(container.name).toBe(undefined);
    });
  });

  describe('with open=true', () => {
    test('should return right heading and contents and cancel and cotinue buttons', () => {
      render(<PopupDialog title={title} content={content} open={true} />);

      const heading = screen.getByRole('heading', { name: title });
      expect(heading).toHaveAttribute('id', 'dialog-title');
      expect(heading).toHaveAttribute('data-testid', 'dialog-title');

      content.map((text: string) => {
        const label = screen.getByText(text);
        expect(label).toBeInTheDocument();
      });

      const cancelButton = screen.getByRole('button', {
        name: /cancel/i
      });
      expect(cancelButton).toHaveAttribute('type', 'submit');
      expect(cancelButton).toHaveAttribute('aria-label', 'Cancel');
      expect(cancelButton).toHaveAttribute('aria-describedby', 'Cancel Button');
      expect(cancelButton).toHaveAttribute('aria-hidden', 'false');
      expect(cancelButton).toHaveAttribute('data-testid', 'cancelId');

      const continueButton = screen.getByRole('button', {
        name: /continue/i
      });
      expect(continueButton).toHaveAttribute('type', 'submit');
      expect(continueButton).toHaveAttribute('aria-label', 'Continue');
      expect(continueButton).toHaveAttribute(
        'aria-describedby',
        'Continue Button'
      );
      expect(continueButton).toHaveAttribute('aria-hidden', 'false');
      expect(continueButton).toHaveAttribute('data-testid', 'continueId');
    });

    test('with customised button text should return appriopriate button components', () => {
      const cancelButtonLabel = 'Abort';
      const continueButtonLabel = 'Submit';
      render(
        <PopupDialog
          title={title}
          content={content}
          open={true}
          cancelButtonLabel={cancelButtonLabel}
          continueButtonLabel={continueButtonLabel}
        />
      );

      const cancelButton = screen.getByRole('button', {
        name: cancelButtonLabel
      });
      expect(cancelButton).toHaveAttribute('type', 'submit');
      expect(cancelButton).toHaveAttribute('aria-label', cancelButtonLabel);
      expect(cancelButton).toHaveAttribute(
        'aria-describedby',
        cancelButtonLabel + ' Button'
      );
      expect(cancelButton).toHaveAttribute('aria-hidden', 'false');
      expect(cancelButton).toHaveAttribute(
        'data-testid',
        cancelButtonLabel.toLowerCase() + 'Id'
      );

      const continueButton = screen.getByRole('button', {
        name: continueButtonLabel
      });
      expect(continueButton).toHaveAttribute('type', 'submit');
      expect(continueButton).toHaveAttribute('aria-label', continueButtonLabel);
      expect(continueButton).toHaveAttribute(
        'aria-describedby',
        continueButtonLabel + ' Button'
      );
      expect(continueButton).toHaveAttribute('aria-hidden', 'false');
      expect(continueButton).toHaveAttribute(
        'data-testid',
        continueButtonLabel.toLowerCase() + 'Id'
      );
    });

    test.skip('click on cancel should issue the close callback', () => {
      const onClose = vi.fn();
      render(
        <PopupDialog
          title={title}
          content={content}
          open={true}
          onClose={onClose}
        />
      );

      const cancelButton = screen.getByRole('button', {
        name: /cancel/i
      });
      user.userEvent.click(cancelButton);
      expect(onClose).toHaveBeenCalledOnce();
    });

    test.skip('click on continue should return dialog response and close callback', () => {
      const onClose = vi.fn();
      const onDialogResponse = vi.fn();
      render(
        <PopupDialog
          title={title}
          content={content}
          open={true}
          onClose={onClose}
          onDialogResponse={onDialogResponse}
        />
      );

      const continueButton = screen.getByRole('button', {
        name: /continue/i
      });
      user.userEvent.click(continueButton);
      expect(onDialogResponse).toHaveBeenCalledOnce();
      expect(onClose).toHaveBeenCalledOnce();
    });
  });
});
