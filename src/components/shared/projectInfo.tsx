import React from 'react';
import { Box, Grid, useTheme } from '@mui/material';
import { useStore } from '../../store/store';
import { TelescopeType } from '../../generated/models/telescope-type';
import { useProjectStore } from '../../store/projectStore';
import { useLocation } from 'react-router-dom';
import { RoutePaths } from '../app/routes';

export const ProjectInfo = (): React.JSX => {
  const theme = useTheme();
  const { pathname } = useLocation();

  const [projectId, projectName] = useProjectStore((state) => [
    state.project.prj_id,
    state.project.name
  ]);

  const [sbdId, telescope] = useStore((state) => [
    state.sbDefinition.sbd_id,
    state.sbDefinition.telescope
  ]);

  const inSbdEditor = pathname.includes(
    RoutePaths.SbdEditor.root.replaceAll('/*', '')
  );

  const detailsString = () => {
    const projectPart = projectId
      ? `${projectName} (${projectId})`
      : projectName;

    if (sbdId && inSbdEditor) {
      return `${projectPart} - ${sbdId}`;
    }
    return projectPart;
  };

  return (
    <Box
      sx={{
        display: 'flex',
        color: theme.palette.background.default,
        verticalAlign: 'center',
        paddingLeft: '10px',
        paddingRight: '0px',
        backgroundColor: theme.palette.secondary.main,
        '& > *': { m: 1 }
      }}
      aria-label="projectInfo"
    >
      <Grid container>
        <Grid item xs={11}>
          <span>{detailsString()}</span>
        </Grid>
        {inSbdEditor && (
          <Grid item xs={1} textAlign={'right'} pr={2}>
            {telescope === TelescopeType.SkaMid ? 'Mid' : 'Low'}
          </Grid>
        )}
      </Grid>
    </Box>
  );
};
