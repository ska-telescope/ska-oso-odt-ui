import {
  Controller,
  FieldValues,
  FieldPath,
  useFormContext
} from 'react-hook-form';
import {
  MenuItem,
  Select,
  FormControlProps,
  InputLabel,
  FormControl,
  SelectChangeEvent
} from '@mui/material';
import { SelectOption } from '../ui';

type ControlledSelectProps<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
> = {
  name: TName;
  options: SelectOption[];
  label?: string;
  onChange?: (event: SelectChangeEvent) => void;
  defaultValue?: string;
  value?: string;
  // This flag decides whether the change should call the RHF change handler and cause the form to update. It should only be set to false
  // if an onChange is provided that will handle the form update within the component (for example after a confirmation dialog has been clicked).
  withFormUpdate?: boolean;
} & FormControlProps;

export const ControlledSelect = <
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>({
  name,
  options,
  label = '',
  onChange = undefined,
  defaultValue = '',
  fullWidth = true,
  value = undefined,
  withFormUpdate = true,
  ...otherProps
}: ControlledSelectProps<TFieldValues, TName>): JSX.Element => {
  const { control } = useFormContext<TFieldValues>();
  const labelId = `${name}-label`;
  if (!withFormUpdate && !onChange) {
    throw new Error(
      "An onChange function that handles the form update should be passed if you don't want this ControlledSelect component to update the form"
    );
  }
  return (
    <Controller
      control={control}
      name={name}
      render={({ field }) => (
        <FormControl fullWidth={fullWidth} {...otherProps}>
          <InputLabel id={labelId}>{label}</InputLabel>
          <Select
            {...field}
            label={label}
            labelId={labelId}
            value={value ?? field.value ?? ''}
            onChange={(evt: SelectChangeEvent) => {
              if (withFormUpdate) {
                field.onChange(evt); // RHF manages its own onChange to track the form field value so need to call that here
              }
              if (onChange) {
                onChange(evt);
              }
            }}
            defaultValue={defaultValue}
          >
            {options?.map((opt: SelectOption) => (
              <MenuItem key={opt.value} value={opt.value}>
                {opt.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}
    />
  );
};
