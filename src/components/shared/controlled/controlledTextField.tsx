import {
  Controller,
  FieldPath,
  FieldValues,
  useFormContext
} from 'react-hook-form';
import { TextField, TextFieldProps } from '@mui/material';
import { ChangeEventHandler } from 'react';

type ControlledTextFieldProps<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
> = {
  name: TName;
  label: TName;
  variant?: string;
  onChange?: (
    event: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>
  ) => void;
} & TextFieldProps;

export const ControlledTextField = <
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>({
  name,
  label,
  variant = 'outlined',
  fullWidth = true,
  type = 'text',
  id = undefined,
  onChange = undefined,
  ...otherProps
}: ControlledTextFieldProps<TFieldValues, TName>): JSX.Element => {
  const { control } = useFormContext<TFieldValues>();
  return (
    <Controller
      control={control}
      name={name}
      key={name}
      render={({ field, fieldState }) => (
        <TextField
          {...field}
          // Default here required to prevent
          // "Warning: A component is changing an uncontrolled input to be controlled.""
          value={field.value ?? ''}
          label={label}
          type={type}
          variant={variant}
          fullWidth={fullWidth}
          helperText={fieldState.error?.message ?? null}
          error={!!fieldState.error}
          id={id ?? name}
          onChange={(evt) => {
            field.onChange(evt); // RHF manages its own onChange to track the form field value so need to call that here
            if (onChange) {
              onChange(evt);
            }
          }}
          {...otherProps}
        />
      )}
    />
  );
};
