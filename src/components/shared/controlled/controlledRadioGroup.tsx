import React from 'react';
import { FormControlLabel, Radio, RadioGroup } from '@mui/material';
import type { SyntheticEvent } from 'react';
import { OsoTitle } from '../ui/title';
import { useTheme } from '@mui/material/styles';
import { SelectOption } from '../ui/select';
import { Controller, useFormContext } from 'react-hook-form';

type RadioButtonGroupProps = {
  name: string;
  options: SelectOption[];
  /* optional fields */
  title?: string;
  value?: string;
  testId?: string;
  horizontal?: boolean;
  handleChange?: (optionValue: string) => void;
  children?: string | JSX.Element | JSX.Element[];
};

export const ControlledRadioButtonGroup = ({
  name,
  options,
  testId,
  title,
  handleChange,
  children,
  horizontal
}: RadioButtonGroupProps) => {
  const theme = useTheme();
  const testIdUsed = testId ?? name;
  const titleId = `${testIdUsed}-title`;
  const { control } = useFormContext();
  return (
    <Controller
      control={control}
      name={name}
      render={({ field }) => (
        <fieldset
          style={{
            border: '1px solid ' + theme.palette.primary.dark,
            flexDirection: 'row',
            borderRadius: '10px',
            marginRight: '20px',
            width: '100%'
          }}
        >
          {title && <OsoTitle title={title} titleId={titleId} />}
          <RadioGroup
            row={horizontal ?? false}
            name={name}
            style={{
              marginLeft: horizontal ? '20px' : '1px'
            }}
            value={field.value}
            onChange={(evt: SyntheticEvent) => {
              field.onChange(evt);
              if (handleChange) {
                handleChange((evt.target as HTMLInputElement).value);
              }
            }}
          >
            {options
              .filter(Boolean)
              .map((option: SelectOption, index: number) => {
                const optionId =
                  testIdUsed + '-radio-' + option.value.replaceAll(' ', '');
                return (
                  <FormControlLabel
                    style={{
                      marginRight: '20px'
                    }}
                    data-cy={optionId}
                    id={optionId}
                    key={index}
                    value={option.value}
                    control={<Radio color={'default'} />}
                    label={option.label}
                  />
                );
              })}
          </RadioGroup>
          {children ?? null}
        </fieldset>
      )}
    />
  );
};
