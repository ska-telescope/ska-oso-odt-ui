import React, { useState } from 'react';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import apiService from '../../services/apiService';
import { useStore } from '../../store/store';
import { AlertDialog } from './dialogs';
import { Alert, Snackbar } from '@mui/material';

export const ValidateSB = () => {
  const [successAlert, setSuccessAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const sbDefinition = useStore((state) => state.sbDefinition);

  const handleValidateSb = async () => {
    try {
      if (sbDefinition) {
        const validateResult = await apiService.validateSBFn(
          sbDefinition as JSON
        );
        setTimeout(() => {}, 5000);
        if (validateResult.data && validateResult.data.messages?.length > 0) {
          const errorMessage = validateResult.data.messages.join(', ');
          setErrorMessage(`Failed: ${errorMessage}`);
        } else {
          setSuccessAlert(true);
        }
      } else {
        setErrorMessage('Empty SB cannot be validated.');
      }
    } catch (e) {
      setErrorMessage(`Validating SB failed ${e?.message}`);
    }
  };

  return (
    <>
      <Snackbar
        open={successAlert}
        onClose={() => setSuccessAlert(false)}
        autoHideDuration={3000} // Automatically hide after 3 seconds
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }} // Position
      >
        <Alert severity="success" variant="filled">
          SB has been validated successfully.
        </Alert>
      </Snackbar>
      <AlertDialog
        open={!!errorMessage}
        title="Error while saving to the ODA."
        content={errorMessage}
        onClose={() => setErrorMessage('')}
      />
      <Button
        testId="validate-sb"
        ariaDescription="Click to validate current SB"
        label="Validate"
        onClick={handleValidateSb}
        color={ButtonColorTypes.Success}
        variant={ButtonVariantTypes.Contained}
        disabled
      />
    </>
  );
};
