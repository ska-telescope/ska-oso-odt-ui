import React from 'react';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import { useNavigate } from 'react-router-dom';
import { RoutePaths } from '../app/routes';

export const CloseSbdEditor = (): JSX.Element => {
  const navigate = useNavigate();
  // TODO need to decide whether this should save as well of throw a warning
  return (
    <Button
      testId="close-sbd-editor"
      ariaDescription="Close the current editor and return to the Observing Block"
      label="Go to Observing Block"
      color={ButtonColorTypes.Inherit}
      variant={ButtonVariantTypes.Contained}
      onClick={() => navigate(RoutePaths.ObservingBlock)}
    />
  );
};
