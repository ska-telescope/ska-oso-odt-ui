import React from 'react';
import { Button, Grid } from '@mui/material';
import { MouseEvent } from 'react';
import { useNavigate } from 'react-router-dom';
import { RoutePaths } from '../app/routes';

export type PopupDialogProps = {
  isValid: boolean;
  reset: (value: object) => void | null;
  blankProps: object | null;
  disabledAll: boolean | null;
};

export const ComponentButtonGroup = ({
  isValid,
  reset,
  blankProps,
  disabledAll
}: PopupDialogProps): JSX.Element => {
  const navigate = useNavigate();

  const gotoHome = (e: MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    const homeHref = `${window.env.BASE_URL.substring(1) || ''}${RoutePaths.Home}`;
    navigate(
      window.location.pathname !== homeHref ? homeHref : RoutePaths.Home
    );
  };

  const handleReset = () => {
    if (reset && typeof reset === 'function') {
      reset(blankProps);
    }
  };

  return (
    <Grid container padding={2}>
      <Grid item>
        <Button
          aria-label="reset"
          data-testid="btnReset"
          onClick={() => handleReset()}
          color="warning"
          variant="contained"
          disabled={disabledAll}
        >
          Reset
        </Button>
      </Grid>
      &nbsp;&nbsp;&nbsp;
      <Grid item>
        <Button
          aria-label="save"
          data-testid="btnSave"
          type="submit"
          color="success"
          variant="contained"
          disabled={!isValid || disabledAll}
        >
          Save
        </Button>
      </Grid>
      &nbsp;&nbsp;&nbsp;
      <Grid item>
        <Button
          aria-label="cancel"
          data-testid="btnCancel"
          onClick={gotoHome}
          color="inherit"
          variant="contained"
        >
          Cancel
        </Button>
      </Grid>
    </Grid>
  );
};
