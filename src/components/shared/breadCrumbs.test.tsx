import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { BreadCrumbs } from './breadCrumbs';

export const checkBreadCrumbNumbers = () => {
  [1, 2, 3, 4, 5, 6, 7].forEach((breadCrumb) => {
    const stepper = screen.getByText(breadCrumb);
    expect(stepper).toBeInTheDocument();
    expect(stepper).toBeVisible();
    expect(stepper).toHaveAttribute('x', '12');
    expect(stepper).toHaveAttribute('y', '12');
    expect(stepper).toHaveAttribute('text-anchor', 'middle');
    expect(stepper).toHaveAttribute('dominant-baseline', 'central');
  });
};

describe('BreadCrumbs', () => {
  test('should return 7 numbers', () => {
    render(<BreadCrumbs />);

    checkBreadCrumbNumbers();
  });
  test('should return 7 appropriate step label', () => {
    render(<BreadCrumbs />);

    // check 7 x step labels
    [
      'General',
      'Scripts',
      'Array',
      'Targets',
      'Signal Processor',
      'Data Processing',
      'Scans'
    ].forEach((title: string) => {
      expect(screen.getByText(title)).toBeInTheDocument();
    });
  });

  describe('Click links', () => {
    [1, 2, 3, 4, 5, 6, 7].forEach((index: number) => {
      test(`click ${index} link should return appropriate component`, () => {
        render(<BreadCrumbs />);

        const stepper = screen.getByText(index);
        fireEvent.click(stepper);
      });
    });
  });
});
