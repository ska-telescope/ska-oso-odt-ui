import React, { useState } from 'react';
import {
  render,
  screen,
  fireEvent,
  within,
  waitForElementToBeRemoved
} from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { ConfirmDialog } from './dialogs';

describe('ConfirmDialog', () => {
  const title = 'Loading Scheduling Block';
  const content =
    'Your existing scheduling block will be overwritten. Are you sure you really want to load scheduling block ?';
  let confirmed = false;

  const DialogWrapper = () => {
    const [open, setOpen] = useState(true);
    const closeConfirmEvent = () => {
      setOpen(false);
      confirmed = true;
    };
    const closeCancelEvent = () => {
      setOpen(false);
      confirmed = false;
    };
    return (
      <ConfirmDialog
        title={title}
        content={content}
        onConfirmClose={closeConfirmEvent}
        onCancelClose={closeCancelEvent}
        open={open}
      />
    );
  };
  test('should render the dialog correctly and call correct function on OK', async () => {
    confirmed = false;
    render(<DialogWrapper />);
    expect(confirmed).toBeFalsy();

    const dialog = screen.getByRole('dialog');
    expect(dialog).toBeInTheDocument();

    const { getByText } = within(screen.getByRole('dialog'));
    expect(getByText(content)).toBeInTheDocument();

    const confirmButton = screen.getByRole('button', { name: 'OK' });
    expect(confirmButton).toBeInTheDocument();

    fireEvent.click(confirmButton);
    await waitForElementToBeRemoved(() => screen.queryByRole('dialog'));
    expect(confirmed).toBeTruthy();
    expect(screen.queryByRole('dialog')).toBeNull();
  });

  test('should call correct function on Cancel', async () => {
    confirmed = false;
    render(<DialogWrapper />);
    expect(confirmed).toBeFalsy();

    const dialog = screen.getByRole('dialog');
    expect(dialog).toBeInTheDocument();

    const cancelButton = screen.getByRole('button', { name: 'Cancel' });
    expect(cancelButton).toBeInTheDocument();

    fireEvent.click(cancelButton);
    await waitForElementToBeRemoved(() => screen.queryByRole('dialog'));
    expect(confirmed).toBeFalsy();
    expect(screen.queryByRole('dialog')).toBeNull();
  });
});
