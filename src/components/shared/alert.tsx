import React from 'react';
import ErrorIcon from '@mui/icons-material/Error';
import InfoIcon from '@mui/icons-material/Info';
import WarningIcon from '@mui/icons-material/Warning';
import { Alert, Stack } from '@mui/material';

export type FormDAlertProps = {
  error: string;
};

export const FormAlert = ({ error }: { error: string }): JSX.Element => (
  <Stack width="100%" spacing={2} id="error-box">
    <Alert
      icon={<ErrorIcon fontSize="inherit" />}
      variant="filled"
      severity="error"
      id="alert-message"
    >
      {error}
    </Alert>
  </Stack>
);

export const FormWarning = ({ warning }: { warning: string }) => (
  <Stack width="100%" spacing={2}>
    <Alert
      icon={<WarningIcon fontSize="inherit" />}
      variant="filled"
      severity="warning"
      id="warning-message"
    >
      {warning}
    </Alert>
  </Stack>
);

export const FormInfo = ({ content }: { content: string }) => (
  <Stack width="100%" spacing={2}>
    <Alert
      icon={<InfoIcon fontSize="inherit" />}
      variant="filled"
      severity="info"
      id="info-message"
    >
      {content}
    </Alert>
  </Stack>
);
