import { act, fireEvent, render, screen } from '@testing-library/react';
import { describe, test } from 'vitest';
import { ProjectCreate } from './projectCreate';
import { mockedUseNavigate } from '../../setUpTests';

describe('ProjectCreate', () => {
  test('should create a new Project in the store and navigate to the /project page', () => {
    render(<ProjectCreate />);
    const button = screen.getByRole('button');
    act(() => {
      fireEvent.click(button);
    });
    expect(mockedUseNavigate).toHaveBeenCalledWith('/project');
  });
});
