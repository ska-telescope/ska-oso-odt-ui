import {
  act,
  fireEvent,
  render,
  screen,
  waitFor
} from '@testing-library/react';
import { describe, test, vi } from 'vitest';
import { ProjectSearch } from './projectSearch';
import apiService from '../../services/apiService';
import { getDefaultProject } from '../../store/defaults';
import { mockedUseNavigate } from '../../setUpTests';

describe('ProjectSearch', () => {
  test('should call the API when the button is clicked', async () => {
    vi.spyOn(apiService, 'loadProject').mockImplementation(() => ({
      data: getDefaultProject()
    }));

    render(<ProjectSearch />);
    const button = screen.getByRole('button');
    await fireEvent.click(button);

    expect(mockedUseNavigate).toHaveBeenCalledWith('/project');
  });

  test('should open an alert when the API returns an error', async () => {
    vi.spyOn(apiService, 'loadProject').mockImplementation(() => ({
      error: 'some error message'
    }));

    render(<ProjectSearch />);
    const button = screen.getByRole('button');
    act(() => {
      fireEvent.click(button);
    });
    await waitFor(() => {
      const warningDialog = screen.getByRole('dialog');
      expect(warningDialog.textContent).toContain('some error message');
    });
  });
});
