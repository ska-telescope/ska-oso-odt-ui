import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
  within
} from '@testing-library/react';
import {
  WithProjectFormContext,
  TEST_PROJECT_FORM,
  getTableRows
} from 'src/components/testUtils';
import { SbDefinitionsTable } from './sbDefinitionsTable';
import { mockedUseNavigate } from '../../setUpTests';
import { expect, vi } from 'vitest';
import apiService from '../../services/apiService';
import { getDefaultSbDefinition } from '../../store/defaults';

describe('SbDefinitionsTable component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <SbDefinitionsTable />
      </WithProjectFormContext>
    );

    const rows = getTableRows('Scheduling Block Definitions');

    expect(rows.length).eq(2);
    const dataCells = within(rows[0]).getAllByRole('cell');
    expect(dataCells.length).eq(4);
    expect(within(dataCells[0]).getByRole('textbox').value).eq('sbd-123');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('TODO');
  });

  test('should navigate to the SbDefinition Editor when the edit button is clicked', async () => {
    const mockLoadSBFn = vi.fn(() => ({
      data: getDefaultSbDefinition('ska_low')
    }));
    vi.spyOn(apiService, 'loadSBFn').mockImplementation(mockLoadSBFn);

    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <SbDefinitionsTable />
      </WithProjectFormContext>
    );

    const editButton = screen.getByRole('button', {
      name: `observingBlocks.0.sbDefinitions.0.edit`
    });

    act(() => {
      fireEvent.click(editButton);
    });
    await waitFor(() =>
      expect(mockedUseNavigate).toHaveBeenCalledWith('/sbd/general')
    );
    expect(mockLoadSBFn).toHaveBeenCalled();
  });

  test('should delete the correct Scheduling Block Definitions row when the delete icon is clicked', () => {
    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <SbDefinitionsTable />
      </WithProjectFormContext>
    );
    // Delete the first Scheduling Block Definitions
    const deleteButton = screen.getByRole('button', {
      name: `observingBlocks.0.sbDefinitions.0.delete`
    });

    act(() => {
      fireEvent.click(deleteButton);
    });
    const rows = getTableRows('Scheduling Block Definitions');
    expect(rows.length).eq(1);
    const dataCells = within(rows[0]).getAllByRole('cell');
    // Now the first row should be the initial second row
    expect(within(dataCells[0]).getByRole('textbox').value).eq('sbd-456');
  });
});
