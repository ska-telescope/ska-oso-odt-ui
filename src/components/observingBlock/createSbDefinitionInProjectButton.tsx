import React, { useState } from 'react';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import apiService from '../../services/apiService';
import { useProjectStore } from '../../store/projectStore';
import { AlertDialog } from '../shared/dialogs';
import { ProjectInput } from 'src/generated/models/project-input';
import { useNavigate } from 'react-router-dom';
import { useStore } from '../../store/store';
import { SBDefinitionInput } from '../../generated/models/sbdefinition-input';
import { RoutePaths } from '../app/routes';
import { getDefaultSbDefinition } from '../../store/defaults';
import { TelescopeType } from 'src/generated/models/telescope-type';
import { useFormContext } from 'react-hook-form';

interface Props {
  telescope: TelescopeType;
}

export const CreateSbDefinitionInProjectButton = ({ telescope }: Props) => {
  const [errorMessage, setErrorMessage] = useState('');
  const { getValues } = useFormContext();

  const navigate = useNavigate();

  const [setPdmProject, obsBlockId] = useProjectStore((state) => [
    state.setPdmProject,
    state.inProgressObservingBlock!.obsBlockId
  ]);

  const setPdmSbDefinition = useStore((state) => state.setPdmSbDefinition);

  const handleCreateSbDefinitionInProject = async () => {
    if (!getValues('code')) {
      setErrorMessage(
        'Project identifier not available. Please save the Project to the ODA.'
      );
      return;
    }
    try {
      const response = await apiService.createSbDefinitionInProjectFn(
        getValues('code'),
        obsBlockId,
        getDefaultSbDefinition(telescope)
      );

      if (response.data) {
        setPdmProject(response.data.prj as ProjectInput);
        setPdmSbDefinition(response.data.sbd as SBDefinitionInput);
        navigate(RoutePaths.SbdEditor.General);
      } else {
        setErrorMessage(JSON.stringify(response.error!));
      }
    } catch (e) {
      setErrorMessage(e?.message);
    }
  };
  const label = telescope == TelescopeType.SkaMid ? 'Mid SBD' : 'Low SBD';
  return (
    <div>
      <AlertDialog
        open={!!errorMessage}
        title="Error while creating Scheduling Block Definition in ODA."
        content={errorMessage}
        onClose={() => setErrorMessage('')}
      />
      <Button
        testId={`create-${telescope}-sbd`}
        ariaDescription="Click to create a new Scheduling Block Definition"
        label={`Create ${label}`}
        onClick={handleCreateSbDefinitionInProject}
        color={ButtonColorTypes.Secondary}
        variant={ButtonVariantTypes.Contained}
      />
    </div>
  );
};
