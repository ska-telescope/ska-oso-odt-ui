import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor
} from '@testing-library/react';
import {
  WithProjectFormContext,
  TEST_PROJECT_FORM
} from 'src/components/testUtils';
import { expect, vi } from 'vitest';
import apiService from '../../services/apiService';
import {
  getDefaultProject,
  getDefaultSbDefinition
} from '../../store/defaults';
import { mockedUseNavigate } from '../../setUpTests';
import { CreateSbDefinitionInProjectButton } from './createSbDefinitionInProjectButton';
import { TelescopeType } from '../../generated/models/telescope-type';

describe('CreateSbDefinitionInProjectButton', () => {
  test('should navigate to the SbDefinition Editor when the button is clicked', async () => {
    const mockCreateSbDefinitionInProjectFn = vi.fn(() => ({
      data: { sbd: getDefaultSbDefinition('ska_low'), prj: getDefaultProject() }
    }));
    vi.spyOn(apiService, 'createSbDefinitionInProjectFn').mockImplementation(
      mockCreateSbDefinitionInProjectFn
    );

    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <CreateSbDefinitionInProjectButton telescope={TelescopeType.SkaLow} />
      </WithProjectFormContext>
    );

    const button = screen.getByRole('button', {
      name: 'Create Low SBD'
    });

    act(() => {
      fireEvent.click(button);
    });
    await waitFor(() =>
      expect(mockedUseNavigate).toHaveBeenCalledWith('/sbd/general')
    );
    expect(mockCreateSbDefinitionInProjectFn).toHaveBeenCalled();
  });
});
