import { ProjectInfo } from '../shared/projectInfo';
import { Box } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { SaveSbButton } from '../shared/saveSbButton';
import { CloseSbdEditor } from '../shared/closeSbdEditor';
import { BreadCrumbs } from '../shared/breadCrumbs';
import { ValidateSB } from '../shared/validateSB';
import { MarkAsReady } from '../shared/markAsReady';
import React from 'react';
import { HomeButton } from '../shared/homeButton';

export const SbdEditorHeader = (): React.JSX => {
  return (
    <Box>
      <ProjectInfo />
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        pb={2}
      >
        <Grid
          size={{ xs: 12, sm: 10, md: 3 }}
          columnSpacing={1}
          justifyContent="flex-start"
        >
          <Grid
            container
            spacing={1}
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
            pl={2}
          >
            <Grid>
              <HomeButton />
            </Grid>
            <Grid>
              <SaveSbButton />
            </Grid>
            <Grid>
              <CloseSbdEditor />
            </Grid>
          </Grid>
        </Grid>
        <Grid size={{ xs: 12, sm: 10, md: 6 }}>
          <BreadCrumbs />
        </Grid>
        <Grid size={{ xs: 12, sm: 10, md: 3 }} justifyContent="flex-end">
          <Grid
            container
            spacing={1}
            direction="row"
            alignItems="center"
            justifyContent="flex-end"
            pr={2}
          >
            <Grid>
              <ValidateSB />
            </Grid>
            <Grid>
              <MarkAsReady />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};
