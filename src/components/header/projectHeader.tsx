import { ProjectInfo } from '../shared/projectInfo';
import { Box } from '@mui/material';
import Grid from '@mui/material/Grid2';
import React from 'react';
import { HomeButton } from '../shared/homeButton';
import { SaveProjectButton } from '../shared/saveProjectButton';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import { useLocation, useNavigate } from 'react-router-dom';
import { RoutePaths } from '../app/routes';

export const ProjectHeader = (): React.JSX => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const inObsBlockEditor = pathname.includes(RoutePaths.ObservingBlock);
  return (
    <Box>
      <ProjectInfo />
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="flex-start"
        pb={2}
        pl={2}
        columnSpacing={1}
        style={{ marginTop: '20px' }}
      >
        <Grid>
          <HomeButton />
        </Grid>
        <Grid>
          <SaveProjectButton />
        </Grid>
        {inObsBlockEditor && (
          <Grid>
            <Button
              testId="go-to-project"
              ariaDescription="Click to return to the Project editor"
              label="Go to Project"
              onClick={() => navigate(RoutePaths.Project)}
              color={ButtonColorTypes.Inherit}
              variant={ButtonVariantTypes.Contained}
            />
          </Grid>
        )}
      </Grid>
    </Box>
  );
};
