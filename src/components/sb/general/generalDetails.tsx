import { Card, CardContent, Stack } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { useFormContext } from 'react-hook-form';
import { OSOStaticInput } from '../../shared/ui/staticInput';
import { OsoTitleWrapper } from '../../shared/ui/titleWrapper';
import { ControlledTextField } from '../../shared/controlled';

const ctrlName = 'general';

export const GeneralDetails = () => {
  const { getValues } = useFormContext();

  const generalValue = getValues(ctrlName);

  return (
    <Card data-testid="sbGeneral" variant="outlined">
      <CardContent>
        <Grid container width="100%" alignItems="center">
          <Grid size={{ xs: 12, md: 6, lg: 6 }} padding="15px">
            <OsoTitleWrapper title="Metadata">
              <Stack spacing={2}>
                <ControlledTextField name={`${ctrlName}.name`} label="Name" />
                <OSOStaticInput
                  label="SBD ID"
                  name="sbdId"
                  value={generalValue?.sbdId}
                />
                <OSOStaticInput
                  label="Version"
                  name="version"
                  value={generalValue?.version}
                />
                <OSOStaticInput
                  label="Created By"
                  name="created_by"
                  value={generalValue?.createdBy}
                />
                <OSOStaticInput
                  label="Created On"
                  name="created_on"
                  value={generalValue?.createdOn}
                />
                <OSOStaticInput
                  label="Last Modified By"
                  name="last_modified_by"
                  value={generalValue?.lastModifiedBy}
                />
                <OSOStaticInput
                  label="Last Modified On"
                  name="last_modified_on"
                  value={generalValue?.lastModifiedOn}
                />
              </Stack>
            </OsoTitleWrapper>
          </Grid>
          <Grid size={{ xs: 12, md: 6, lg: 6 }} sx={{ p: '15px', pt: '0px' }}>
            <ControlledTextField
              name={`${ctrlName}.description`}
              label="Description"
              multiline
              minRows={20}
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
