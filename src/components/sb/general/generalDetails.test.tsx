import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { GeneralDetails } from './generalDetails';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_GENERAL
} from 'src/components/testUtils';
import React from 'react';

describe('GeneralDetails', () => {
  test('should display the values from the form', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_GENERAL}>
        <GeneralDetails />
      </WithSbDefinitionFormContext>
    );
    expect(screen.getByLabelText('Name').value).eq('SBD Name');
    expect(screen.getByLabelText('SBD ID').value).eq('sbd-123');
    expect(screen.getByLabelText('Description').value).eq('SB Description');
    expect(screen.getByLabelText('Version').value).eq('1');
    expect(screen.getByLabelText('Created By').value).eq('Example User');
    expect(screen.getByLabelText('Created On').value).eq('2024-12-12');
    expect(screen.getByLabelText('Last Modified By').value).eq('Example User');
    expect(screen.getByLabelText('Last Modified On').value).eq('2024-12-12');
  });
});
