import React from 'react';
import { useFormContext } from 'react-hook-form';
import {
  OsoDataTable,
  DisplayType,
  OsoDataTableColumnDefinitions,
  createTableDataFromFormValues
} from '../../shared/ui';
import {
  LOW_SPECTRAL_WINDOW_BANDWIDTH_MAX_MHZ,
  LOW_SPECTRAL_WINDOW_BANDWIDTH_MIN_MHZ,
  LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MAX_MHZ,
  LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MIN_MHZ,
  LowSpectralWindowScheme,
  LowSpectralWindowSchemeType,
  SpectralWindowMode
} from '../../../models/signalProcessor/spectralWindow';
import { SelectOption } from '../../shared/ui';
import { OperationsButtonGroup } from '../../shared/operationButtonGroup';
import {
  getLowContinuumResolutionDisplayValue,
  getLowPstResolutionDisplayValue
} from '../../../store/mappers/signalProcessorMapper';

interface Props {
  selectedSignalProcessorIndex: number;
}

export const LowSpectralWindowsTable = ({
  selectedSignalProcessorIndex
}: Props): JSX.Element => {
  const ctrlName =
    `signalProcessor.${selectedSignalProcessorIndex}.spectralWindows` as const;
  const { getValues, setValue } = useFormContext();

  const doPst = getValues(
    `signalProcessor.${selectedSignalProcessorIndex}.doPst`
  );

  const addLowSpectralWindow = () => {
    const newSpectralWindow = LowSpectralWindowScheme.parse({});
    setValue(ctrlName, [...(getValues(ctrlName) ?? []), newSpectralWindow]);
  };

  const deleteLowSpectralWindow = (index: number) => {
    setValue(ctrlName, getValues(ctrlName).toSpliced(index, 1));
  };

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'spwId',
      isIdField: true
    },
    {
      displayType: [DisplayType.StaticField],
      title: 'SPW',
      columnName: 'spw'
    },
    {
      displayType: [DisplayType.TextField],
      title: 'Mode',
      columnName: 'mode',
      disabled: true
    },
    {
      displayType: [DisplayType.NumberField],
      title: 'Centre Frequency (MHz)',
      columnName: 'centreFrequencyMhz',
      stepSize: 1.5625,
      min: LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MIN_MHZ,
      max: LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MAX_MHZ,
      disabledByValueCondition: { fieldName: 'mode', value: 'PST' }
    },
    {
      displayType: [DisplayType.NumberField],
      title: 'Bandwidth (MHz)',
      columnName: 'bandwidthMhz',
      stepSize: 6.25,
      min: LOW_SPECTRAL_WINDOW_BANDWIDTH_MIN_MHZ,
      max: LOW_SPECTRAL_WINDOW_BANDWIDTH_MAX_MHZ,
      disabledByValueCondition: { fieldName: 'mode', value: 'PST' }
    },
    {
      displayType: [DisplayType.SelectField, DisplayType.NumberField],
      title: 'Av. Time (s)',
      columnName: 'averageTimeS',
      options: averageTimeOptions,
      displayTypeCondition: {
        fieldName: 'mode',
        value: SpectralWindowMode.enum.CORR
      },
      disabledByValueCondition: {
        negate: true,
        fieldName: 'mode',
        value: SpectralWindowMode.enum.CORR
      }
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      description: 'Click to delete this spectral window entry',
      columnName: 'delete',
      onClick: deleteLowSpectralWindow,
      disabled: doPst || getValues(ctrlName).length === 1
    }
  ];

  const tableContent = createTableDataFromFormValues(
    columns,
    getValues(ctrlName).filter(
      (spectralWindow: LowSpectralWindowSchemeType) =>
        spectralWindow.mode === SpectralWindowMode.enum.CORR || doPst
    )
  );

  // TODO this is a similar hack to the scans page, where a change to the form (in this case the centre frequency)
  // needs to trigger a change elsewhere in the form (the resolution). Previously this was done at the store level and
  // then rewritten to the form, but we removed that part of the loop.
  const resolutionIndexInTable = 5;
  const resolutions = getValues(ctrlName).map((spectralWindow) =>
    spectralWindow.mode === SpectralWindowMode.enum.CORR
      ? getLowContinuumResolutionDisplayValue(
          spectralWindow.centreFrequencyMhz * 1e6
        )
      : getLowPstResolutionDisplayValue(spectralWindow.centreFrequencyMhz * 1e6)
  );

  columns.splice(resolutionIndexInTable, 0, {
    displayType: [DisplayType.StaticField],
    title: 'Resolution (kHz, km/s)',
    columnName: 'resolution'
  });

  tableContent.forEach((row, index) =>
    row.splice(resolutionIndexInTable, 0, {
      columnName: 'resolution',
      value: resolutions[index]
    })
  );

  return (
    <div>
      <OsoDataTable baseName={ctrlName} data={tableContent} columns={columns} />
      <OperationsButtonGroup
        title="Correlation SPW"
        count={1}
        handleAdd={addLowSpectralWindow}
        disabled={doPst}
      />
    </div>
  );
};

const averageTimeOptions: SelectOption[] = [
  { value: '0.849', label: '0.849' },
  { value: '0.283', label: '0.283' }
];
