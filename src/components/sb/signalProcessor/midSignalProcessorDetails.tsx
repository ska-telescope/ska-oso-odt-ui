import React from 'react';
import { useFormContext } from 'react-hook-form';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import Grid from '@mui/material/Grid2';

import {
  ControlledSelect,
  ControlledTextField,
  ControlledNumberField
} from '../../shared/controlled';
import { MidSpectralWindowsTable } from './midSpectralWindowsTable';
import { SelectOption } from '../../shared/ui';
import {
  MidBandsDefaults,
  MidBand
} from '../../../models/signalProcessor/spectralWindow';

interface Props {
  selectedSignalProcessorIndex: number;
}

export const MidSignalProcessorDetails = ({
  selectedSignalProcessorIndex
}: Props): JSX.Element => {
  const ctrlName = `signalProcessor.${selectedSignalProcessorIndex}` as const;

  const { setValue, getValues } = useFormContext();

  const onChangeBand = (event: SelectChangeEvent<string>): void => {
    const band = event.target.value as MidBand;
    const bandDefaults = MidBandsDefaults.get(band);
    setValue(`${ctrlName}.fsOffsetMhz`, bandDefaults?.fsOffsetMhz);
    getValues('signalProcessor')[
      selectedSignalProcessorIndex
    ].spectralWindows.forEach((_, index) => {
      setValue(
        `${ctrlName}.spectralWindows.${index}.bandwidthMhz`,
        bandDefaults?.bandwidthMhz
      );
      setValue(
        `${ctrlName}.spectralWindows.${index}.centreFrequencyMhz`,
        bandDefaults?.centreMhz
      );
    });
  };

  return (
    <Grid
      key={selectedSignalProcessorIndex}
      container
      rowSpacing={4}
      columnSpacing={1}
    >
      <Grid size={{ xs: 4 }}>
        <ControlledTextField
          id="signal-processor-details-name"
          label="Name"
          name={`${ctrlName}.name`}
        />
      </Grid>
      <Grid size={{ xs: 4 }}>
        <ControlledSelect
          id="signal-processor-details-band"
          label="Band"
          name={`${ctrlName}.band`}
          options={bandOptions}
          onChange={onChangeBand}
        />
      </Grid>
      <Grid size={{ xs: 4 }}>
        <ControlledNumberField
          id="signal-processor-details-fs-offset"
          label="FS Offset (MHz)"
          name={`${ctrlName}.fsOffsetMhz`}
          showArrows
          disabled
        />
      </Grid>
      <Grid size={{ xs: 12 }}>
        <MidSpectralWindowsTable
          selectedSignalProcessorIndex={selectedSignalProcessorIndex}
        />
      </Grid>
    </Grid>
  );
};

const bandOptions: SelectOption[] = [
  { value: '1', label: '1' },
  { value: '2', label: '2' }
];
