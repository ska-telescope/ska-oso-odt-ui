import React, { useContext, useState } from 'react';
import AddIcon from '@mui/icons-material/Add';
import SettingsIcon from '@mui/icons-material/Settings';
import { Button, ButtonGroup } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  MidSignalProcessorScheme,
  LowSignalProcessorScheme
} from '../../../models/signalProcessor/signalProcessor';
import { OsoListComponent } from '../../shared/ui/list';
import { makeCspConfigurationIdAndName } from '../../../lib/id-generator';
import { LowSpectralWindowScheme } from '../../../models/signalProcessor/spectralWindow';
import { AlertDialog } from '../../shared/dialogs';
import { TelescopeContext } from '../../../pages/sbdEditor/sbdEditor';
import { TelescopeType } from '../../../generated/models/telescope-type';

const ctrlName = 'signalProcessor';

interface Props {
  selectedSignalProcessorIndex: number;
  setSelectedSignalProcessorIndex: () => void;
}

export const SignalProcessorList = ({
  selectedSignalProcessorIndex,
  setSelectedSignalProcessorIndex
}: Props): JSX.Element => {
  const { getValues, setValue } = useFormContext();
  const [deleteDialog, setDeleteDialog] = useState(false);
  const telescopeType = useContext(TelescopeContext);

  const addSignalProcessor = () => {
    const newSignalProcessor =
      telescopeType === TelescopeType.SkaMid
        ? MidSignalProcessorScheme.parse({ ...makeCspConfigurationIdAndName() })
        : LowSignalProcessorScheme.parse({
            ...makeCspConfigurationIdAndName(),
            spectralWindows: [LowSpectralWindowScheme.parse({})]
          });
    setValue(ctrlName, [...getValues(ctrlName), newSignalProcessor]);
  };

  const deleteSignalProcessor = (index: number) => {
    // TODO this needs all the pages to be moved up to the single form to work
    // const signalProcessor = getValues(ctrlName)[index];
    // if (getValues(ctrlName)[index].id in getValues('scans...')) {
    //   setDeleteDialog(true);
    // }
    setValue(ctrlName, getValues(ctrlName).toSpliced(index, 1));
  };

  const signalProcessorNames = getValues(ctrlName).map(
    (signalProcessor) => signalProcessor.name
  );

  return (
    <>
      {/*TODO can this alert be moved into the list component? Or moved somewhere else into a general
      'resource in use' handler */}
      <AlertDialog
        title="Signal Processor in use"
        content="Signal Processor is currently used in a scan. Please remove from the scan before deleting."
        open={deleteDialog}
        onClose={() => setDeleteDialog(false)}
      />
      <OsoListComponent
        title={'CSP Configs'}
        name={'configs'}
        selectedIndex={selectedSignalProcessorIndex}
        setSelectedIndex={setSelectedSignalProcessorIndex}
        names={signalProcessorNames}
        deleteItem={deleteSignalProcessor}
        Icon={SettingsIcon}
      >
        {/*TODO should this add button be contained within the list component?*/}
        <ButtonGroup
          disableElevation
          variant="contained"
          aria-label="Manage config buttons"
          sx={{ mt: '15px' }}
        >
          <Button
            aria-label="add config"
            startIcon={<AddIcon />}
            onClick={addSignalProcessor}
            variant="contained"
            color="secondary"
          >
            Add
          </Button>
        </ButtonGroup>
      </OsoListComponent>
    </>
  );
};
