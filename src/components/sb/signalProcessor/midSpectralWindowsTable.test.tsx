import React from 'react';
import { act, fireEvent, render, screen, within } from '@testing-library/react';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_MID_SIGNAL_PROCESSOR
} from 'src/components/testUtils';
import { MidSpectralWindowsTable } from './midSpectralWindowsTable';

const PAGE_BASE_NAME = 'signalProcessor.0.spectralWindows';

describe('MidSpectralWindowsTable component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <MidSpectralWindowsTable selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    const rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(2); // 1 header row + 1 SPW
    const dataCells = within(rows[1]).getAllByRole('cell');
    expect(dataCells.length).eq(7);
    expect(within(dataCells[0]).getByRole('textbox').value).eq('1');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('CORR');
    expect(within(dataCells[2]).getByRole('spinbutton').value).eq('500');
    expect(within(dataCells[3]).getByRole('spinbutton').value).eq('700.00896');
    expect(within(dataCells[4]).getByRole('textbox').value).eq('13.44 (8.06)');
    expect(within(dataCells[5]).getByRole('combobox').textContent).eq('0.284');
  });

  test('should add a new spectral window when the add button is clicked', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <MidSpectralWindowsTable selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );
    let rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(2);

    const addButton = screen.getByRole('button', {
      name: 'Add New Correlation SPW'
    });

    act(() => {
      fireEvent.click(addButton);
    });
    rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(3);
  });

  test('should delete the correct SPW row when the delete icon is clicked', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <MidSpectralWindowsTable selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    let rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(2);

    const deleteButton = screen.getByRole('button', {
      name: `${PAGE_BASE_NAME}.0.delete`
    });

    act(() => {
      fireEvent.click(deleteButton);
    });
    rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(1); // Just the header row should be left
  });
});

export const getSpectralWindowsTableRows = () => {
  const table = screen.getByRole('table');
  return within(table).getAllByRole('row');
};
