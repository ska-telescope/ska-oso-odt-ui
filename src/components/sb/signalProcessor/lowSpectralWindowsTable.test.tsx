import React from 'react';
import { act, fireEvent, render, screen, within } from '@testing-library/react';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_LOW_SIGNAL_PROCESSOR
} from 'src/components/testUtils';
import { LowSpectralWindowsTable } from './lowSpectralWindowsTable';

describe('LowSignalProcessorDetails component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_LOW_SIGNAL_PROCESSOR}
      >
        <LowSpectralWindowsTable selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    const rows = getSpectralWindowsTableRows();

    expect(rows.length).eq(3); // 1 header row + 2 SPW
    const dataCells = within(rows[1]).getAllByRole('cell');
    expect(dataCells.length).eq(7);
    expect(within(dataCells[0]).getByRole('textbox').value).eq('1');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('CORR');
    expect(within(dataCells[2]).getByRole('spinbutton').value).eq('500');
    expect(within(dataCells[3]).getByRole('spinbutton').value).eq('700.00896');
    expect(within(dataCells[4]).getByRole('textbox').value).eq('5.43 (3.25)');
    expect(within(dataCells[5]).getByRole('combobox').textContent).eq('0.283');
  });

  test('should add a new spectral window when the add button is clicked', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_LOW_SIGNAL_PROCESSOR}
      >
        <LowSpectralWindowsTable selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );
    let rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(3);

    const addButton = screen.getByRole('button', {
      name: 'Add New Correlation SPW'
    });

    act(() => {
      fireEvent.click(addButton);
    });
    rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(4);
  });

  test('should delete the correct SPW row when the delete icon is clicked', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_LOW_SIGNAL_PROCESSOR}
      >
        <LowSpectralWindowsTable selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    let rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(3);

    // Delete the first SPW
    let deleteButton = screen.getByRole('button', {
      name: `signalProcessor.0.spectralWindows.0.delete`
    });

    act(() => {
      fireEvent.click(deleteButton);
    });
    rows = getSpectralWindowsTableRows();
    expect(rows.length).eq(2);

    // Check the delete button is now disabled, as need at least one SPW for Low
    deleteButton = screen.getByRole('button', {
      name: `signalProcessor.0.spectralWindows.0.delete`
    });
    expect(deleteButton).toBeDisabled();
  });

  test('should disable adding new SPW when PST is checked', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={{
          signalProcessor: [
            {
              ...TEST_FORM_LOW_SIGNAL_PROCESSOR.signalProcessor[0],
              doPst: true
            }
          ]
        }}
      >
        <LowSpectralWindowsTable selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    const addButton = screen.getByRole('button', {
      name: 'Add New Correlation SPW'
    });

    expect(addButton).toBeDisabled();
  });
});

export const getSpectralWindowsTableRows = () => {
  const table = screen.getByRole('table', {
    name: 'signalProcessor.0.spectralWindows'
  });
  return within(table).getAllByRole('row');
};
