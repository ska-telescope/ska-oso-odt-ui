import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
  within
} from '@testing-library/react';
import { SignalProcessorList } from './signalProcessorList';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_MID_SIGNAL_PROCESSOR
} from 'src/components/testUtils';
import { vi } from 'vitest';

describe('SignalProcessorList component', () => {
  test('should display the names of the CSP configurations', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <SignalProcessorList
          selectedSignalProcessorIndex={0}
          setSelectedSignalProcessorIndex={vi.fn()}
        />
      </WithSbDefinitionFormContext>
    );

    const rows = getSignalProcessorListItems();

    expect(rows.length).eq(3);
    expect(rows.map((row) => row.textContent)).toEqual([
      'Config 123',
      'Config 456',
      'Config 789'
    ]);
  });

  test('should delete the correct row when the delete icon is clicked', async () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <SignalProcessorList
          selectedSignalProcessorIndex={0}
          setSelectedSignalProcessorIndex={vi.fn()}
        />
      </WithSbDefinitionFormContext>
    );
    const deleteButton = screen.getByRole('button', {
      name: /delete-1/i
    });
    act(() => {
      fireEvent.click(deleteButton);
    });

    await waitFor(() => {
      expect(getSignalProcessorListItems().length).eq(2);
    });

    expect(getSignalProcessorListItems().map((row) => row.textContent)).toEqual(
      ['Config 123', 'Config 789']
    );
  });

  test('should add a new CSP Config when the add button is clicked', async () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <SignalProcessorList
          selectedSignalProcessorIndex={0}
          setSelectedSignalProcessorIndex={vi.fn()}
        />
      </WithSbDefinitionFormContext>
    );

    expect(getSignalProcessorListItems().length).eq(3);
    const addButton = screen.getByRole('button', { name: /add config/i });

    act(() => {
      fireEvent.click(addButton);
    });

    expect(getSignalProcessorListItems().length).eq(4);
  });
});

export const getSignalProcessorListItems = () => {
  const list = screen.getByRole('list', { name: /configs/i });
  return within(list).getAllByText('Config', { exact: false });
};
