import Grid from '@mui/material/Grid2';
import {
  ControlledCheckbox,
  ControlledTextField
} from '../../../components/shared/controlled';
import { OsoTitleWrapper } from '../../../components/shared/ui/titleWrapper';
import { RadialMotionComponent } from './radialMotion';
import { FieldPatternComponent } from './fieldPattern';
import { CoordinateComponent } from './coordinate';
import { useFormContext } from 'react-hook-form';
import { coordinateKind } from '../../../models/targets/target';
import { ResolveCoordinatesComponent } from './resolveCoordinates';
import { useContext } from 'react';
import { TelescopeContext } from '../../../pages/sbdEditor/sbdEditor';
import { TelescopeType } from '../../../generated/models/telescope-type';

type TargetDetailsProps = {
  selectedTargetIndex: number;
};

export const TargetDetails = ({
  selectedTargetIndex
}: TargetDetailsProps): JSX.Element => {
  const ctrlName = `targets.${selectedTargetIndex}` as const;

  const telescopeType = useContext(TelescopeContext);
  const { getValues } = useFormContext();
  const coordKind = getValues(ctrlName).coordinate.kind;

  return (
    <Grid
      // 'key' here triggers re-rendering when selectedTarget changes:
      key={selectedTargetIndex}
      container
      direction="row"
      width="100%"
      alignItems={'top'}
      spacing={3}
    >
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <Grid
          container
          direction="row"
          alignItems={'center'}
          justifyContent={'center'}
          spacing={1}
        >
          <Grid size={{ xs: coordKind === coordinateKind.ICRS ? 9 : 12 }}>
            <ControlledTextField
              name={`targets.${selectedTargetIndex}.name`}
              label="Name"
            />
          </Grid>
          <Grid size={{ xs: 3 }}>
            {coordKind === coordinateKind.ICRS && (
              <ResolveCoordinatesComponent
                selectedTargetIndex={selectedTargetIndex}
              />
            )}
          </Grid>
        </Grid>
        <br />
        <OsoTitleWrapper title="Coordinate">
          <CoordinateComponent selectedTargetIndex={selectedTargetIndex} />
        </OsoTitleWrapper>
        <br />
        {telescopeType === TelescopeType.SkaLow && (
          <OsoTitleWrapper title="PST">
            <ControlledCheckbox
              name={`targets.${selectedTargetIndex}.addPstBeam`}
              label="Add 'boresight' pulsar timing beam"
              disabled={coordKind === coordinateKind.SSO}
            />
          </OsoTitleWrapper>
        )}
      </Grid>
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <OsoTitleWrapper title="Radial Motion">
          <RadialMotionComponent selectedTargetIndex={selectedTargetIndex} />
        </OsoTitleWrapper>
      </Grid>
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <OsoTitleWrapper title="Field Pattern">
          <FieldPatternComponent selectedTargetIndex={selectedTargetIndex} />
        </OsoTitleWrapper>
      </Grid>
    </Grid>
  );
};
