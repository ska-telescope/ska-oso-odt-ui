import { Stack } from '@mui/material';
import { useFormContext, useWatch } from 'react-hook-form';

import {
  ControlledNumberField,
  ControlledSelect
} from '../../../components/shared/controlled';
import {
  fieldPattern,
  fieldPatternSchema,
  FieldPatternType
} from '../../../models/targets/target';
import { createSelectOptions } from '../../shared/ui/select';
import {
  createTableDataFromFormValues,
  DisplayType,
  OsoDataTable,
  OsoDataTableColumnDefinitions
} from '../../shared/ui';
import { useContext } from 'react';
import { TelescopeContext } from '../../../pages/sbdEditor/sbdEditor';
import { TelescopeType } from '../../../generated/models/telescope-type';

const renderFivePoint = (ctrlBaseName: string): JSX.Element => (
  <ControlledNumberField
    sx={{ m: 1 }}
    name={`${ctrlBaseName}.offset`}
    label="Offset (arcsec)"
  />
);

const renderPointingCentres = (
  ctrlBaseName: string,
  formValues: FieldPatternType
): JSX.Element => {
  const ctrlName = `${ctrlBaseName}.offsets`;
  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'id',
      isIdField: true
    },
    {
      displayType: [DisplayType.TextField],
      title: 'R.A. offset (arcsec)',
      columnName: 'raOffset'
    },
    {
      displayType: [DisplayType.TextField],
      title: 'Dec. offset (arcsec)',
      columnName: 'decOffset'
    }
  ];
  const tableData = createTableDataFromFormValues(columns, formValues.offsets);
  return (
    <OsoDataTable baseName={ctrlName} data={tableData} columns={columns} />
  );
};

type FieldPatternProps = {
  selectedTargetIndex: number;
};

export const FieldPatternComponent = ({
  selectedTargetIndex
}: FieldPatternProps): JSX.Element => {
  const ctrlName = `targets.${selectedTargetIndex}.pointingPattern` as const;
  const { setValue, getValues } = useFormContext();
  const selectedFPK = useWatch({
    name: `${ctrlName}.kind`
  });
  const telescopeType = useContext(TelescopeContext);

  const setFieldPatternDefaults = (event) => {
    // Set defaults for the new selection based on the defaults
    // declared in the zod FieldPattern schema
    const newFieldPattern = event.target.value;
    setValue(ctrlName, fieldPatternSchema.parse({ kind: newFieldPattern }));
  };

  let dataFields = <></>;
  switch (selectedFPK) {
    case fieldPattern.FIVEPOINT:
      dataFields = renderFivePoint(ctrlName);
      break;
    case fieldPattern.POINTINGCENTRES:
      dataFields = renderPointingCentres(ctrlName, getValues(ctrlName));
      break;
    default:
      console.error(`Error: unknown kind '${selectedFPK}'.`);
  }

  return (
    <Stack>
      {telescopeType === TelescopeType.SkaMid && (
        <ControlledSelect
          sx={{ m: 1 }}
          name={`${ctrlName}.kind`}
          options={createSelectOptions(fieldPattern)}
          onChange={setFieldPatternDefaults}
          label="Field Pattern Type"
        />
      )}
      {dataFields}
    </Stack>
  );
};
