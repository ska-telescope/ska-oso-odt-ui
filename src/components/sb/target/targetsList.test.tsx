import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
  within
} from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { TargetsList } from './targetsList';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_TARGETS
} from 'src/components/testUtils';

describe('TargetsList component', () => {
  test('should display the names of the targets', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
        <TargetsList selectedTargetIndex={0} setSelectedTargetIndex={vi.fn()} />
      </WithSbDefinitionFormContext>
    );

    const rows = getTargetsListItems();

    expect(rows.length).eq(3);
    expect(rows.map((row) => row.textContent)).toEqual([
      'Target 123',
      'Target 456',
      'Target 789'
    ]);
  });

  test('should delete the correct row when the delete icon is clicked', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
        <TargetsList selectedTargetIndex={0} setSelectedTargetIndex={vi.fn()} />
      </WithSbDefinitionFormContext>
    );
    const deleteButton = screen.getByRole('button', {
      name: /delete-1/i
    });
    act(() => {
      fireEvent.click(deleteButton);
    });

    await waitFor(() => {
      expect(getTargetsListItems().length).eq(2);
    });

    expect(getTargetsListItems().map((row) => row.textContent)).toEqual([
      'Target 123',
      'Target 789'
    ]);
  });

  test('should add a new Target when the add button is clicked', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
        <TargetsList selectedTargetIndex={0} setSelectedTargetIndex={vi.fn()} />
      </WithSbDefinitionFormContext>
    );

    expect(getTargetsListItems().length).eq(3);
    const addButton = screen.getByRole('button', { name: /add target/i });

    act(() => {
      fireEvent.click(addButton);
    });

    expect(getTargetsListItems().length).eq(4);
  });
});

// TODO add a ListItem to the list component so can select by listitem here
export const getTargetsListItems = () => {
  const list = screen.getByRole('list', { name: /targets-list/i });
  return within(list).getAllByText('Target', { exact: false });
};
