import { useFormContext } from 'react-hook-form';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import apiService from '../../../services/apiService';
import { AlertDialog } from '../../shared/dialogs';
import React, { useState } from 'react';
import {
  radialMotionKind,
  referenceFrame,
  velocityDefinition
} from '../../../models/targets/target';

type ResolveCoordinatesProps = {
  selectedTargetIndex: number;
};

export const ResolveCoordinatesComponent = ({
  selectedTargetIndex
}: ResolveCoordinatesProps): JSX.Element => {
  const { setValue, getValues } = useFormContext();
  const ctrlName = `targets.${selectedTargetIndex}` as const;
  const [apiErrorMsg, setAPIErrorMsg] = useState('');

  const setTargetValues = async () => {
    const targetName = getValues(ctrlName).name;

    const resolveResponse = await apiService.getSystemCoordinates(targetName);
    if (resolveResponse?.data) {
      if (resolveResponse.data.equatorial) {
        setValue(
          `${ctrlName}.coordinate.ra`,
          resolveResponse.data.equatorial.ra
        );
        setValue(
          `${ctrlName}.coordinate.dec`,
          resolveResponse.data.equatorial.dec
        );

        // Set the velocity values if non-zero velocity is present in the response or if
        // both velocity and redshift are returned as 0 and velocity is currently selected
        if (
          resolveResponse.data.equatorial.velocity ||
          (!resolveResponse.data.equatorial.velocity &&
            !resolveResponse.data.equatorial.redshift &&
            getValues(ctrlName).radialMotion.kind === radialMotionKind.VELOCITY)
        ) {
          setValue(`${ctrlName}.radialMotion.kind`, radialMotionKind.VELOCITY);
          setValue(
            `${ctrlName}.radialMotion.velocity`,
            resolveResponse.data.equatorial.velocity
          );
          setValue(
            `${ctrlName}.radialMotion.referenceFrame`,
            referenceFrame.BARY
          );
          setValue(
            `${ctrlName}.radialMotion.velocityDefinition`,
            velocityDefinition.OPTICAL
          );
        } else {
          setValue(`${ctrlName}.radialMotion.kind`, radialMotionKind.REDSHIFT);
          setValue(
            `${ctrlName}.radialMotion.redshift`,
            resolveResponse.data.equatorial.redshift
          );
        }
      }
    } else {
      setAPIErrorMsg(
        resolveResponse.error ?? 'Unknown error while resolving target'
      );
    }
  };

  return (
    <>
      <AlertDialog
        title="Failed to resolve target"
        content={apiErrorMsg}
        open={!!apiErrorMsg}
        onClose={() => setAPIErrorMsg('')}
      />
      <Button
        onClick={setTargetValues}
        color={ButtonColorTypes.Inherit}
        variant={ButtonVariantTypes.Contained}
        label="Resolve"
        testId="button-test-resolve"
      />
    </>
  );
};
