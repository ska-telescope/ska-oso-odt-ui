import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { TargetDetails } from './targetDetails';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_TARGETS
} from 'src/components/testUtils';
import { TelescopeType } from '../../../generated/models/telescope-type';

describe('TargetDetailsComponent', () => {
  test('mid should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext
        telescopeType={TelescopeType.SkaMid}
        initialState={TEST_FORM_TARGETS}
      >
        <TargetDetails selectedTargetIndex={0} />
      </WithSbDefinitionFormContext>
    );

    const nameInput = screen.getByLabelText('Name');
    expect(nameInput).toHaveValue('Target 123');

    const coordinateGroup = screen.getByRole('group', {
      name: 'Coordinate'
    });
    expect(coordinateGroup).toBeVisible();

    const radialMotionGroup = screen.getByRole('group', {
      name: 'Radial Motion'
    });
    expect(radialMotionGroup).toBeVisible();

    const fieldPatternGroup = screen.getByRole('group', {
      name: 'Field Pattern'
    });
    expect(fieldPatternGroup).toBeVisible();

    // PST option should only be present in Low telescope
    const pstGroup = screen.queryByRole('group', {
      name: 'PST'
    });
    expect(pstGroup).toBeNull();
  });

  test('low should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext
        telescopeType={TelescopeType.SkaLow}
        initialState={TEST_FORM_TARGETS}
      >
        <TargetDetails selectedTargetIndex={0} />
      </WithSbDefinitionFormContext>
    );

    const nameInput = screen.getByLabelText('Name');
    expect(nameInput).toHaveValue('Target 123');

    const coordinateGroup = screen.getByRole('group', {
      name: 'Coordinate'
    });
    expect(coordinateGroup).toBeVisible();

    const radialMotionGroup = screen.getByRole('group', {
      name: 'Radial Motion'
    });
    expect(radialMotionGroup).toBeVisible();

    const fieldPatternGroup = screen.getByRole('group', {
      name: 'Field Pattern'
    });
    expect(fieldPatternGroup).toBeVisible();

    const pstGroup = screen.getByRole('group', {
      name: 'PST'
    });
    expect(pstGroup).toBeVisible();
  });
});
