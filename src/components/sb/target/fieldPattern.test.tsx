import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { FieldPatternComponent } from './fieldPattern';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_TARGETS
} from 'src/components/testUtils';
import { TelescopeType } from '../../../generated/models/telescope-type';

describe('FieldPatternComponent', () => {
  test('mid should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext
        telescopeType={TelescopeType.SkaMid}
        initialState={TEST_FORM_TARGETS}
      >
        <FieldPatternComponent selectedTargetIndex={0} />
      </WithSbDefinitionFormContext>
    );
    const combobox = screen.getByRole('combobox');
    expect(combobox).toBeInTheDocument();
    expect(combobox).toBeVisible();
  });

  test('low should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext
        telescopeType={TelescopeType.SkaLow}
        initialState={TEST_FORM_TARGETS}
      >
        <FieldPatternComponent selectedTargetIndex={0} />
      </WithSbDefinitionFormContext>
    );
    // The dropdown for Field Pattern type should not be present in Low as
    // there is only one option there
    const combobox = screen.queryByRole('combobox');
    expect(combobox).toBeNull();
  });
});
