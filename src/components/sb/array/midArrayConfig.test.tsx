import { act, fireEvent, render, screen, within } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { MidArrayComponent } from './midArrayConfig';
import { DefaultMidArrayAA05 } from '../../../models/array/arrayConfig';
import {
  TEST_FORM_MID_ARRAY,
  WithSbDefinitionFormContext
} from '../../testUtils';
import React from 'react';

describe('MidArrayComponent', () => {
  const checkNumberOfStations = (count: number) => {
    expect(
      screen.getByRole('textbox', { name: 'Number of Dishes' })
    ).toHaveValue(count.toString());
  };

  test('should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_MID_ARRAY}>
        <MidArrayComponent />
      </WithSbDefinitionFormContext>
    );

    const configuration = screen.getByLabelText('Configuration');
    expect(configuration).toBeInTheDocument();
    expect(configuration).toBeVisible();

    checkNumberOfStations(4);

    const dishesGroup = screen.getByRole('group', { name: 'Dishes' });
    expect(dishesGroup).toBeInTheDocument();
    const dishes = within(dishesGroup).getByRole('textbox');
    expect(dishes).toHaveAttribute('value', DefaultMidArrayAA05.join(', '));
  });

  // TODO: Define a standard for how to get different components. Should we use testId?
  //  The decided field should be set consistently on all the common components and used
  //  to get components in these tests.

  test('with user action should return return appropriate components', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_MID_ARRAY}>
        <MidArrayComponent />
      </WithSbDefinitionFormContext>
    );
    const dropdown = screen.getByDisplayValue(/aa0\.5/i);

    // Check user event on changing config type to 'Custom'
    const customConfigType = 'Custom';
    act(() => {
      fireEvent.click(dropdown);
    });
    act(() => {
      fireEvent.change(dropdown, { target: { value: customConfigType } });
    });
    const combobox = screen.getByRole('combobox');
    expect(combobox.innerText).eq(customConfigType);

    const dishesGroup = screen.getByRole('group', { name: 'Dishes' });
    expect(dishesGroup).toBeInTheDocument();
    const dishesInput = within(dishesGroup).getByRole('textbox');
    expect(dishesInput).toHaveAttribute(
      'value',
      DefaultMidArrayAA05.join(', ')
    );
    checkNumberOfStations(4);

    // // simulate valid dishes 'SKA013, SKA102, MKT002' list being entered by user
    const newDishesValue = 'SKA013, SKA102, MKT002';
    act(() => {
      fireEvent.change(dishesInput, { target: { value: newDishesValue } });
    });
    expect(dishesInput).toHaveAttribute('value', newDishesValue);
  });
});
