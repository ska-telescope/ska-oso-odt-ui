import React, { useState } from 'react';
import { Card, CardContent, type SelectChangeEvent } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { Controller, useFormContext } from 'react-hook-form';
import { OsoInput } from '../../../components/shared/ui/input';
import {
  MidArrayConfigTypeOptions,
  MidArrayConfigTypeEnum,
  DefaultMidArrayAA05
} from '../../../models/array/arrayConfig';
import { ControlledSelect } from '../../shared/controlled';
import { OSOStaticInput } from '../../shared/ui/staticInput';
import { OsoTitleWrapper } from '../../shared/ui/titleWrapper';

const ctrlName = 'array' as const;

export const MidArrayComponent = (): JSX.Element => {
  const { control, trigger, setValue, getValues } = useFormContext();

  const arrayConfigValue = getValues(ctrlName);

  // Store array type separately instead of always displaying the stored label so that
  // user Custom selection is temporarily stored even if dish IDs match an existing array
  const [arrayLabel, setArrayLabel] = useState<string>(
    arrayConfigValue.configuration
  );

  const subarrayOnChangeHandle = (e: SelectChangeEvent<string>): void => {
    const newArray = e.target.value as keyof typeof MidArrayConfigTypeEnum;
    // Currently Mid only supports two array types, Custom and AA0.5
    // If type is not custom, set dishes to AA0.5 list (expand the logic
    // here in the future to allow different array types)
    setValue(ctrlName, {
      dishes:
        newArray !== MidArrayConfigTypeEnum.Custom
          ? DefaultMidArrayAA05.join(', ')
          : arrayConfigValue.dishes,
      configuration: newArray
    });
    setArrayLabel(newArray);
  };

  const inputDishList = (): JSX.Element => {
    const disabledInput: boolean = arrayLabel !== MidArrayConfigTypeEnum.Custom;
    return (
      <Controller
        name={`${ctrlName}.dishes`}
        control={control}
        defaultValue={arrayConfigValue.dishes}
        render={({ field: { onChange, value }, fieldState: { error } }) => {
          const dishesListOnChangeHandle = async (
            e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) => {
            onChange(e);
            const fieldIsValid = await trigger([`${ctrlName}.dishes`]);
            if (fieldIsValid) {
              const newDishList: string = e.target.value;
              setValue(ctrlName, {
                ...arrayConfigValue,
                dishes: newDishList
              });
            }
          };
          return (
            <OsoInput
              helperText={error ? error.message : null}
              error={!!error}
              onChange={dishesListOnChangeHandle}
              testId="dishes"
              defaultValue={disabledInput ? arrayConfigValue.dishes : value}
              title="Dishes"
              name={`${ctrlName}.dishes`}
              disabled={disabledInput}
            />
          );
        }}
      />
    );
  };

  return (
    <Card data-testid="sbMidArrayConfig" variant="outlined">
      <CardContent sx={{ width: '100%' }}>
        <OsoTitleWrapper title={'Configuration'}>
          <Grid container direction="row" width="100%" alignItems={'center'}>
            <Grid size={{ xs: 3 }} paddingRight="10px">
              <ControlledSelect
                name={`${ctrlName}.configuration`}
                label="Configuration"
                options={MidArrayConfigTypeOptions}
                onChange={subarrayOnChangeHandle}
              />
            </Grid>
            <Grid size={{ xs: 7 }} paddingLeft="10px">
              {inputDishList()}
            </Grid>
            <Grid size={{ xs: 2 }} paddingLeft="10px">
              <OSOStaticInput
                label={'Number of Dishes'}
                name={`${ctrlName}.numberOfDishes`}
                value={
                  arrayConfigValue.dishes
                    ? arrayConfigValue.dishes.split(',').length
                    : 0
                }
              />
            </Grid>
          </Grid>
        </OsoTitleWrapper>
      </CardContent>
    </Card>
  );
};
