import { Card, CardContent } from '@mui/material';
import Grid from '@mui/material/Grid2';
import React, { useState } from 'react';
import HubOutlinedIcon from '@mui/icons-material/HubOutlined';
import { OsoTitleWrapper } from '../../shared/ui/titleWrapper';
import { useFormContext } from 'react-hook-form';
import {
  DisplayType,
  OsoDataTable,
  OsoDataTableColumnDefinitions
} from '../../shared/ui';
import { OsoListComponent } from '../../shared/ui/list';
import { createTableDataFromFormValues } from '../../shared/ui/dataTable';

type ApertureTableProps = {
  selectedStation: string;
};

const ApertureTable = ({
  selectedStation
}: ApertureTableProps): JSX.Element => {
  const ctrlName = `array.subarrayBeam.apertures.${selectedStation}`;
  const { getValues } = useFormContext();
  const stationApertures = getValues(ctrlName);
  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.StaticField],
      title: 'Substation ID',
      columnName: 'substationID',
      isIdField: true
    },
    {
      displayType: [DisplayType.TextField],
      title: 'Weighting Key',
      columnName: 'weightingKey',
      id: 'weightingKey'
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      columnName: 'delete',
      description: 'Click to delete this spectral window entry',
      disabled: true
    }
  ];

  return (
    <OsoDataTable
      baseName={ctrlName}
      data={createTableDataFromFormValues(columns, stationApertures)}
      columns={columns}
    />
  );
};

export const SubarrayBeamComponent = () => {
  const ctrlName = 'array.subarrayBeam';
  const { getValues } = useFormContext();
  const subarrayBeam = getValues(ctrlName);
  const [selectedStation, setSelectedStation] = useState<number>(0);
  if (Object.keys(subarrayBeam.apertures).length <= selectedStation) {
    setSelectedStation(0);
  }

  return (
    <Card data-testid="sbSubarratBeamConfig" variant="outlined">
      <CardContent>
        <OsoTitleWrapper title={subarrayBeam.subarrayBeamID}>
          <Grid container direction="row" width="100%" padding="5px">
            <Grid size={{ xs: 3 }}>
              <OsoListComponent
                title="Stations"
                name="stations-table"
                selectedIndex={selectedStation}
                setSelectedIndex={setSelectedStation}
                names={Object.keys(subarrayBeam.apertures)}
                Icon={HubOutlinedIcon}
                withDelete={false}
                disabledDelete={true}
              />
            </Grid>
            <Grid size={{ xs: 9 }} padding="10px">
              <ApertureTable
                selectedStation={
                  Object.keys(subarrayBeam.apertures)[selectedStation]
                }
              />
            </Grid>
          </Grid>
        </OsoTitleWrapper>
      </CardContent>
    </Card>
  );
};
