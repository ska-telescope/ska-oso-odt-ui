import React, { useState } from 'react';
import { OsoTitleWrapper } from '../../../components/shared/ui/titleWrapper';
import { useFormContext } from 'react-hook-form';

import { OperationsButtonGroup } from '../../shared/operationButtonGroup';
import { AlertDialog } from '../../shared/dialogs';
import {
  OsoDataTable,
  DisplayType,
  OsoDataTableColumnDefinitions,
  createTableDataFromFormValues
} from '../../shared/ui';
import { ScanDefinitionScheme } from '../../../models/scans/scanDefinitions';
import { ScanSequenceItemScheme } from '../../../models/scans/scanSequence';
import { makeScanDefinitionId } from '../../../lib/id-generator';

const ctrlName = 'scans.scanDefinitions';
const scanSequenceCtrlName = 'scans.scanSequence';

export const ScanDefinitions = (): JSX.Element => {
  const { setValue, getValues } = useFormContext();
  const [deleteDialog, setDeleteDialog] = useState(false);

  const cspOptions = (getValues('signalProcessor') ?? []).map(
    (signalProcessor) => ({
      value: signalProcessor.id,
      label: signalProcessor.name
    })
  );

  const targetOptions = (getValues('targets') ?? []).map((target) => ({
    value: target.id,
    label: target.name
  }));

  const addScanDefinition = () => {
    const scanDefinitionId = makeScanDefinitionId();
    const newScanDefinition = ScanDefinitionScheme.parse({
      scanDefinitionId,
      durationS: 1
    });
    setValue(ctrlName, [...(getValues(ctrlName) ?? []), newScanDefinition]);
  };

  const deleteScanDefinition = (index: number) => {
    const scanDefinitionId = getValues(ctrlName)[index].scanDefinitionId;
    if (
      getValues(scanSequenceCtrlName).some(
        (scanSequenceItem) =>
          scanSequenceItem.scanDefinitionId === scanDefinitionId
      )
    ) {
      setDeleteDialog(true);
    } else {
      setValue(ctrlName, getValues(ctrlName).toSpliced(index, 1));
    }
  };

  const addScanDefinitionToScanSequence = (scanDefinitionIndex: number) => {
    const scanDefinition = getValues(ctrlName)[scanDefinitionIndex];
    const scanDefinitionId = scanDefinition.scanDefinitionId;
    const target = targetOptions.find(
      (target) => target.value === scanDefinition.target
    ).label;
    const newScanSequence = ScanSequenceItemScheme.parse({
      scanDefinitionId,
      target
    });
    setValue(scanSequenceCtrlName, [
      ...(getValues(scanSequenceCtrlName) ?? []),
      newScanSequence
    ]);
  };

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'scanDefinitionId',
      isIdField: true
    },
    {
      displayType: [DisplayType.SelectField],
      title: 'Target',
      columnName: 'target',
      options: targetOptions
    },
    {
      displayType: [DisplayType.SelectField],
      title: 'CSP',
      columnName: 'csp',
      options: cspOptions
    },
    {
      displayType: [DisplayType.NumberField],
      title: 'Duration (s)',
      columnName: 'durationS'
    },
    {
      displayType: [DisplayType.AddButton],
      title: '',
      columnName: 'add',
      description: 'Click to add this scan definition to the scan sequence',
      onClick: addScanDefinitionToScanSequence
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      columnName: 'delete',
      description: 'Click to delete this scan definition',
      onClick: deleteScanDefinition
    }
  ];

  const tableContent = createTableDataFromFormValues(
    columns,
    getValues(ctrlName)
  );

  return (
    <>
      <AlertDialog
        title="Scan Definition in use"
        content="Scan definition is currently used in the scan sequence. Please remove from the sequence before deleting the definition."
        open={deleteDialog}
        onClose={() => setDeleteDialog(false)}
      />
      <OsoTitleWrapper title={'Scan Definitions'}>
        <OsoDataTable
          baseName={ctrlName}
          data={tableContent}
          columns={columns}
        />
        <OperationsButtonGroup
          title="Scan Definition"
          count={1}
          handleAdd={addScanDefinition}
        />
      </OsoTitleWrapper>
    </>
  );
};
