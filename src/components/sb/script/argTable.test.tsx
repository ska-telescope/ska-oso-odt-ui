import React from 'react';
import { render, screen, within } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import userEvent from '@testing-library/user-event';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_SCRIPT,
  getTableRows
} from 'src/components/testUtils';
import { TelescopeType } from '../../../generated/models/telescope-type';
import { ArgTable } from './argTable';

// Script component should work exactly the same way for both Mid and Low SBs
describe.each([TelescopeType.SkaLow, TelescopeType.SkaMid])(
  'Script',
  (telescope: TelescopeType) => {
    test.each([
      ['Init Stage Arguments', 'initArgs'],
      ['Run Stage Arguments', 'mainArgs']
    ])(
      `${telescope}: Arguments can be added and removed from %s table`,
      async (argTableLabel: string, schemaKey: string) => {
        render(
          <WithSbDefinitionFormContext
            telescopeType={telescope}
            initialState={TEST_FORM_SCRIPT}
          >
            <ArgTable argName={schemaKey} title={argTableLabel} />
          </WithSbDefinitionFormContext>
        );

        let argTableRows = getTableRows(argTableLabel);
        // Check that table has been populated with the correct number of arguments
        expect(argTableRows).toHaveLength(
          TEST_FORM_SCRIPT.script[schemaKey].length
        );
        let firstRowCells = within(argTableRows[0]).getAllByRole('cell');

        expect(within(firstRowCells[0]).getByRole('textbox')).toHaveValue(
          TEST_FORM_SCRIPT.script[schemaKey][0].kw
        );
        expect(within(firstRowCells[1]).getByRole('textbox')).toHaveValue(
          TEST_FORM_SCRIPT.script[schemaKey][0].value
        );
        const deleteButton = screen.getByRole('button', {
          name: `script.${schemaKey}.0.delete`
        });

        await userEvent.click(deleteButton);

        argTableRows = getTableRows(argTableLabel);
        firstRowCells = within(argTableRows[0]).getAllByRole('cell');
        // Check that the number of rows has reduced by one
        expect(argTableRows).toHaveLength(
          TEST_FORM_SCRIPT.script[schemaKey].length - 1
        );

        // Check that the first item is no longer there and first row now contains the second argument
        expect(within(firstRowCells[0]).getByRole('textbox')).toHaveValue(
          TEST_FORM_SCRIPT.script[schemaKey][1].kw
        );
        expect(within(firstRowCells[1]).getByRole('textbox')).toHaveValue(
          TEST_FORM_SCRIPT.script[schemaKey][1].value
        );
      }
    );
  }
);
