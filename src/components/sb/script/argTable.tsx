import {
  createTableDataFromFormValues,
  DisplayType,
  OsoDataTable,
  OsoDataTableColumnDefinitions,
  OsoTitleWrapper
} from '../../shared/ui';
import { Button, ButtonGroup } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import React from 'react';
import { useFieldArray, useFormContext } from 'react-hook-form';

interface Props {
  argName: string;
  title: string;
}

export const ArgTable = ({ argName, title }: Props): JSX.Element => {
  const ctrlName = `script.${argName}`;
  const { control } = useFormContext();
  const { fields, remove, append } = useFieldArray({
    name: ctrlName,
    control
  });

  const deleteFn = (index: number) => remove(index);

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'id',
      isIdField: true
    },
    {
      displayType: [DisplayType.TextField],
      title: 'Keyword',
      columnName: 'kw'
    },
    {
      displayType: [DisplayType.TextField],
      title: 'Argument',
      columnName: 'value'
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      columnName: 'delete',
      description: 'Click to delete this main argument record',
      onClick: deleteFn
    }
  ];

  const tableData = createTableDataFromFormValues(columns, fields);

  return (
    <OsoTitleWrapper title={title}>
      <OsoDataTable baseName={ctrlName} data={tableData} columns={columns} />
      <ButtonGroup disableElevation variant="contained" sx={{ mt: '15px' }}>
        <Button
          startIcon={<AddIcon />}
          onClick={() => append({ kw: '', value: '' })}
          variant="contained"
          color="secondary"
        >
          Add
        </Button>
      </ButtonGroup>
    </OsoTitleWrapper>
  );
};
