import { Stack } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  ControlledSelect,
  ControlledTextField,
  ControlledRadioButtonGroup
} from '../../shared/controlled';
import { ScriptKindOptions, Kind } from '../../../models/activity';

export const ScriptDetails = (): JSX.Element => {
  const ctrlName = 'script' as const;

  const { getValues } = useFormContext();

  const branchCommitOptions = [
    { label: 'Branch', value: 'branch' },
    { label: 'Commit', value: 'commit' }
  ];

  return (
    <Stack spacing={2}>
      <ControlledSelect
        name={`${ctrlName}.kind`}
        label="Kind"
        options={ScriptKindOptions}
      />
      <ControlledTextField name={`${ctrlName}.path`} label="Path" />
      {getValues(ctrlName).kind === Kind.git && (
        <>
          <ControlledTextField name={`${ctrlName}.repo`} label="Repo" />
          <ControlledRadioButtonGroup
            name={`${ctrlName}.selected`}
            options={branchCommitOptions}
            horizontal={true}
          />
          {getValues(ctrlName).selected == 'branch' ? (
            <ControlledTextField name={`${ctrlName}.branch`} label="Branch" />
          ) : (
            <ControlledTextField name={`${ctrlName}.commit`} label="Commit" />
          )}
        </>
      )}
    </Stack>
  );
};
