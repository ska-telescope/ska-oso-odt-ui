import { Box } from '@mui/material';
import { Progress } from '@ska-telescope/ska-gui-components';

export const Loader = (): JSX.Element => (
  <Box>
    <Progress size={100} testId="progressTestId" />
  </Box>
);
