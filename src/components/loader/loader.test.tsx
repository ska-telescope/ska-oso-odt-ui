import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { Loader } from './loader';

describe('Loader', () => {
  test('should return progress bar and svg icon', () => {
    const { container } = render(<Loader />);

    const progress = screen.getByLabelText('Progress');
    expect(progress).toBeInTheDocument();
    expect(progress).toBeVisible();
    expect(progress).toHaveAttribute('role', 'progressbar');
    expect(progress).toHaveAttribute(
      'aria-describedby',
      'Indicates the progress of an activity'
    );
    expect(progress).toHaveAttribute('data-testid', 'progressTestId');

    const svg = container.querySelector('div > div > div > span > svg');
    expect(svg).toBeInTheDocument();
    expect(svg).toBeVisible();
    expect(svg).toHaveAttribute('viewBox', '22 22 44 44');
  });
});
