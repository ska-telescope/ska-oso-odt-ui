import { describe, expect, test } from 'vitest';

import appConfigLoadService from './configurationService';

describe('appConfigLoadService.loadAppConfiguration', () => {
  test('should return predefined config setup', () => {
    const config = appConfigLoadService.loadAppConfiguration();
    expect(config).not.toBeNull();
    expect(config?.appTitle).toStrictEqual([
      { name: 'Observation Design Tool' }
    ]);
    expect(config?.componentNames.length).toBe(11);
    expect(config?.currentVersion).toBe('0.2.0');
    expect(config?.documentationURL).toBe(
      'https://developer.skao.int/projects/ska-oso-odt-ui/en/latest/'
    );
    expect(config?.procedureTypes).toStrictEqual([
      'filesystem',
      'inline',
      'git'
    ]);
    expect(config?.receiverBands).toStrictEqual([
      { id: '1', name: 'BAND_1' },
      { id: '2', name: 'BAND_2' },
      { id: '5a', name: 'BAND_5A' },
      { id: '5b', name: 'BAND_5B' }
    ]);
    expect(config?.skaoHomepage).toBe('https://www.skatelescope.org/');
    expect(config?.workflowKind).toStrictEqual(['batch', 'realtime']);
  });
});
