import * as appConfiguration from '../assets/configuration/configuration.json';
import { AppConfig, AppConfigSchema } from '../models';

let saveAppConfiguration: AppConfig | null = null;

const loadAppConfig = () => {
  if (saveAppConfiguration) {
    return saveAppConfiguration;
  }
  const parsedAppConfig = AppConfigSchema.safeParse(appConfiguration);
  if (!parsedAppConfig.success) {
    return null;
  }
  saveAppConfiguration = appConfiguration as AppConfig;
  return saveAppConfiguration;
};

const appConfigLoadService = {
  loadAppConfiguration: () => loadAppConfig()
};

export default appConfigLoadService;
