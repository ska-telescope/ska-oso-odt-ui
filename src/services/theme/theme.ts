import { createTheme, ThemeOptions } from '@mui/material';
import { Theme } from '@ska-telescope/ska-gui-components';

const theme = (mode: string) => createTheme(Theme(mode) as ThemeOptions);
export default theme;
