# KUBE_HOST defines the IP address of the Minikube ingress.
KUBE_HOST ?= http://`minikube ip`
# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-oso-odt-ui
K8S_CHART ?= ska-oso-odt-ui-umbrella
RELEASE_NAME ?= test

JS_E2E_TEST_BASE_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/odt/
JS_E2E_COVERAGE_COMMAND_ENABLED = false

JS_ESLINT_CONFIG ?= eslint.config.js

JS_COMMAND_RUNNER ?= yarn
JS_TEST_COMMAND ?= vitest
JS_TEST_DEFAULT_SWITCHES = run --coverage.enabled=true --reporter=junit --reporter=default --coverage.reportsDirectory=$(JS_BUILD_REPORTS_DIRECTORY) --outputFile=$(JS_BUILD_REPORTS_DIRECTORY)/unit-tests.xml

POSTGRES_HOST ?= $(RELEASE_NAME)-postgresql
K8S_CHART_PARAMS += \
  --set global.ingress.host=$(KUBE_HOST) \
  --set ska-db-oda-umbrella.pgadmin4.serverDefinitions.servers.firstServer.Host=$(POSTGRES_HOST)

# include core makefile targets for release management
-include .make/base.mk
-include .make/oci.mk
-include .make/helm.mk
-include .make/k8s.mk
-include .make/js.mk

# For the test, dev and integration environment, use the freshly built image in the GitLab registry
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep 'test|dev|integration')
ifneq ($(ENV_CHECK),)
K8S_CHART_PARAMS += --set ska-oso-odt-ui.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-oso-odt-ui.image.registry=$(CI_REGISTRY)/ska-telescope/oso/ska-oso-odt-ui
endif

# Set cluster_domain to minikube default (cluster.local) in local development
# (CI_ENVIRONMENT_SLUG should only be defined when running on the CI/CD pipeline)
ifeq ($(CI_ENVIRONMENT_SLUG),)
K8S_CHART_PARAMS += --set global.cluster_domain="cluster.local"

K8S_CHART_PARAMS += --set ska-oso-services-umbrella.ska-db-oda-umbrella.vault.enabled=false
endif

js-pre-lint:
	$(JS_COMMAND_RUNNER) prettier
typecheck:
	$(JS_COMMAND_RUNNER) typecheck

set-dev-env-vars:
	BASE_URL="/" BACKEND_URL=$(KUBE_HOST)/$(KUBE_NAMESPACE) ENVJS_FILE=./public/env.js ./scripts/write_env_js.sh

set-absolute-paths:
	BASE_URL="/" INDEX_FILE=./dist/index.html ./scripts/set_absolute_paths.sh

# The generator creates supporting files, some of which we do not want. To achieve this, it is simplest to whitelist the ones we do want
CODEGEN_TARGETS = \
	models        \
	apis,supportingFiles=api.ts	\
	supportingFiles=base.ts   \
	supportingFiles=common.ts   \
	supportingFiles=configuration.ts   \
	supportingFiles=index.ts           \

models:
	rm -fr src/generated
	#$(OCI_BUILDER) run --rm -v $(MINIKUBE_NFS_SHARES_ROOT)$(PWD):/spec redocly/cli bundle local/odt-openapi-v1.yaml > local/combined.yaml
	$(foreach var,$(CODEGEN_TARGETS),$(OCI_BUILDER) run --rm --user `id -u $$USER`:`id -g $$USER` --volume "$(MINIKUBE_NFS_SHARES_ROOT)$(PWD):/spec" openapitools/openapi-generator-cli:v7.4.0 \
  	generate \
    -i /spec/local/odtopenapi.yaml \
    -g typescript-axios \
    -o /spec/src/generated \
  	--additional-properties "withSeparateModelsAndApi=true,apiPackage=api,modelPackage=models" \
  	--global-property "$(var)" \
  	--inline-schema-options REFACTOR_ALLOF_INLINE_SCHEMAS=true \
  	--model-name-mappings "PythonArguments_args_inner=PythonArgs" \
  	--model-name-mappings "PointingPattern_parameters_inner=PointingPatternParameters" \
  	--model-name-mappings "SBDefinition_activities_value=Activity" \
  	;) \
  	rm -r src/generated/.openapi-generator
# The --inline-schema-options fixes this bug: https://github.com/OpenAPITools/openapi-generator/issues/16150
# with a fix suggested here: https://github.com/OpenAPITools/openapi-generator/issues/16831
