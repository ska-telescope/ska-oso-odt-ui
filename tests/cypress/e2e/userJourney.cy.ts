import Url from '../support/url';
import Mui from '../support/mui';

context('ODT-UI', () => {
  beforeEach(() => {
    Url.openPage('/');
  });

  it('Header : Verify external link to skao site', () => {
    Mui.fieldVisible('[data-testid="skaoLogo"]');
    cy.get('[data-testid="skaoLogo"]').click({ multiple: true, force: true });
  });

  it('Header : Has the right title', () => {
    Mui.visibleWithText(
      '[data-testid="headerTitleId"]',
      'OBSERVATION DESIGN TOOL'
    );
  });

  it('Header : Verify external link to skao odt document', () => {
    Mui.click('button[aria-label="document icon"]');
  });

  it('Header : Verify light/dark mode is available', () => {
    Mui.click('[data-testid="Brightness7Icon"]');
    Mui.click('[data-testid="Brightness4Icon"]');
    Mui.fieldVisible('[data-testid="Brightness7Icon"]');
  });

  it('Footer : Verify version', () => {
    Mui.visibleWithText('[data-testid="footerId"]', '2.0.0');
  });

  it('Footer : Verify copyright text', () => {
    Mui.visibleWithText('[data-testid="copyrightLinkTestId"]', 'SKAO');
  });
});
