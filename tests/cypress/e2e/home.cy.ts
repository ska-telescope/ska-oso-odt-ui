import Url from '../support/url';

context('Navigate to Landing Page', () => {
  beforeEach(() => {
    Url.openPage('/');
  });

  it('should have appropriate components', () => {
    cy.contains('Create a new Project');
  });

  it('select telescope ska_mid option should redirect to /sbd/general page', () => {
    cy.contains('CLICK HERE').click();
    Url.shouldContain('/project');
  });
});
