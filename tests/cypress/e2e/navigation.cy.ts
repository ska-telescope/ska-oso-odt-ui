import Mui from '../support/mui';
import Url from '../support/url';

const ODT_DOCUMENT_URL =
  'https://developer.skao.int/projects/ska-oso-odt-ui/en/latest/';
const SKAO_HOME_URL = 'https://www.skao.int/';

context('Top menu button navigation', () => {
  const skaHomeNavigationTests = [
    { url: '/', id: '[data-testid="skaoLogo"]', redirect: SKAO_HOME_URL },
    {
      url: '/importsb',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/general',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/scripts',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/array',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/targets',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/scans',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/general',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/scripts',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/array',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/targets',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    },
    {
      url: '/sbd/scans',
      id: '[data-testid="skaoLogo"]',
      redirect: SKAO_HOME_URL
    }
  ];

  it('Click SKAO Logo should redirect to the SKAO site', (done) => {
    skaHomeNavigationTests.forEach((item) => {
      Url.openPage(item.url);
      Url.loadingCompleted(done);
      Mui.click(item.id);
      Url.pathChanged(item.redirect);
    });
  });

  const homeNavigateTests = [
    { url: '/', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/importsb', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/general', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/scripts', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/array', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/targets', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/scans', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/general', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/scripts', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/array', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/targets', id: '[id="homeNavId"]', redirect: '/' },
    { url: '/sbd/scans', id: '[id="homeNavId"]', redirect: '/' }
  ];

  it('Click Home icon in non home page should redirect to SKAO ODT home page', (done) => {
    homeNavigateTests.forEach((item) => {
      Url.openPage(item.url);
      Url.loadingCompleted(done);
      Mui.shouldEnabled('[data-testid="functionId"]'); // check the /sbd/array is presented
      Mui.click(item.id);
      Url.pathChanged(item.redirect);
    });
  });

  const documentNavigationTests = [
    {
      url: '/',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/importsb',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/general',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/scripts',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/array',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/targets',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/scans',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/general',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/scripts',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/array',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/targets',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    },
    {
      url: '/sbd/scans',
      id: 'button[aria-label="document icon"]',
      redirect: ODT_DOCUMENT_URL
    }
  ];

  it('Click document icon should redirect to SKAO ODT-UI document page', (done) => {
    documentNavigationTests.forEach((item) => {
      Url.openPage(item.url);
      Url.loadingCompleted(done);
      Mui.click(item.id);
      Url.pathChanged(item.redirect);
    });
  });
});
