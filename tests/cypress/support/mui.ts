import Url from './url';

class Mui {
  static selectValue(query, value) {
    cy.get(query)
      .parent()
      .click()
      .get('ul > li[data-value="' + value + '"]')
      .click();
  }

  static fieldVisible(query) {
    cy.get(query).should('be.visible');
  }

  static visibleWithText(query, text) {
    cy.get(query).should('be.visible').contains(text);
  }

  static click(query) {
    cy.get(query).should('be.visible').click();
  }

  static haveValue(query, value) {
    cy.get(query).should('be.visible').should('have.value', value);
  }

  static clickAndRedirect(query, path) {
    Mui.click(query);
    Url.shouldContain(path);
  }

  static shouldEnabled(query) {
    cy.get(query).should('be.enabled');
  }

  static inputValue(query, value) {
    cy.get(query).should('be.visible').type(value);
  }

  static nestedGetInputValue(query, value) {
    cy.get('div').get(query).find('input').should('have.value', value);
  }

  static nestedUpdateInputValue(query, value) {
    cy.get('div').get(query).find('input').type(value);
  }

  static textShouldExist(text) {
    cy.contains(text).should('be.visible');
  }
}

export default Mui;
